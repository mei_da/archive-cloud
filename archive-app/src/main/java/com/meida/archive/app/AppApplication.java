package com.meida.archive.app;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.bus.jackson.RemoteApplicationEventScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.zxp.esclientrhl.annotation.EnableESTools;


/**
 * APP服务
 */
@EnableFeignClients(basePackages = {"com.meida"})
@SpringBootApplication(scanBasePackages = "com.meida")
@RemoteApplicationEventScan(basePackages = "com.meida")
@MapperScan(basePackages = "com.meida.**.mapper")
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.meida"})
@EnableKnife4j
@EnableESTools(basePackages={"com.meida.module.arc.provider.repository"},entityPath = {"com.meida.module.arc.client.entity"})
public class AppApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppApplication.class, args);
    }


}
