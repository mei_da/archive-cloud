package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcRefRecord;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 关联记录 Mapper 接口
 * @author flyme
 * @date 2022-09-05
 */
public interface ArcRefRecordMapper extends SuperMapper<ArcRefRecord> {

}
