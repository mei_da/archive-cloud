package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcOriginal;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 档案原文表 Mapper 接口
 * @author flyme
 * @date 2021-12-05
 */
public interface ArcOriginalMapper extends SuperMapper<ArcOriginal> {

}
