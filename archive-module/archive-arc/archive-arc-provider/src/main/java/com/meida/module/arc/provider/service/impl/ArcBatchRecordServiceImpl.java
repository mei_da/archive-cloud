package com.meida.module.arc.provider.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcBatch;
import com.meida.module.arc.client.entity.ArcBatchRecord;
import com.meida.module.arc.client.entity.ArcInfo;
import com.meida.module.arc.provider.mapper.ArcBatchRecordMapper;
import com.meida.module.arc.provider.service.ArcBatchRecordService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.provider.service.ArcBatchService;
import com.meida.module.arc.provider.service.ArcInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.transaction.annotation.Propagation;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 批量关联记录接口实现类
 *
 * @author flyme
 * @date 2021-12-20
 */
@Service
@Transactional(rollbackFor = Exception.class)
@DS("sharding")
public class ArcBatchRecordServiceImpl extends BaseServiceImpl<ArcBatchRecordMapper, ArcBatchRecord> implements ArcBatchRecordService {

    @Autowired
    private ArcBatchService arcBatchService;

    @Autowired
    @Lazy
    private ArcInfoService arcInfoService;

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcBatchRecord abr, EntityMap extra) {
        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcBatchRecord> cq, ArcBatchRecord abr, EntityMap requestMap) {
        ApiAssert.isNotEmpty("批量关联接口id不能为空",abr);
        ApiAssert.isNotEmpty("批量关联接口id不能为空",abr.getArcBatchId());
        cq.lambda().eq(ArcBatchRecord::getArcBatchId,abr.getArcBatchId());
        cq.orderByDesc("abr.createTime");
        return ResultBody.ok();
    }

    @Override
    public ResultBody back(Map var1) {
        Object recordId = var1.get("recordId");
        ApiAssert.isNotEmpty("批量关联配置记录ID不能为空",recordId);
        ArcBatchRecord record = this.getById(Long.parseLong(recordId.toString()));
        ApiAssert.isNotEmpty("批量关联配置记录信息为空",recordId);
        ArcBatch arcBatch = arcBatchService.getById(record.getArcBatchId());
        ApiAssert.isNotEmpty("批量关联配置信息为空",arcBatch);





        //批量关联子档案信息查询
        LambdaQueryWrapper<ArcInfo> childArcInfoQuery = new LambdaQueryWrapper<ArcInfo>();
        childArcInfoQuery.eq(ArcInfo::getCategoryId,arcBatch.getTargetCategoryId())
                .eq(ArcInfo::getRelevanceNo,record.getBatchNo()).isNotNull(ArcInfo::getParentId);
        List<ArcInfo> childBatchArcInfos = this.arcInfoService.list(childArcInfoQuery);

        if(FlymeUtils.isNotEmpty(childBatchArcInfos)){
            //更新子档案
            LambdaUpdateWrapper<ArcInfo> childArcInfoUpdate = new LambdaUpdateWrapper<>();
            childArcInfoUpdate.eq(ArcInfo::getCategoryId,arcBatch.getTargetCategoryId())
                    .eq(ArcInfo::getRelevanceNo,record.getBatchNo())
                    .set(ArcInfo::getParentId,null);
            this.arcInfoService.update(childArcInfoUpdate);
            //更新父档案信息
            for(int i=0;i<childBatchArcInfos.size();i++){
                ArcInfo parentArcInfo = this.arcInfoService.getArcInfo(childBatchArcInfos.get(i).getParentId()
                        ,arcBatch.getSrcCategoryId());
                if(FlymeUtils.isNotEmpty(parentArcInfo)){
                    //更新父档案
                    LambdaUpdateWrapper<ArcInfo> parentArcInfoUpdate = new LambdaUpdateWrapper<>();
                    parentArcInfoUpdate.eq(ArcInfo::getArcInfoId,parentArcInfo.getArcInfoId())
                            .eq(ArcInfo::getCategoryId,parentArcInfo.getCategoryId())
                            .set(ArcInfo::getChildCount,FlymeUtils.isEmpty(parentArcInfo.getChildCount())||parentArcInfo.getChildCount()<1?0:parentArcInfo.getChildCount()-1);
                    this.arcInfoService.update(parentArcInfoUpdate);
                }
            }

        }
        this.removeById(Long.parseLong(recordId.toString()));
        return ResultBody.ok();
    }
}
