package com.meida.module.arc.provider.handler;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.meida.common.constants.QueueConstants;
import com.meida.module.arc.client.entity.ArcInfoRemind;
import com.meida.module.arc.provider.service.ArcInfoRemindService;
import com.meida.module.arc.provider.service.ArcUseRemindService;
import com.meida.mq.annotation.MsgListener;
import com.meida.starter.rabbitmq.config.RabbitComponent;
import com.meida.starter.rabbitmq.core.BaseRabbiMqHandler;
import com.meida.starter.rabbitmq.listener.MqListener;
import com.rabbitmq.client.Channel;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;

@RabbitComponent(value = "remindListener")
@AllArgsConstructor
@Slf4j
public class RemindListener extends BaseRabbiMqHandler<Long> {

    private final ArcInfoRemindService arcInfoRemindService;
    private final ArcUseRemindService arcUseRemindService;


    @MsgListener(queues = QueueConstants.QUEUE_REMIND)
    public void onMessage(Long id, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag) {

        super.onMessage(id, deliveryTag, channel, new MqListener<Long>() {
            @Override
            public void handler(Long map, Channel channel) {
                System.out.println(String.format("remindHandler   id=[%s]", id));
                try {
                    ArcInfoRemind infoRemind = arcInfoRemindService.getById(id);

                    if (ObjectUtil.isNotNull(infoRemind) && StrUtil.isNotEmpty(infoRemind.getRemindUserIds())) {
                        for (String s : StrUtil.split(infoRemind.getRemindUserIds(), ",")) {
                            arcUseRemindService.createRemid(5, "档案到期提醒", infoRemind.getQzId(), Long.valueOf(s), null, infoRemind.getArcInfoId());
                        }
                    }
                } catch (Exception e) {
                    log.error("档案提醒异常", e);
                }
            }
        });
    }

}

