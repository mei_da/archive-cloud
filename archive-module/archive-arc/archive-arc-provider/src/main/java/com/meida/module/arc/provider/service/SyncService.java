package com.meida.module.arc.provider.service;

import com.meida.module.arc.client.entity.ArcDestorySetting;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * <b>功能名：syncService</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-02-24 jiabing
 */
public interface SyncService {
    public Future<Long> categoryHasArcQuery(Long categoryId, String queryStr, Map param);

    public void destoryArcTask(ArcDestorySetting setting);

    public Future<Long> getCategoryHasStoreRoom(Long categoryId, Long storeId);

    /**
     * description: 删除OSS原文文件
     * date: 2023年-05月-09日 19:28
     * author: ldd
     *
     * @param objects
     * @return void
     */
    public void removeOriginalOss(List<String> objects);

    /**
     * description: 查询已删除档案
     * date: 2023年-05月-22日 12:27
     * author: ldd
     *
     * @param categoryId
     * @param o
     * @param map
     * @return java.util.concurrent.Future<java.lang.Long>
     */
    Future<Long> categoryHasRecycleArcQuery(Long categoryId, Object o, Map map);

    /**
     * description: 查询已收藏档案
     * date: 2023年-05月-30日 14:56
     * author: ldd
     *
     * @param categoryId
     * @param o
     * @param map`
     * @return java.util.concurrent.Future<java.lang.Long>
     */
    Future<Long> categoryHasCollectArcQuery(Long categoryId, Object o, Map map);


}
