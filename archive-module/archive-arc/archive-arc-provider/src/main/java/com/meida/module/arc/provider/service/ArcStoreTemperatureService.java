package com.meida.module.arc.provider.service;

import com.meida.module.arc.client.entity.ArcStoreTemperature;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 温湿度记录 接口
 *
 * @author flyme
 * @date 2022-01-03
 */
public interface ArcStoreTemperatureService extends IBaseService<ArcStoreTemperature> {

}
