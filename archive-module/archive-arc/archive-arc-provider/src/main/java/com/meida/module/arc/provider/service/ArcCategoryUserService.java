package com.meida.module.arc.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcCategoryUser;

import java.util.List;
import java.util.Map;

/**
 * 用户门类 接口
 *
 * @author flyme
 * @date 2022-08-07
 */
public interface ArcCategoryUserService extends IBaseService<ArcCategoryUser> {
    /**
     * 保存分配门类
     *
     * @param requestMap
     * @return
     */
    ResultBody saveOrUpdate(EntityMap requestMap);

    /**
     * description: 创建门类后分配角色
     * date: 2023年-05月-10日 09:28
     * author: ldd
     *
     * @param requestMap
     * @return com.meida.common.mybatis.model.ResultBody
     */
    ResultBody saveOrUpdate(Map<String, String> requestMap);

    /**
     * 查询已分配的门类
     *
     * @param roleId
     * @return
     */
    List<EntityMap> selectCateGoryByRoleId(Long roleId);
}
