//package com.meida.module.arc.provider.configurator;
//
//import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
//import com.baomidou.mybatisplus.extension.plugins.handler.TableNameHandler;
//import com.baomidou.mybatisplus.extension.plugins.inner.DynamicTableNameInnerInterceptor;
//import com.meida.common.base.utils.FlymeUtils;
//import com.meida.common.mybatis.query.CriteriaQuery;
//import com.meida.common.utils.WebUtils;
//import com.meida.module.arc.client.constants.ArchiveConstants;
//import com.meida.module.arc.client.entity.ArcCategorySuffix;
//import com.meida.module.arc.provider.service.ArcCategorySuffixService;
//import org.apache.commons.collections.map.HashedMap;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.security.core.annotation.AuthenticationPrincipal;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * <b>功能名：MybatisPlusConfig</b><br>
// * <b>说明：</b><br>
// * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
// * <b>修改履历：
// *
// * @author 2021-12-09 jiabing
// */
//@Configuration
//public class TempMybatisPlusConfig {
//
//    @Lazy
//    @Autowired
//    private ArcCategorySuffixService arcCategorySuffixService;
//
//    @Bean
//    public MybatisPlusInterceptor mybatisPlusDynamicTableNameInterceptor(){
//        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
//        DynamicTableNameInnerInterceptor dynamicTableNameInnerInterceptor
//                = new DynamicTableNameInnerInterceptor();
//
//        dynamicTableNameInnerInterceptor.setTableNameHandler((sql, tableName) -> {
//            if("arc_info".equals(tableName)||"arc_original".equals(tableName)){
//                HttpServletRequest request = WebUtils.getHttpServletRequest();
//                Object categoryId = request.getParameter("categoryId");
//                if(FlymeUtils.isNotEmpty(categoryId)){
//                    CriteriaQuery<ArcCategorySuffix> suffixCriteriaQuery = new CriteriaQuery<ArcCategorySuffix>(ArcCategorySuffix.class);
//                    suffixCriteriaQuery.lambda().eq(ArcCategorySuffix::getCategoryId,Long.parseLong(categoryId.toString()));
//                    ArcCategorySuffix suffixObj = arcCategorySuffixService.getOne(suffixCriteriaQuery);
//                    if(FlymeUtils.isNotEmpty(suffixObj)){
//                        return tableName +"_"+suffixObj.getSuffix();
//                    }
//                }
//            }
//            return tableName;
//        });
//        mybatisPlusInterceptor.addInnerInterceptor(dynamicTableNameInnerInterceptor);
//        return mybatisPlusInterceptor;
//    }
//
//}