package com.meida.module.arc.provider.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcConfigRef;
import com.meida.module.arc.provider.service.ArcConfigRefService;


/**
 * 归档配置关联控制器
 *
 * @author flyme
 * @date 2021-11-25
 */
@RestController
@RequestMapping("/arc/config/ref/")
@Api(tags = "归档配置关联", value = "归档配置关联")
public class ArcConfigRefController extends BaseController<ArcConfigRefService, ArcConfigRef>  {

    @ApiOperation(value = "归档配置关联-分页列表", notes = "归档配置关联分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "归档配置关联-列表", notes = "归档配置关联列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "归档配置关联-添加", notes = "添加归档配置关联")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "归档配置关联-添加或修改", notes = "添加或修改归档配置关联")
    @PostMapping(value = "saveOrUpdate")
    public ResultBody saveOrUpdate(@RequestParam(required = false) Map params) {
        return bizService.saveOrUpdate(params);
    }


    @ApiOperation(value = "归档配置关联-更新", notes = "更新归档配置关联")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "归档配置关联-删除", notes = "删除归档配置关联")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "归档配置关联-详情", notes = "归档配置关联详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

}
