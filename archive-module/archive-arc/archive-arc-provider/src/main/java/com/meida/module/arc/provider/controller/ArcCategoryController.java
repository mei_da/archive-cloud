package com.meida.module.arc.provider.controller;

import com.meida.common.annotation.LoginRequired;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.client.utils.ArcUtils;
import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import com.meida.module.arc.provider.service.ArcCategoryService;
import com.meida.module.arc.provider.service.ArcCategorySuffixService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * 门类控制器
 *
 * @author flyme
 * @date 2021-11-22
 */
@RestController
@RequestMapping("/arc/category/")
@Api(tags = "门类", value = "门类")
public class ArcCategoryController extends BaseController<ArcCategoryService, ArcCategory>  {
    @Autowired
    private ArcCategorySuffixService arcCategorySuffixService;

    @ApiOperation(value = "门类-分页列表", notes = "门类分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "门类-列表", notes = "门类列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "门类-列表", notes = "门类列表")
    @GetMapping(value = "listByRole")
    public ResultBody listByRole(@RequestParam(required = false) Map params) {
        params.put("listByRole","listByRole");
        return bizService.listEntityMap(params);
    }




    @ApiOperation(value = "门类-添加", notes = "添加门类")
    @PostMapping(value = "save")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.CATEGORY,optObj="添加门类")
    public ResultBody save(@RequestParam(required = false) Map params) {
//        Object tableName = params.get("tableName");
        Object type = params.get("type");
        ApiAssert.isNotEmpty("门类类型不能为空",type);

//        ResultBody result = null;
//        try{
//            result = bizService.add(params);
//        }catch (Exception e){
//            //如有异常  说明业务数据回滚
//            //此处回滚创建的档案表/原文表和档案后缀表字段
//            CriteriaQuery<ArcCategorySuffix> suffixQuery = new CriteriaQuery<ArcCategorySuffix>(ArcCategorySuffix.class);
//            suffixQuery.lambda().eq(ArcCategorySuffix::getSuffix,tableName.toString());
//            arcCategorySuffixService.remove(suffixQuery);
//
//            //删除档案表
//        }

        return bizService.add(params);
    }

    @ApiOperation(value = "门类-更新", notes = "更新门类")
    @PostMapping(value = "update")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.CATEGORY,optObj="更新门类")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "门类-删除", notes = "删除门类")
    @PostMapping(value = "remove")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.CATEGORY,optObj="删除门类")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "门类-详情", notes = "门类详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "门类-获取表名", notes = "添加门类，获取表名")
    @GetMapping(value = "tablename")
    public ResultBody tablename(@RequestParam(required = false) Map params) {
        return bizService.getTableName(params);
    }

    @ApiOperation(value = "根据字典编码或父id获取tree", notes = "根据字典编码或父id获取tree")
    @GetMapping(value = "getTreeByParentId")
    public ResultBody getTreeByParentIdOrParentDictCode(@RequestParam(required = false) Map params) {
        return bizService.getTreeByParentId(params);
    }

    @ApiOperation(value = "门类-上下移动", notes = "们配排序-上下移动")
    @GetMapping(value = "upOrDown")
    public ResultBody upOrDown(@RequestParam(required = false) Map params) {
        return bizService.upOrDown(params);
    }


    @ApiOperation(value = "回收站下门类", notes = "回收站下门类")
    @GetMapping(value = "recycleCategory")
    public ResultBody recycleCategory(@RequestParam(required = false) Map params) {
        return bizService.recycleCategory(params);
    }

    @ApiOperation(value = "收藏下门类", notes = "收藏下门类")
    @GetMapping(value = "collectCategory")
    public ResultBody collectCategory(@RequestParam(required = false) Map params) {
        return bizService.collectCategory(params);
    }

    /**
     * 导出门类统计
     *
     * @param params
     * @param request
     * @param response
     */
    @PostMapping(value = "export")
    public void export(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        bizService.export(null,params, request, response, "门类统计", "门类统计表");
    }

    @LoginRequired(required = false)
    @PostMapping(value = "test")
    public void test(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        List<ArcCategory> cats = new ArrayList<>();
        for (int i=0;i<=5000000;i++){
            ArcCategory cat = new ArcCategory();
            cat.setSysCode(ArcUtils.genNextSysCode(null,null));
            cat.setSeq(i);
            cat.setIsSystem(1);
            cat.setCnName("测试门类名称"+1);
            cat.setType(1);
            cats.add(cat);
            if(i/2000>0){
                this.bizService.saveBatch(cats);
                cats.clear();
            }
        }
        this.bizService.saveBatch(cats);
    }

}
