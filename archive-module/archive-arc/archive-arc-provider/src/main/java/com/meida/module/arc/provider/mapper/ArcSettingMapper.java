package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcSetting;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 系统配置 Mapper 接口
 * @author flyme
 * @date 2021-11-18
 */
public interface ArcSettingMapper extends SuperMapper<ArcSetting> {

}
