package com.meida.module.arc.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcInfoRemind;
import com.meida.module.arc.provider.service.ArcInfoRemindService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 档案到期提醒控制器
 *
 * @author flyme
 * @date 2023-07-24
 */
@RestController
@RequestMapping("/arc/air/")
public class ArcInfoRemindController extends BaseController<ArcInfoRemindService, ArcInfoRemind> {

    @ApiOperation(value = "档案到期提醒-分页列表", notes = "档案到期提醒分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "档案到期提醒-列表", notes = "档案到期提醒列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "档案到期提醒-添加", notes = "添加档案到期提醒")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "档案到期提醒-更新", notes = "更新档案到期提醒")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "档案到期提醒-删除", notes = "删除档案到期提醒")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "档案到期提醒-详情", notes = "档案到期提醒详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

}
