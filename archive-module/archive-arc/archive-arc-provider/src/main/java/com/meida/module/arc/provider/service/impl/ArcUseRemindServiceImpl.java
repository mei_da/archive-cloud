package com.meida.module.arc.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.vo.JoinBean;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.DateUtils;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.arc.client.entity.ArcUseRemind;
import com.meida.module.arc.provider.mapper.ArcUseRemindMapper;
import com.meida.module.arc.provider.service.ArcUseRemindService;
import com.meida.module.file.client.entity.SysFile;
import com.meida.module.file.provider.service.SysFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 借阅消息提醒接口实现类
 *
 * @author flyme
 * @date 2022-01-07
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcUseRemindServiceImpl extends BaseServiceImpl<ArcUseRemindMapper, ArcUseRemind> implements ArcUseRemindService {

    @Autowired
    SysFileService sysFileService;

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcUseRemind aur, EntityMap extra) {
        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcUseRemind> cq, ArcUseRemind aur, EntityMap requestMap) {
        ApiAssert.isNotEmpty("全宗id不能为空", aur);
        ApiAssert.isNotEmpty("全宗id不能为空", aur.getQzId());
        cq.eq("qzId");
        cq.eq("isRead");
        cq.eq("type");
        cq.eq("aur.userId", OpenHelper.getUserId());

        JoinBean join = cq.createJoin(BaseUser.class);
        join.setJoinField("userId");
        join.setMainField("verifyUserId");
        cq.select("aur.*");

        cq.addSelect(BaseUser.class, "nickName", "avatar");
        cq.orderByDesc("aur.createTime");
        return ResultBody.ok();
    }

    @Override
    public List<EntityMap> afterPageList(CriteriaQuery<ArcUseRemind> cq, List<EntityMap> data, ResultBody resultBody) {
        for (EntityMap datum : data) {
            if (datum.getLong("userId") != null && datum.getLong("verifyUserId") == null) {
                datum.put("nickName", OpenHelper.getUser().getNickName());
                datum.put("avatar", OpenHelper.getUser().getAvatar());
            }
            Date createTime = datum.get("createTime");
            //格式化显示日期
            String showTime = DateUtils.formatFromTodayCn(createTime);
            datum.put("showTime", showTime);
        }
        long count = this.count(new QueryWrapper<ArcUseRemind>().lambda().eq(ArcUseRemind::getQzId, cq.getParams("qzId")).eq(ArcUseRemind::getUserId, OpenHelper.getUserId()).eq(ArcUseRemind::getIsRead, 0));
        resultBody.setExtra(resultBody.getExtra().put("count", count));
        return super.afterPageList(cq, data, resultBody);
    }

    @Override
    public void createRemid(int type, String title, Long qzId, Long userId, Long verifyUserId, Long arcInfoId) {
        try {
            ArcUseRemind remind = new ArcUseRemind();
            remind.setQzId(qzId);
            remind.setTitle(title);
            remind.setContent("");
            remind.setType(type);
            remind.setUserId(userId);
            remind.setIsRead(0);
            remind.setUseId(null);
            remind.setUseRecordId(null);
            remind.setVerifyUserId(verifyUserId);
            remind.setCreateTime(new Date());
            remind.setArcInfoId(arcInfoId);
            if (arcInfoId != null && userId == null) {
                List<ArcUseRemind> list = this.list(new QueryWrapper<ArcUseRemind>().lambda().eq(ArcUseRemind::getArcInfoId, arcInfoId).eq(ArcUseRemind::getType, type).orderByDesc(ArcUseRemind::getCreateTime));
                remind.setUserId(list.get(0).getUserId());
            }

            this.save(remind);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createRemid(int type, String title, String content, Long qzId, Long userId, Long verifyUserId, Long arcInfoId) {
        try {
            ArcUseRemind remind = new ArcUseRemind();
            remind.setQzId(qzId);
            remind.setTitle(title);
            remind.setContent(content);
            remind.setType(type);
            remind.setUserId(userId);
            remind.setIsRead(0);
            remind.setUseId(null);
            remind.setUseRecordId(null);
            remind.setVerifyUserId(verifyUserId);
            remind.setCreateTime(new Date());
            remind.setArcInfoId(arcInfoId);
            if (arcInfoId != null && userId == null) {
                List<ArcUseRemind> list = this.list(new QueryWrapper<ArcUseRemind>().lambda().eq(ArcUseRemind::getArcInfoId, arcInfoId).eq(ArcUseRemind::getType, type).orderByDesc(ArcUseRemind::getCreateTime));
                remind.setUserId(list.get(0).getUserId());
            }

            this.save(remind);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ResultBody allRead(Map params) {
        return ResultBody.ok(this.update(new UpdateWrapper<ArcUseRemind>().lambda().eq(ArcUseRemind::getQzId, params.get("qzId")).eq(ArcUseRemind::getUserId, OpenHelper.getUserId()).set(ArcUseRemind::getIsRead, 1)));
    }

    @Override
    public ResultBody allUnRead(Map params) {
        long useRemind = this.count(new QueryWrapper<ArcUseRemind>().lambda().eq(ArcUseRemind::getQzId, params.get("qzId")).eq(ArcUseRemind::getUserId, OpenHelper.getUserId()).eq(ArcUseRemind::getIsRead, 0));
        Map map = new HashMap();
        map.put("useRemind", useRemind);
        long downloadCount = sysFileService.count(new QueryWrapper<SysFile>().lambda().eq(SysFile::getCreateUser, OpenHelper.getUserId()).isNotNull(SysFile::getOssPath).eq(SysFile::getDownloadCount, 0));
        map.put("downloadCount", downloadCount);

        return ResultBody.ok(map);
    }
}
