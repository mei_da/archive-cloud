package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcDict;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 档案字典 Mapper 接口
 * @author flyme
 * @date 2021-11-17
 */
public interface ArcDictMapper extends SuperMapper<ArcDict> {

}
