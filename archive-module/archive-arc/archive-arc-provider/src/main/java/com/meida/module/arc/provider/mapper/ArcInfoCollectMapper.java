package com.meida.module.arc.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.arc.client.entity.ArcInfoCollect;

/**
 * 档案收藏 Mapper 接口
 *
 * @author flyme
 * @date 2023-05-29
 */
public interface ArcInfoCollectMapper extends SuperMapper<ArcInfoCollect> {

}
