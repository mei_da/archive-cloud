package com.meida.module.arc.provider.log.constants;

/**
 * <b>功能名：LogOptTypeConstans</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-02-03 jiabing
 */
public class LogOptTypeConstans {
    public static final String PRE_ARC_FILE = "预归档-文件管理";
    public static final String PRE_ARC_ARC = "预归档-档案管理";
    public static final String RECYCLE_PRE = "回收站-文件管理";
    public static final String RECYCLE_ARC = "回收站-档案管理";
    public static final String RECYCLE_COMPILATION = "回收站-编研成果";
    public static final String ARC = "档案管理";
    public static final String ARC_COMPILATION = "档案编研-编研素材";
    public static final String ARC_COMPILATION_RESULT = "档案编研-编研成果";
    public static final String ARC_DESTORY_PRE = "档案销毁库-待销毁文件库";
    public static final String ARC_DESTORY_VERIFY = "档案销毁库-销毁审核";
    public static final String ARC_DESTORY_RECORD = "档案销毁库-销毁记录";
    public static final String ARC_STORE_ARC = "档案库房-库房档案";
    public static final String ARC_STORE_RECORD = "档案库房-出入库记录";
    public static final String ARC_STORE_TEMP = "档案库房-温湿度记录";
    public static final String ARC_STORE_CHECK = "档案库房-库房检查记录";
    public static final String ARC_USE = "档案利用-待借文件库";
    public static final String ARC_USE_VERIFY = "档案利用-借阅审核";
    public static final String ARC_USE_QUERY = "档案利用-借阅查询";
    public static final String ARC_USE_USER = "档案利用-用户待借文件库";
    public static final String ARC_USE_USER_MY = "档案利用-我的借阅";
    public static final String ARC_USE_USER_MGR = "档案利用-利用人管理";
    public static final String ARC_USE_USER_MSG = "档案利用-消息推送提醒";
}   