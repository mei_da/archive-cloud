package com.meida.module.arc.provider.handler;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.base.module.MyExportParams;
import com.meida.common.mybatis.interceptor.ExportInterceptor;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.provider.service.ArcStoreCheckService;
import com.meida.module.arc.provider.service.ArcStoreRecordService;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <b>功能名：ArcInfoExportHandler</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-02-11 jiabing
 */
@Service("arcStoreCheckExportHandler")
@Transactional(rollbackFor = Exception.class)
@DS("sharding")
public class ArcStoreCheckExportHandler implements ExportInterceptor {

    @Autowired
    private SysDeptService sysDeptService;

    @Autowired
    private ArcStoreCheckService arcStoreCheckService;



    @Override
    public void initExportParams(MyExportParams exportParams) {
        ServletRequestAttributes servletRequestAttributes =
                (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        Map param = (Map)request.getAttribute("reqParam");
        Object qzId = param.get("qzId");
        ApiAssert.isNotEmpty("全宗id",qzId);
        //初始化字典
        Map<String,Map<String,Object>> dictMap = new HashMap<>();
        //初始化机构名称字典
        QueryWrapper<SysDept> query = new QueryWrapper();
        query.lambda().eq(SysDept::getCompanyId,Long.parseLong(qzId.toString()));
        List<SysDept> deptList = this.sysDeptService.list(query);
        dictMap.put("unitId",deptList.stream().collect(Collectors.toMap(item->{
            return item.getDeptId().toString();
        },item->{
            return item.getDeptName();
        })));
        exportParams.setDictHandler(new ExportDictHandler(dictMap));
    }





    @Override
    public ResultBody initData(Map params) {
        return ResultBody.ok(((Map)arcStoreCheckService.pageList(params).getData()).get("records"));
    }




}   