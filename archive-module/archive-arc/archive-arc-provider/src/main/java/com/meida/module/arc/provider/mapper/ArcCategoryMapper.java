package com.meida.module.arc.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.arc.client.entity.ArcCategory;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 门类 Mapper 接口
 * @author flyme
 * @date 2021-11-22
 */
public interface ArcCategoryMapper extends SuperMapper<ArcCategory> {

    /**
     * 创建档案信息表
     * @param tableName
     * @return
     */
    int createArcInfoTable(@Param("tableName") String tableName);

    /**
     * 创建档案原文信息表
     * @param tableName
     * @return
     */
    int createArcOriginaTable(@Param("tableName") String tableName);

    /**
     * 创建档案信息表
     * @param tableName
     * @return
     */
    int createArcInfoTablePg(@Param("tableName") String tableName);

    int createArcInfoTableHg(@Param("tableName") String tableName);

    /**
     * 创建档案原文信息表
     *
     * @param tableName
     * @return
     */
    int createArcOriginaTablePg(@Param("tableName") String tableName);

    int createArcOriginaTableHg(@Param("tableName") String tableName);

    /**
     * 删除表
     */
    @Update("DROP TABLE IF EXISTS ${tableName}")
    int deleteArcInfoTable(@Param("tableName") String tableName);

    /**
     * 删除表
     */
    @Update("DROP TABLE IF EXISTS ${tableName}")
    int deleteOriginalTable(@Param("tableName") String tableName);

}
