package com.meida.module.arc.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcInfoCollect;

import java.util.List;
import java.util.Map;

/**
 * 档案收藏 接口
 *
 * @author flyme
 * @date 2023-05-29
 */
public interface ArcInfoCollectService extends IBaseService<ArcInfoCollect> {

    /**
     * description: 档案收藏
     * date: 2023年-05月-29日 16:48
     * author: ldd
     *
     * @param params
     * @return com.meida.common.mybatis.model.ResultBody
     */
    ResultBody updateCollect(Map params);

    /**
     * description: 我收藏的档案
     * date: 2023年-05月-29日 19:18
     * author: ldd
     *
     * @param requestMap
     * @param userId
     * @return List<Long>
     */
    List<Long> getArcInfoIds(EntityMap requestMap, Long userId);

    /**
     * description: 查看用户是否搜藏此档案
     * date: 2023年-05月-30日 09:37
     * author: ldd
     *
     * @param arcInfoId
     * @param userId
     * @return int
     */
    long getArcInfoCollect(Long arcInfoId, Long userId);

    /**
     * description: 我收藏的档案门类
     * date: 2023年-05月-30日 14:52
     * author: ldd
     *
     * @param param
     * @param userId
     * @return java.util.List<java.lang.Long>
     */
    List<Long> getCategoryIds(Map param, Long userId);
}
