package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcDb;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 数据库备份配置 Mapper 接口
 * @author flyme
 * @date 2021-12-25
 */
public interface ArcDbMapper extends SuperMapper<ArcDb> {

}
