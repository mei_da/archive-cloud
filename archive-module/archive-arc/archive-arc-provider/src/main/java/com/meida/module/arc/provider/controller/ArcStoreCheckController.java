package com.meida.module.arc.provider.controller;

import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.DateUtil;
import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.Date;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcStoreCheck;
import com.meida.module.arc.provider.service.ArcStoreCheckService;


/**
 * 库房检查控制器
 *
 * @author flyme
 * @date 2022-01-03
 */
@RestController
@RequestMapping("/arc/store/check/")
public class ArcStoreCheckController extends BaseController<ArcStoreCheckService, ArcStoreCheck>  {

    @ApiOperation(value = "库房检查-分页列表", notes = "库房检查分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "库房检查-列表", notes = "库房检查列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "库房检查-添加", notes = "添加库房检查")
    @PostMapping(value = "save")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.CHECK,optObj="添加库房检查")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "库房检查-更新", notes = "更新库房检查")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "库房检查-删除", notes = "删除库房检查")
    @PostMapping(value = "remove")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.CHECK,optObj="删除库房检查")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "库房检查-详情", notes = "库房检查详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }
    @GetMapping(value = "export")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.CHECK,optObj="库房检查导出")
    public void export(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute("reqParam",params);
        ApiAssert.isNotEmpty("全宗id不能为空",params.get("qzId"));
        params.put("columns","[\n" +
                "    {\n" +
                "        \"key\": \"unitId\",\n" +
                "        \"name\": \"所属部门\",\n" +
                "        \"dict\": \"unitId\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"checkUserName\",\n" +
                "        \"name\": \"检查人\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"checkTime\",\n" +
                "        \"name\": \"检查时间\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"checkName\",\n" +
                "        \"name\": \"检查内容\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"checkResult\",\n" +
                "        \"name\": \"检查结果\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"remark\",\n" +
                "        \"name\": \"备注\"\n" +
                "    }\n" +
                "]");
        bizService.export(null,params, request, response, "库房检查记录"+ DateUtil.date2Str(new Date(),"yyyyMMddHHmmss"), "库房检查记录","arcStoreCheckExportHandler");
    }
}
