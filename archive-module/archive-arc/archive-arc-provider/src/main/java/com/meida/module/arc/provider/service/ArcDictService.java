package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcDict;

import java.util.Map;

/**
 * 档案字典 接口
 *
 * @author flyme
 * @date 2021-11-17
 */
public interface ArcDictService extends IBaseService<ArcDict> {

    ResultBody upOrDown(Map var1);

    ResultBody getTreeByParentIdOrParentDictCode(Map var1);

    /**
     * description: 字典缓存列表
     * date: 2024年-01月-25日 10:26
     * author: ldd
     *
     * @param params
     * @return com.meida.common.mybatis.model.ResultBody
     */
    ResultBody list4Redis(Map params);
}
