package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcCategorySecond;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 二级分类 Mapper 接口
 * @author flyme
 * @date 2022-08-07
 */
public interface ArcCategorySecondMapper extends SuperMapper<ArcCategorySecond> {

}
