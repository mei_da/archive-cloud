package com.meida.module.arc.provider.controller;

import com.meida.common.base.entity.EntityMap;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.List;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcCategoryUser;
import com.meida.module.arc.provider.service.ArcCategoryUserService;


/**
 * 用户门类控制器
 *
 * @author flyme
 * @date 2022-08-07
 */
@RestController
@RequestMapping("/arc/category/user/")
public class ArcCategoryUserController extends BaseController<ArcCategoryUserService, ArcCategoryUser>  {

    @ApiOperation(value = "用户门类-分页列表", notes = "用户门类分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "用户门类-列表", notes = "用户门类列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "用户门类-添加", notes = "添加用户门类")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "用户门类-更新", notes = "更新用户门类")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "用户门类-添加修改", notes = "添加修改")
    @PostMapping(value = "saveOrUpdate")
    public ResultBody saveOrUpdate(@RequestParam(required = false) Map params) {
        return bizService.saveOrUpdate(new EntityMap(params));
    }

    @ApiOperation(value = "用户门类-删除", notes = "删除用户门类")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "用户门类-详情", notes = "用户门类详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "查询用户已分配的门类")
    @GetMapping(value = "listByRoleId")
    public ResultBody listByUserId(@RequestParam Long roleId) {
        List<EntityMap> list = bizService.selectCateGoryByRoleId(roleId);
        return ResultBody.ok(list);
    }


}
