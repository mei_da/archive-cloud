package com.meida.module.arc.provider.log.convert.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.utils.SpringContextHolder;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.client.entity.ArcField;
import com.meida.module.arc.client.entity.ArcLog;
import com.meida.module.arc.provider.log.convert.LogConvert;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.service.ArcCategoryService;
import com.meida.module.arc.provider.service.ArcDictService;
import com.meida.module.arc.provider.service.ArcFieldService;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysDeptService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <b>功能名：ArcUpdateLogConvert</b><br>
 * <b>说明：档案日志转换类</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-01-27 jiabing
 */
public class ArcOptLogConvert implements LogConvert {
    private ArcCategoryService arcCategoryService;
    private ArcFieldService arcFieldService;
    private SysDeptService sysDeptService;
    private ArcDictService arcDictService;
    public ArcOptLogConvert(){
        this.initService();
    }
    private void initService(){
        ArcCategoryService arcCategoryService = (ArcCategoryService) SpringContextHolder.getHandler("arcCategoryServiceImpl", ArcCategoryService.class);
        this.arcCategoryService = arcCategoryService;

        ArcFieldService arcFieldService = (ArcFieldService) SpringContextHolder.getHandler("arcFieldServiceImpl", ArcFieldService.class);
        this.arcFieldService = arcFieldService;

        SysDeptService sysDeptService = (SysDeptService) SpringContextHolder.getHandler("sysDeptServiceImpl", SysDeptService.class);
        this.sysDeptService = sysDeptService;

        ArcDictService arcDictService = (ArcDictService) SpringContextHolder.getHandler("arcDictServiceImpl", ArcDictService.class);
        this.arcDictService = arcDictService;
    }
    @Override
    public boolean convert(Object[] args, ArcLog log) {
        //判断预归档/档案管理等等
        if(LogOptTypeEnum.NULL.getName().equals(log.getOptType())){
            if(args!=null&&args.length>0){
                if(args.length==1) {

                    EntityMap param = new EntityMap((Map) args[0]);
                    if(FlymeUtils.isNotEmpty(param.get("isLog"))&&"0".equals(param.get("isLog").toString())){
                        return false;
                    }
                    if(FlymeUtils.isEmpty(log.getOptObj())&&FlymeUtils.isNotEmpty(param.get("bizType"))){
                        Object bizType = param.get("bizType");
                        switch(bizType.toString()){
                            case "0":
                                log.setOptObj("加入待归档还原");
                                break;
                            case "1":
                                log.setOptObj("加入待归档");
                                break;
                            case "2":
                                log.setOptObj("归档");
                                break;
                            case "3":
                                log.setOptObj("加入回收站");
                                break;
                            case "4":
                                log.setOptObj("回收站还原");
                                break;
                            case "5":
                                log.setOptObj("彻底删除");
                                break;
                            case "6":
                                log.setOptObj("清空回收站");
                                break;
                            case "7":
                                log.setOptObj("加入待借");
                                break;
                            case "8":
                                log.setOptObj("加入编研");
                                break;
                            case "9":
                                log.setOptObj("档案入库");
                                break;
                            case "10":
                                log.setOptObj("加入销毁");
                                break;

                        }
                    }
                    //排除条数
                    param.remove("limit");
                    //排除页码
                    param.remove("page");
                    //排除handlerName
                    param.remove("handlerName");
                    param.remove("status");
                    Object categoryIdObj = param.get("categoryId");
                    Object logOptType = param.remove("logOptType");
                    ArcCategory category = null;


                    if (ObjectUtils.isNull(categoryIdObj)) {
                        return false;
                    }
                    if (FlymeUtils.isNotEmpty(categoryIdObj)) {
                        category = arcCategoryService.getById(Long.parseLong(categoryIdObj.toString()));
                    }
                    if (FlymeUtils.isNotEmpty(logOptType)) {
                        log.setOptType(logOptType.toString());
                    }
                    if (FlymeUtils.isNotEmpty(category)) {
                        log.setOptType(log.getOptType() + "-" + category.getCnName());
                    }
                    param.remove("categoryId");
                    param.remove("qzId");
                    param.remove("isRecycle");
                    param.remove("order");
                    param.remove("sort");

                    Map<String, Object> dictMap = new HashMap<>();
                    Map dictQueryMap = new HashMap();
                    dictQueryMap.put("qzId", category.getQzId());
                    ResultBody result = arcDictService.listEntityMap(dictQueryMap);

                    dictMap.putAll(((List<EntityMap>) result.getData()).stream().filter(item -> {
                        return FlymeUtils.isNotEmpty(item.get("dictCode")) && FlymeUtils.isNotEmpty(item.get("dictName"));
                    }).collect(Collectors.toMap(item -> {
                        return item.get("dictCode").toString();
                    }, item -> {
                        return item.get("dictName").toString();
                    }, (k1, k2) -> k1)));

                    Map<String, Object> unitMap = new HashMap<>();
                    QueryWrapper<SysDept> query = new QueryWrapper();
                    query.lambda().eq(SysDept::getCompanyId, category.getQzId());
                    List<SysDept> deptList = this.sysDeptService.list(query);
                    unitMap.putAll(deptList.stream().collect(Collectors.toMap(item -> {
                        return item.getDeptId().toString();
                    }, item -> {
                        return item.getDeptName();
                    })));

                    if (FlymeUtils.isNotEmpty(categoryIdObj)) {
                        List<ArcField> fields = arcFieldService.listByCategoryId(Long.parseLong(categoryIdObj.toString()));
                        StringBuffer sb = new StringBuffer();
                        fields.forEach(item -> {
                            if (param.containsKey(item.getFieldName()) && FlymeUtils.isNotEmpty(param.get(item.getFieldName()))) {
                                if ("unitId".equals(item.getFieldName()) ) {
                                    Object value = param.get(item.getFieldName());
                                    sb.append(item.getFieldCnName());
                                    sb.append("[");
                                    if (unitMap.containsKey(value.toString())) {
                                        sb.append(unitMap.get(value.toString()));
                                    } else {
                                        sb.append(value);
                                    }
                                    sb.append("]");
                                } else if(FlymeUtils.isNotEmpty(item.getDictCode())){
                                    Object value = param.get(item.getFieldName());
                                    sb.append(item.getFieldCnName());
                                    sb.append("[");
                                    if (dictMap.containsKey(value.toString())) {
                                        sb.append(dictMap.get(value.toString()));
                                    } else {
                                        sb.append(value);
                                    }
                                    sb.append("]");
                                }else{
                                    sb.append(item.getFieldCnName());
                                    sb.append("[");
                                    sb.append(param.get(item.getFieldName()).toString());
                                    sb.append("]");
                                }
                            }
                        });
                        log.setOptContent(sb.toString());
                    }
                }
            }else{
                return false;
            }
        }
        return true;
    }

}
