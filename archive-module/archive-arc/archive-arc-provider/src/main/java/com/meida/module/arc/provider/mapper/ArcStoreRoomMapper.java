package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcStoreRoom;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 档案库房 Mapper 接口
 * @author flyme
 * @date 2022-01-02
 */
public interface ArcStoreRoomMapper extends SuperMapper<ArcStoreRoom> {

}
