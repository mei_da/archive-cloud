package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcCompilationCategory;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 素材类型 Mapper 接口
 * @author flyme
 * @date 2022-01-04
 */
public interface ArcCompilationCategoryMapper extends SuperMapper<ArcCompilationCategory> {

}
