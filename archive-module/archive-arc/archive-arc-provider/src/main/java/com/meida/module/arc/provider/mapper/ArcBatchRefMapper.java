package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcBatchRef;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 批量关联 Mapper 接口
 * @author flyme
 * @date 2021-11-29
 */
public interface ArcBatchRefMapper extends SuperMapper<ArcBatchRef> {

}
