package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcCategorySecond;

import java.util.Map;

/**
 * 二级分类 接口
 *
 * @author flyme
 * @date 2022-08-07
 */
public interface ArcCategorySecondService extends IBaseService<ArcCategorySecond> {

    public ResultBody upOrDown(Map var1);

    /**
     * description: 耳机门类缓存列表
     * date: 2024年-01月-25日 10:26
     * author: ldd
     *
     * @param params
     * @return com.meida.common.mybatis.model.ResultBody
     */
    ResultBody list4Redis(Map params);
}
