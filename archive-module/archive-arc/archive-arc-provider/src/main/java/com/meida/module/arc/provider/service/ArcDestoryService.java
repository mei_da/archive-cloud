package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcDestory;
import com.meida.module.arc.client.entity.ArcInfo;

import java.util.List;
import java.util.Map;

/**
 * 档案销毁 接口
 *
 * @author flyme
 * @date 2022-01-11
 */
public interface ArcDestoryService extends IBaseService<ArcDestory> {

    ResultBody destoryArcInfos(List<ArcInfo> arcInfoList);

    /**
     *
     */
    ResultBody destory(Map param);

    ResultBody verify(Map param);

    ResultBody getDestoryCategory(Map param);

    void delCategory(Long categoryId);

}
