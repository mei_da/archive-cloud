package com.meida.module.arc.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcConfig;
import com.meida.module.arc.client.entity.ArcOriginal;
import com.meida.module.file.client.entity.SysFile;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 档案原文表 接口
 *
 * @author flyme
 * @date 2021-12-05
 */
public interface ArcOriginalService extends IBaseService<ArcOriginal> {

    /**
     * 上传原文
     *
     * @param file
     * @param busId
     * @param busType
     * @return
     */
    EntityMap upload(HttpServletRequest request, MultipartFile[] file, Long[] fileIds, Long categoryId, Long busId, String busType);


    /**
     * 查询原文ID
     *
     * @param categoryId
     * @param arcInfoId
     * @return
     */
    List<Long> selectIddByArcInfoId(Long categoryId, Long arcInfoId);


    /**
     * @param arcInfoIds
     * @param arcConfig
     */
    void srcArcOriginal2TargetArcOriginal(List<Long> arcInfoIds, ArcConfig arcConfig);

    void delOriginal(List<Long> arcInfoIds, Long categoryId);

    /**
     * 清空原文
     *
     * @param categoryId
     * @param status     为null不过滤  0预归档-文件级 1预归档-档案级
     */
    void clearOriginal(Long categoryId, Integer status);

    /**
     * 删除原文
     *
     * @param param
     * @return
     */
    ResultBody removeOriginal(Map param);


    /**
     * 批量挂载
     *
     * @param categoryId
     */
    ResultBody batchHookUp(Long categoryId);

    /**
     * 批量挂载
     *
     * @param categoryId
     * @param hookUpType 挂接方式 1=文件 2=文件夹 3=多级文件夹
     */
    ResultBody batchHookUp2(Long categoryId,Integer hookUpType);

    /**
     * 挂接检测
     * @return
     */
    ResultBody checkHookUp();


    /**
     * 查询原文
     *
     * @param arcInfoIds
     * @param categoryId
     * @return
     */
    List<ArcOriginal> listByArcInfoIds(List<Long> arcInfoIds, Long categoryId);

    /**
     * description: 档案原文表-更新名称和排序值
     * date: 2023年-05月-15日 15:59
     * author: ldd
     *
     * @param params
     * @return com.meida.common.mybatis.model.ResultBody
     */
    ResultBody updateArcOriginalInfo(Map params);

    /**
     * description: 档案原文表上下移动
     * date: 2023年-05月-16日 14:21
     * author: ldd
     *
     * @param params
     * @return com.meida.common.mybatis.model.ResultBody
     */
    ResultBody upOrDown(Map params);

    /**
     * description: 档案原文表-文件转换和合成
     * date: 2023年-05月-16日 22:15
     * author: ldd
     *
     * @param params
     * @return com.meida.common.mybatis.model.ResultBody
     */
    ResultBody convertArcOriginal(HttpServletRequest request, Map params);

    /**
     * description: 下载原文
     * date: 2023年-08月-09日 15:41
     * author: ldd
     *
     * @param request
     * @param params
     * @param sync 是否异步下载
     * @param sysFile 文件
     * @return com.meida.common.mybatis.model.ResultBody
     */
    void downloadArcOriginal(HttpServletRequest request, HttpServletResponse response, Map params, SysFile sysFile,Boolean sync);

    /**
     * 下载次数检测
     * @param fileId
     */
    void downloadCheckCount(Long fileId);

    /**
     * 清空已下载
     * @param fileId
     */
    void clearDownLoad(Long fileId);


    /**
     * description: 打印原文
     * date: 2024年-02月-24日 10:57
     * author: ldd
     *
     * @param request
     * @param response
     * @param params
     * @return void
     */
    String printArcOriginal(HttpServletRequest request, HttpServletResponse response, Map params);
}
