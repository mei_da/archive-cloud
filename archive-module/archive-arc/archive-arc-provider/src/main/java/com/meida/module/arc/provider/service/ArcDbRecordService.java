package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcDbRecord;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.Map;

/**
 * 备份记录 接口
 *
 * @author flyme
 * @date 2021-12-25
 */
public interface ArcDbRecordService extends IBaseService<ArcDbRecord> {

    //备份还原
    ResultBody back(Map var1);

    ResultBody doDbRecord(Map var1);

    String getDefaultRecordPath();

}
