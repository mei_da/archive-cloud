package com.meida.module.arc.provider.service.impl;
import java.util.ArrayList;

import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.vo.JoinBean;
import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.JsonUtils;
import com.meida.module.arc.client.entity.ArcConfig;
import com.meida.module.arc.client.entity.ArcConfigRef;
import com.meida.module.arc.client.entity.ArcField;
import com.meida.module.arc.client.vo.ArcRefJson;
import com.meida.module.arc.provider.mapper.ArcConfigRefMapper;
import com.meida.module.arc.provider.service.ArcConfigRefService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.provider.service.ArcConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.transaction.annotation.Propagation;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 归档配置关联接口实现类
 *
 * @author flyme
 * @date 2021-11-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcConfigRefServiceImpl extends BaseServiceImpl<ArcConfigRefMapper, ArcConfigRef> implements ArcConfigRefService {

    @Autowired
    private ArcConfigService arcConfigService;
    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcConfigRef acr, EntityMap extra) {
        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcConfigRef> cq, ArcConfigRef acr, EntityMap requestMap) {
      cq.orderByDesc("acr.createTime");
      return ResultBody.ok();
    }

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<ArcConfigRef> cq, ArcConfigRef t, EntityMap requestMap) {
        ApiAssert.isNotEmpty("归档配置id不能为空",t.getConfigId());
        cq.eq(ArcConfigRef.class,"configId",t.getConfigId());

        cq.select(ArcConfigRef.class,"configRefId");
        cq.select(ArcConfigRef.class,"srcFieldId");
        cq.select(ArcConfigRef.class,"targetFieldId");
        cq.select(ArcConfigRef.class,"configId");

        JoinBean srcjoin = cq.createJoin(ArcField.class);
        srcjoin.setMainField("srcFieldId");
        srcjoin.setJoinField("fieldId");
        srcjoin.setJoinAlias("srcField");
        cq.addSelect("srcField.fieldCnName as srcFieldCnName");
        cq.addSelect("srcField.dataType as srcDataType");
        cq.addSelect("srcField.columnLength as srcColumnLength");

        JoinBean targetJoin = cq.createJoin(ArcField.class);
        targetJoin.setMainField("targetFieldId");
        targetJoin.setJoinField("fieldId");
        targetJoin.setJoinAlias("targetField");
        cq.addSelect("targetField.fieldCnName as targetFieldCnName");
        cq.addSelect("targetField.dataType as targetDataType");
        cq.addSelect("targetField.columnLength as targetColumnLength");

        cq.orderByDesc("acr.createTime");
        return ResultBody.ok();
    }

    @Override
    public ResultBody saveOrUpdate(Map var1) {
        Object configId = var1.get("configId");
        Object configRef = var1.get("configRef");
        ApiAssert.isNotEmpty("归档配置id不能为空",configId);
        ApiAssert.isNotEmpty("归档配置信息不能为空",configRef);
        //删除所有的配置
        CriteriaQuery<ArcConfigRef> refQuery = new CriteriaQuery<ArcConfigRef>(ArcConfigRef.class);
        refQuery.lambda().eq(ArcConfigRef::getConfigId,Long.parseLong(configId.toString()));
        this.remove(refQuery);
        ArcConfig config = arcConfigService.getById(Long.parseLong(configId.toString()));
        List<ArcRefJson> configMap = JsonUtils.json2list(configRef.toString(), ArcRefJson.class);
        List<ArcConfigRef> saveObjs = new ArrayList<>();
        if(FlymeUtils.isNotEmpty(configMap)){
            configMap.stream().forEach(obj->{
                ApiAssert.isNotEmpty("源档案id不能为空",obj.getSrcFieldId());
                ApiAssert.isNotEmpty("目标档案id不能为空",obj.getSrcFieldId());
                ArcConfigRef ref = new ArcConfigRef();
                ref.setDeleted(0);
                ref.setConfigId(config.getConfigId());
                ref.setSrcCategoryId(config.getSrcCategoryId());
                ref.setTargetCategoryId(config.getTargetCategoryId());
                ref.setSrcCategoryTable(config.getSrcCategoryTable());
                ref.setTargetCategoryTable(config.getTargetCategoryTable());
                ref.setSrcFieldId(obj.getSrcFieldId());
                ref.setTargetFieldId(obj.getTargetFieldId());
                saveObjs.add(ref);
            });
        }
        if(FlymeUtils.isNotEmpty(saveObjs)){
            this.saveBatch(saveObjs);
        }
        return ResultBody.ok();
    }
}
