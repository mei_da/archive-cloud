package com.meida.module.arc.provider.controller;

import com.meida.common.annotation.LoginRequired;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcSetting;
import com.meida.module.arc.provider.service.ArcSettingService;


/**
 * 系统配置控制器
 *
 * @author flyme
 * @date 2021-11-18
 */
@RestController
@RequestMapping("/arc/setting/")
@Api(tags = "系统配置", value = "系统配置")
public class ArcSettingController extends BaseController<ArcSettingService, ArcSetting>  {

    @ApiOperation(value = "系统配置-分页列表", notes = "系统配置分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "系统配置-列表", notes = "系统配置列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "系统配置-添加", notes = "添加系统配置")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "系统配置-更新", notes = "更新系统配置")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "系统配置-删除", notes = "删除系统配置")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "系统配置-详情", notes = "系统配置详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

}
