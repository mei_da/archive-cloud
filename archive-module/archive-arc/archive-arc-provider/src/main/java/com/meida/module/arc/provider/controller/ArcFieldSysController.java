package com.meida.module.arc.provider.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcFieldSys;
import com.meida.module.arc.provider.service.ArcFieldSysService;


/**
 * 系统字段表控制器
 *
 * @author flyme
 * @date 2021-11-22
 */
@RestController
@RequestMapping("/arc/field/sys/")
@Api(tags = "系统字段", value = "系统字段")
public class ArcFieldSysController extends BaseController<ArcFieldSysService, ArcFieldSys>  {

    @ApiOperation(value = "系统字段表-分页列表", notes = "系统字段表分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "系统字段表-列表", notes = "系统字段表列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "系统字段表-添加", notes = "添加系统字段表")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "系统字段表-更新", notes = "更新系统字段表")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "系统字段表-删除", notes = "删除系统字段表")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "系统字段表-详情", notes = "系统字段表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }
    /**
    * 导出系统字段表统计
    *
    * @param params
    * @param request
    * @param response
    */
    @PostMapping(value = "export")
    public void export(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        bizService.export(null,params, request, response, "系统字段表统计", "系统字段表统计表");
    }

}
