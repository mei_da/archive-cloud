package com.meida.module.arc.provider.controller;

import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcStoreRoom;
import com.meida.module.arc.provider.service.ArcStoreRoomService;


/**
 * 档案库房控制器
 *
 * @author flyme
 * @date 2022-01-02
 */
@RestController
@RequestMapping("/arc/store/room/")
public class ArcStoreRoomController extends BaseController<ArcStoreRoomService, ArcStoreRoom>  {

    @ApiOperation(value = "档案库房-分页列表", notes = "档案库房分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "档案库房-列表", notes = "档案库房列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "档案库房-添加", notes = "添加档案库房")
    @PostMapping(value = "save")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.STOREROOM,optObj="添加库房")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "档案库房-更新", notes = "更新档案库房")
    @PostMapping(value = "update")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.STOREROOM,optObj="库房更新")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "档案库房-删除", notes = "删除档案库房")
    @PostMapping(value = "remove")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.STOREROOM,optObj="删除库房")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "档案库房-详情", notes = "档案库房详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "档案库房-上下移动", notes = "档案库房-上下移动")
    @GetMapping(value = "upOrDown")
    public ResultBody upOrDown(@RequestParam(required = false) Map params) {
        return bizService.upOrDown(params);
    }

    @ApiOperation(value = "档案库房-移出库房", notes = "档案库房-移出库房")
    @PostMapping(value = "moveout")
    public ResultBody moveout(@RequestParam(required = false) Map params) {
        return bizService.moveout(params);
    }

}
