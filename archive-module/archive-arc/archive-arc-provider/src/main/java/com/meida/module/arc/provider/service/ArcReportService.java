package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcReport;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 报表模板 接口
 *
 * @author flyme
 * @date 2022-01-13
 */
public interface ArcReportService extends IBaseService<ArcReport> {

    public void export(Map params, HttpServletRequest request, HttpServletResponse response);

    public ResultBody upOrdown(Map params);

    public ResultBody getReportFieldByType(Map params);

    /**
     * description: 报表模板缓存列表
     * date: 2024年-01月-25日 10:26
     * author: ldd
     *
     * @param params
     * @return com.meida.common.mybatis.model.ResultBody
     */
    ResultBody list4Redis(Map params);
}
