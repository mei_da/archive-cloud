package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcImport;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * excel导入记录 Mapper 接口
 * @author flyme
 * @date 2021-12-26
 */
public interface ArcImportMapper extends SuperMapper<ArcImport> {

}
