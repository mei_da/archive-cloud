package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcBatchRef;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.Map;

/**
 * 批量关联 接口
 *
 * @author flyme
 * @date 2021-11-29
 */
public interface ArcBatchRefService extends IBaseService<ArcBatchRef> {
    ResultBody saveOrUpdate(Map var1);
    ResultBody targetCategoryInfo(Map var1);
    ResultBody batchRef(Map var1);

    public ResultBody batchRefField(Map var1);

}
