package com.meida.module.arc.provider.controller;

import com.meida.common.annotation.LoginRequired;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcSetting;
import com.meida.module.arc.provider.service.ArcDictService;
import com.meida.module.arc.provider.service.ArcSettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.Map;


/**
 * 免授权接口
 *
 * @author flyme
 * @date 2021-11-18
 */
@RestController
@RequestMapping("/act/common/")
@Api(tags = "免授权模块", value = "免授权模块")
public class ArcNoAuthController extends BaseController<ArcSettingService, ArcSetting> {

    @Resource
    private ArcDictService dictService;

    @ApiOperation(value = "系统配置-默认配置", notes = "系统配置-默认配置")
    @GetMapping(value = "defaultSetting")
    @LoginRequired(required = false)
    public ResultBody defaultSetting(@RequestParam(required = false) Map params) {
        return bizService.getDefaultArcSetting(params);
    }

    @ApiOperation(value = "档案字典-列表", notes = "档案字典列表")
    @GetMapping(value = "dictList")
    public ResultBody list(@RequestParam(required = false) Map params) {

        //return dictService.list4Redis(params);

        return dictService.listEntityMap(params);
    }

    @ApiOperation(value = "系统配置-更新", notes = "更新系统配置")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "上传LICENSE", notes = "上传LICENSE")
    @RequestMapping(value = "uploadLicense", method = RequestMethod.POST)
    public ResultBody uploadLicense(@RequestParam(value = "file") MultipartFile file) {
        return this.bizService.uploadLicense(file);
    }

    @ApiOperation(value = "校验授权码", notes = "校验授权码")
    @GetMapping(value = "checkToken")
    public ResultBody checkToken(@RequestParam(required = false) Map params) {
        return bizService.checkToken();
    }

}
