package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcDbRecord;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 备份记录 Mapper 接口
 * @author flyme
 * @date 2021-12-25
 */
public interface ArcDbRecordMapper extends SuperMapper<ArcDbRecord> {

}
