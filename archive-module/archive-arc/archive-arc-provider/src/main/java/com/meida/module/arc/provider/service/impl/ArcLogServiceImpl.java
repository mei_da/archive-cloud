package com.meida.module.arc.provider.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcLog;
import com.meida.module.arc.provider.mapper.ArcLogMapper;
import com.meida.module.arc.provider.service.ArcLogService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;

/**
 * 日志接口实现类
 *
 * @author flyme
 * @date 2022-01-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcLogServiceImpl extends BaseServiceImpl<ArcLogMapper, ArcLog> implements ArcLogService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcLog log, EntityMap extra) {
        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcLog> cq, ArcLog log, EntityMap requestMap) {
        // TODO 写爆Redis
        //cq.setExportKey("arcLogOpt:"+ OpenHelper.getUserId());
        cq.eq("logType");
        cq.like("arcNo", log.getArcNo());
        cq.eq("loginType");
        cq.like("optType", log.getOptType());
        cq.like("optObj", log.getOptObj());
        cq.like("optParam", log.getOptParam());
        cq.like("optContent", log.getOptContent());
        cq.like("nickName", log.getNickName());
        cq.like("userName", log.getUserName());
        cq.like("loginIp", log.getLoginIp());

        if (ObjectUtil.isNotNull(cq.getParams("account"))) {
            cq.eq("userName", cq.getParams("account"));
        }
        String optTimeBegin = requestMap.get("optTimeBegin");
        String optTimeEnd = requestMap.get("optTimeEnd");
        String loginTimeBegin = requestMap.get("loginTimeBegin");
        String loginTimeEnd = requestMap.get("loginTimeEnd");
        String logoutTimeBegin = requestMap.get("logoutTimeBegin");
        String logoutTimeEnd = requestMap.get("logoutTimeEnd");
        if (FlymeUtils.isNotEmpty(optTimeBegin)) {
            cq.ge("optTime", dateStrToDate(optTimeBegin));
        }
        if (FlymeUtils.isNotEmpty(optTimeEnd)) {
            cq.le("optTime",dateStrToDate(optTimeEnd));
        }

        if(FlymeUtils.isNotEmpty(loginTimeBegin)){
            cq.ge("loginTime",dateStrToDate(loginTimeBegin));
        }
        if(FlymeUtils.isNotEmpty(loginTimeEnd)){
            cq.le("loginTime",dateStrToDate(loginTimeEnd));
        }

        if(FlymeUtils.isNotEmpty(logoutTimeBegin)){
            cq.ge("logoutTime",dateStrToDate(logoutTimeBegin));
        }
        if(FlymeUtils.isNotEmpty(logoutTimeEnd)){
            cq.le("logoutTime",dateStrToDate(logoutTimeEnd));
        }

        String logIds =requestMap.get("logIds");
        if(FlymeUtils.isNotEmpty(logIds)){
            cq.in("logId", Arrays.asList(logIds.split(",")));
        }


        cq.orderByDesc("log.createTime");
      return ResultBody.ok();
    }
    private Date dateStrToDate(String str){
        Date date = null;
        try{
            date = DateUtil.parse(str,"yyyy-MM-dd HH:mm:ss");
        }catch (Exception e){

        }
        if(date==null){
            date =  DateUtil.parse(str,"yyyy-MM-dd");
        }
        if(date==null){
            ApiAssert.failure("查询日期转换错误，必须为yyyy-MM-dd HH:mm:ss格式");
        }

        return date;
    }

    @Async("arcInfoQueryExecutor")
    @Override
    public void saveLog(ArcLog log) {
        this.save(log);
    }
}
