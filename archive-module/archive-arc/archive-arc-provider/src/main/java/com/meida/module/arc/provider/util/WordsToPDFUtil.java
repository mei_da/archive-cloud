//package com.meida.module.arc.provider.util;
//
//import com.aspose.words.Document;
//import com.aspose.words.License;
//import com.aspose.words.SaveFormat;
//import javassist.ClassPool;
//import javassist.CtClass;
//import javassist.CtMethod;
//import lombok.extern.slf4j.Slf4j;
//
//import java.io.*;
//import java.lang.reflect.Modifier;
//
//@Slf4j
//public class WordsToPDFUtil {
//
//    public static void main(String[] args) {
//
//        String doc="/Users/mhsdong/Desktop/abcd44442.docx";
//        //String doc="/Users/mhsdong/Downloads/2b.docx";
//        String pdf="/Users/mhsdong/Desktop/output.pdf";
//        doc2pdf(doc,pdf);
//        //modifyWordsJar();
//    }
//
//    /*
//     *去水印
//     */
//    public static boolean getLicense() {
//        boolean result = false;
//        try {
//            InputStream is = WordsToPDFUtil.class.getClassLoader().getResourceAsStream("license.xml"); //  license.xml应放在..\WebRoot\WEB-INF\classes路径下
//
//            License aposeLic = new License();
//            aposeLic.setLicense(is);
//            result = true;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return result;
//    }
//    public static boolean doc2pdf(String inPath, String outPath) {
//        getLicense();
//        FileOutputStream os = null;
//        try {
//            long old = System.currentTimeMillis();
//            File file = new File(outPath); // 新建一个空白pdf文档
//            os = new FileOutputStream(file);
//            Document doc = new Document(inPath); // Address是将要被转化的word文档
//            doc.save(os, SaveFormat.PDF);// 全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF,
//            // EPUB, XPS, SWF 相互转换
//            long now = System.currentTimeMillis();
//            System.out.println("pdf转换成功，共耗时：" + ((now - old) / 1000.0) + "秒"); // 转化用时
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }finally {
//            if (os != null) {
//                try {
//                    os.flush();
//                    os.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        return true;
//    }
//
//    /**
//     * 输出到指定的目录
//     * @param is
//     * @param toPath
//     * @param fileName
//     */
//    public static void wordToPdf(InputStream is, String toPath,String fileName){
//        Document doc = null;
//      	 //去水印
//       	removeWaterMark();
//        try {
//            String resultPath = toPath + fileName + ".pdf";
//            FileOutputStream os = new FileOutputStream(resultPath);
//            doc = new Document(is);
//            doc.save(os, SaveFormat.PDF);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
// 	 /**
//     * 返回byte数组
//     * @param is
//     */
//    public static byte[] wordToPdf(InputStream is){
//        Document doc = null;
//        //去水印
//        removeWaterMark();
//        try {
//            // 创建一个字节数组输出流
//            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//            doc = new Document(is);
//            doc.save(outputStream, SaveFormat.PDF);
//            return outputStream.toByteArray();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//        /**
//     * 去除水印
//     * 使用反射替换变量
//     * @return
//     */
//     private static void removeWaterMark()   {
//         try {
//             Class<?> aClass = Class.forName("com.aspose.words.zzXyu");
//             java.lang.reflect.Field zzZXG = aClass.getDeclaredField("zzZXG");
//             zzZXG.setAccessible(true);
//             java.lang.reflect.Field modifiersField = zzZXG.getClass().getDeclaredField("modifiers");
//             modifiersField.setAccessible(true);
//             modifiersField.setInt(zzZXG, zzZXG.getModifiers() &~ Modifier.FINAL);
//             zzZXG.set(null,new byte[]{76, 73, 67, 69, 78, 83, 69, 70});
//         } catch (ClassNotFoundException e) {
//             throw new RuntimeException(e);
//         } catch (NoSuchFieldException e) {
//             throw new RuntimeException(e);
//         } catch (IllegalAccessException e) {
//             throw new RuntimeException(e);
//         }
//     }
//
//
//    /**
//     * 修改words jar包里面的校验
//     */
//    public static void modifyWordsJar() {
//        try {
//            //这一步是完整的jar包路径,选择自己解压的jar目录
//            ClassPool.getDefault().insertClassPath("/Users/mhsdong/Downloads/aspose-words-21.11-jdk16");
//            //获取指定的class文件对象
//            CtClass zzZJJClass = ClassPool.getDefault().getCtClass("com.aspose.words.zzXDb");
//            //从class对象中解析获取指定的方法
//            CtMethod[] methodA = zzZJJClass.getDeclaredMethods("zzY0J");
//            //遍历重载的方法
//            for (CtMethod ctMethod : methodA) {
//                CtClass[] ps = ctMethod.getParameterTypes();
//                if (ctMethod.getName().equals("zzY0J")) {
//                    System.out.println("ps[0].getName==" + ps[0].getName());
//                    //替换指定方法的方法体
//                    ctMethod.setBody("{this.zzZ3l = new java.util.Date(Long.MAX_VALUE);this.zzWSL = com.aspose.words.zzYeQ.zzXgr;zzWiV = this;}");
//                }
//            }
//            //这一步就是将破译完的代码放在桌面上
//            zzZJJClass.writeFile("/Users/mhsdong/Desktop/yycx/archive/jar");
//
//            //获取指定的class文件对象
//            CtClass zzZJJClassB = ClassPool.getDefault().getCtClass("com.aspose.words.zzYKk");
//            //从class对象中解析获取指定的方法
//            CtMethod methodB = zzZJJClassB.getDeclaredMethod("zzWy3");
//            //替换指定方法的方法体
//            methodB.setBody("{return 256;}");
//            //这一步就是将破译完的代码放在桌面上
//            zzZJJClassB.writeFile("/Users/mhsdong/Desktop/yycx/archive/jar");
//        } catch (Exception e) {
//            System.out.println("错误==" + e);
//        }
//    }
//}
