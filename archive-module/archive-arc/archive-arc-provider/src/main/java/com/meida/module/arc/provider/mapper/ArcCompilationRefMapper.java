package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcCompilationRef;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 编研素材关联 Mapper 接口
 * @author flyme
 * @date 2022-01-04
 */
public interface ArcCompilationRefMapper extends SuperMapper<ArcCompilationRef> {

}
