package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcCategoryUser;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 用户门类 Mapper 接口
 * @author flyme
 * @date 2022-08-07
 */
public interface ArcCategoryUserMapper extends SuperMapper<ArcCategoryUser> {

}
