package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcCompilationCategory;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 素材类型 接口
 *
 * @author flyme
 * @date 2022-01-04
 */
public interface ArcCompilationCategoryService extends IBaseService<ArcCompilationCategory> {

}
