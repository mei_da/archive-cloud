package com.meida.module.arc.provider.service.impl;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcUserCategory;
import com.meida.module.arc.provider.mapper.ArcUserCategoryMapper;
import com.meida.module.arc.provider.service.ArcUserCategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 个人档案查询接口实现类
 *
 * @author flyme
 * @date 2023-07-19
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcUserCategoryServiceImpl extends BaseServiceImpl<ArcUserCategoryMapper, ArcUserCategory> implements ArcUserCategoryService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcUserCategory auc, EntityMap extra) {
        auc.setUserId(OpenHelper.getUserId());
        auc.setSeq(999);
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcUserCategory> cq, ArcUserCategory auc, EntityMap requestMap) {
        cq.eq("userId", OpenHelper.getUserId());
        cq.eq("categoryId");
        cq.orderByAsc("auc.seq");
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<ArcUserCategory> cq, ArcUserCategory t, EntityMap requestMap) {
        cq.eq("userId", OpenHelper.getUserId());
        cq.eq("categoryId", cq.getParams("categoryId"));
        cq.orderByAsc("auc.seq");
        return ResultBody.ok();
    }

    @Override
    public ResultBody upOrDown(Map var1) {
        ApiAssert.isNotEmpty("字段id不能为空", var1.get("aucId"));
        List<ArcUserCategory> upList = new ArrayList<>();
        List<String> split = StrUtil.split(var1.get("aucId").toString(), ',');
        for (int i = 0; i < split.size(); i++) {
            ArcUserCategory toArcUserCategory = new ArcUserCategory();
            toArcUserCategory.setAucId(NumberUtil.parseLong(split.get(i)));
            toArcUserCategory.setSeq(i);
            upList.add(toArcUserCategory);
        }
        return ResultBody.ok(this.saveOrUpdateBatch(upList));

    }
}
