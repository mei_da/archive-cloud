package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcStoreCheck;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 库房检查 Mapper 接口
 * @author flyme
 * @date 2022-01-03
 */
public interface ArcStoreCheckMapper extends SuperMapper<ArcStoreCheck> {

}
