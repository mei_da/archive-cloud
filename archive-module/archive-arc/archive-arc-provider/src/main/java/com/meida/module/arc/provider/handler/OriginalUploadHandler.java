package com.meida.module.arc.provider.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcOriginal;
import com.meida.module.arc.provider.service.ArcInfoService;
import com.meida.module.arc.provider.service.ArcOriginalService;
import com.meida.module.file.client.entity.SysFile;
import com.meida.module.file.provider.handler.FileUploadHandler;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * 原文上传
 *
 * @author zyf
 */
@Component
@AllArgsConstructor
public class OriginalUploadHandler implements FileUploadHandler {

    private final ArcOriginalService arcOriginalService;
    @Lazy
    private final ArcInfoService arcInfoService;

    final EsHandler esHandler;

    @Override
    public Boolean validation(HttpServletRequest request, Long busId, EntityMap params) {
        String arcInfoId = request.getParameter("arcInfoId");
        String categoryId = request.getParameter("categoryId");
        String qzId = request.getParameter("qzId");
        ApiAssert.isNotEmpty("arcInfoId不能为空", arcInfoId);
        ApiAssert.isNotEmpty("categoryId不能为空", categoryId);
        ApiAssert.isNotEmpty("qzId不能为空", qzId);
        //传递参数给上传转换队列
        params.put("categoryId",categoryId);
        params.put("qzId",qzId);
        params.put("arcInfoId",arcInfoId);
        return true;
    }

    /**
     * 自定义保存文件
     *
     * @param sysFile
     * @return 返回结果决定SysFile表是否保存记录
     */
    @Override
    public Boolean customSaveFile(HttpServletRequest request, SysFile sysFile) {
        String arcInfoId = request.getParameter("arcInfoId");
        String categoryId = request.getParameter("categoryId");
        String qzId = request.getParameter("qzId");
        Long fileId = sysFile.getFileId();
        ArcOriginal arcOriginal = new ArcOriginal();
        arcOriginal.setArcOriginalId(fileId);
        arcOriginal.setArcInfoId(Long.parseLong(arcInfoId));
        arcOriginal.setFileName(sysFile.getFileName());
        arcOriginal.setCategoryId(Long.parseLong(categoryId));
        arcOriginal.setMd5(sysFile.getSha256());
        arcOriginal.setOssPath(sysFile.getOssPath());
        arcOriginal.setLocalPath(sysFile.getLocalPath());
        arcOriginal.setQzId(Long.parseLong(qzId));
        arcOriginal.setFileSize(sysFile.getFileSize());
        arcOriginal.setFileContenttype(sysFile.getFileType());
        arcOriginal.setFileType(sysFile.getFileExt());
        Integer width = sysFile.getWidth();
        Integer height = sysFile.getHeight();
        if (FlymeUtils.isNotEmpty(width)) {
            arcOriginal.setFilePixel(width + "x" + height);
            if (FlymeUtils.isNotEmpty(sysFile.getWidthDpi())) {
                arcOriginal.setFileDpi(sysFile.getWidthDpi() + "");
            }
        }
        arcOriginalService.save(arcOriginal);
        arcInfoService.updateOriginalCount(categoryId, arcInfoId, 1);
        esHandler.saveEs(arcOriginal);
        return false;
    }

    @Override
    public Boolean convertBefore(SysFile f) {
        return true;
    }

    @Override
    public Boolean ocrBefore(SysFile f) {
        return true;
    }
}
