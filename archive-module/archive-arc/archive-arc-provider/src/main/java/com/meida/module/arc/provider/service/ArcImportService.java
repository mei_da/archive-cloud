package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcImport;
import com.meida.common.mybatis.base.service.IBaseService;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * excel导入记录 接口
 *
 * @author flyme
 * @date 2021-12-26
 */
public interface ArcImportService extends IBaseService<ArcImport> {

    ResultBody fileUpload(MultipartFile file);

    List getExcelHead(String filePath);

    List<Map> getExcelInfo(String filePath);

    ResultBody check(String filePath, Long categoryId, Long unidId,String config);

    ResultBody doImport(String filePath, Long categoryId, Long unidId,String config);

    ResultBody back(Long importId);

    void exportTmp(Long categoryId,HttpServletRequest request, HttpServletResponse response);



}
