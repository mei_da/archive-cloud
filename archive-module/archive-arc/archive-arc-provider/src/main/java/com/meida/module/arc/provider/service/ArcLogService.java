package com.meida.module.arc.provider.service;

import com.meida.module.arc.client.entity.ArcLog;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 日志 接口
 *
 * @author flyme
 * @date 2022-01-25
 */
public interface ArcLogService extends IBaseService<ArcLog> {
    public void saveLog(ArcLog log);
}
