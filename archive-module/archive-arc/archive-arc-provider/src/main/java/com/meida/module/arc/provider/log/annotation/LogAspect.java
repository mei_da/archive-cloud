package com.meida.module.arc.provider.log.annotation;

import com.meida.module.arc.provider.log.convert.LogConvert;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;

import java.lang.annotation.*;

/**
 * <b>功能名：LogAspect</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-01-23 jiabing
 */


@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface LogAspect {

    LogTypeEnum logType() default LogTypeEnum.LOG_OPERATE;

    //操作模块
    LogOptTypeEnum optType() default LogOptTypeEnum.NULL;

    //操作类型
    String optObj() default "";

    //操作内容 只有logType为 operate必传
    String optContent() default "";

    Class<? extends LogConvert> convert() default LogConvert.class;

}   