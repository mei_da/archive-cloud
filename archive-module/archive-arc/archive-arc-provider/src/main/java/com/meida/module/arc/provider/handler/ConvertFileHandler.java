package com.meida.module.arc.provider.handler;

import com.meida.common.configuration.OpenCommonProperties;
import com.meida.module.arc.client.entity.ArcOriginal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @Description:
 * @ClassName EsHandler
 * @date: 2023.07.20 14:33
 * @Author: ldd
 */

@Slf4j
@Component
public class ConvertFileHandler {

    @Value("${meida.uploadFolder:''}")
    String uploadFolder;

    @Autowired
    private OpenCommonProperties openCommonProperties;

    @Autowired
    private JdbcTemplate jdbcTemplate;


    //转换文件
    @Async("arcInfoQueryExecutor")
    public void convert(ArcOriginal arcOriginal) {

        String localPath = arcOriginal.getLocalPath();
        File file = new File(localPath);
        try {
            if (file.exists()) {
               /* File targetDir = new File(file.getParentFile(), "pdf");
                Integer allowConvert = MapUtil.getInt(map, "allowConvert");
                ConvertResult result = new ConvertResult(null, false);
                switch (allowConvert) {
                    case 1:
                        result = OfficeUtils.convertPdf(file, targetDir, openCommonProperties.getOfficePath());
                        break;
                    case 2:
                        //视频转换
                        result = MediaUtils.convertMedia(file, targetDir, openCommonProperties.getFfmpegPath(), CommonConstants.INT_2);
                        break;
                    case 3:
                        //音频转换
                        result = MediaUtils.convertMedia(file, targetDir, openCommonProperties.getFfmpegPath(), CommonConstants.INT_3);
                        break;
                }
                if (result.isSuccess()) {
                    String ossPath = map.get("ossPath");
                    log.info("转换回调,文件路径:" + ossPath);
                    if (FlymeUtils.isNotEmpty(ossPath)) {
                        String pdfPath = getPdfPath(ossPath);
                        updatePdf(baseMap, pdfPath);
                    }
                } else {
                    log.error("转换失败:" + localPath);
                }

            */
            } else {
                log.info("文件不存在:", localPath);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
