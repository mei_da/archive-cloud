package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcField;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 字段配置表 Mapper 接口
 * @author flyme
 * @date 2021-11-22
 */
public interface ArcFieldMapper extends SuperMapper<ArcField> {

}
