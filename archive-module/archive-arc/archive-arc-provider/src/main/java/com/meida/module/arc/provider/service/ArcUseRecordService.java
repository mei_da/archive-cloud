package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcInfo;
import com.meida.module.arc.client.entity.ArcUseRecord;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;
import java.util.Map;

/**
 * 档案借阅记录 接口
 *
 * @author flyme
 * @date 2022-01-06
 */
public interface ArcUseRecordService extends IBaseService<ArcUseRecord> {
    ResultBody changeStatus(Map param);

    ResultBody verify(Map param);

    ResultBody usePurposeStatistics(Map param);

    ResultBody useStatistics(Map param);
}
