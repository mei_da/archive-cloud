package com.meida.module.arc.provider.handler;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.module.ExportField;
import com.meida.common.base.module.MyExportParams;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.ExportInterceptor;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.client.entity.ArcField;
import com.meida.module.arc.provider.service.*;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <b>功能名：ArcInfoExportHandler</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-02-11 jiabing
 */
@Service("arcDestoryExportHandler")
@Transactional(rollbackFor = Exception.class)
@DS("sharding")
public class ArcDestoryExportHandler implements ExportInterceptor {

    @Autowired
    private ArcDestoryService arcDestoryService;

    @Autowired
    private ArcCategoryService arcCategoryService;
    @Autowired
    private ArcDictService arcDictService;


    @Override
    public void initExportParams(MyExportParams exportParams) {
        ServletRequestAttributes servletRequestAttributes =
                (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        Map param = (Map)request.getAttribute("reqParam");
        Object catogoryId = param.get("categoryId");
        ApiAssert.isNotEmpty("门类id不能为空",catogoryId);
        ArcCategory category = this.arcCategoryService.getById(Long.parseLong(catogoryId.toString()));
        //初始化字典
        Map dictQueryMap = new HashMap();
        dictQueryMap.put("qzId",category.getQzId());
        ResultBody result = arcDictService.listEntityMap(dictQueryMap);
        List<EntityMap> dictList = (List)result.getData();
        Map<String,Map<String,Object>> dictMap = new HashMap<>();

        Map dbDict = dictList.stream().filter(item->{
            return FlymeUtils.isNotEmpty(item.get("dictCode"))&&FlymeUtils.isNotEmpty(item.get("dictName"));
        }).collect(Collectors.toMap(item->{
            return item.get("dictCode").toString();
        },item->{
            return item.get("dictName").toString();
        }));
        dictMap.put("arcDict",dbDict);

//        //初始化机构名称字典
//        QueryWrapper<SysDept> query = new QueryWrapper();
//        query.lambda().eq(SysDept::getCompanyId,category.getQzId());
//        List<SysDept> deptList = this.sysDeptService.list(query);
//        dictMap.put("unitId",deptList.stream().collect(Collectors.toMap(item->{
//            return item.getDeptId().toString();
//        },item->{
//           return item.getDeptName();
//        })));
        exportParams.setDictHandler(new ExportDictHandler(dictMap));
    }





    @Override
    public ResultBody initData(Map params) {
        return ResultBody.ok(((Map)arcDestoryService.pageList(params).getData()).get("records"));
    }




}   