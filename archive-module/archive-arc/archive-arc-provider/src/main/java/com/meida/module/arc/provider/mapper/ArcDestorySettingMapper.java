package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcDestorySetting;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 销毁配置 Mapper 接口
 * @author flyme
 * @date 2022-01-11
 */
public interface ArcDestorySettingMapper extends SuperMapper<ArcDestorySetting> {

}
