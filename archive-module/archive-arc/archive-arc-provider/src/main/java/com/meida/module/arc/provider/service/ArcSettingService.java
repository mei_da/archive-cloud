package com.meida.module.arc.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcSetting;
import com.meida.common.mybatis.base.service.IBaseService;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;
import java.util.Map;

/**
 * 系统配置 接口
 *
 * @author flyme
 * @date 2021-11-18
 */
public interface ArcSettingService extends IBaseService<ArcSetting> {

    ResultBody getDefaultArcSetting(Map param);

    ResultBody checkToken();

    /**
     * 查询某个租户的配置
     *
     * @return
     */
    EntityMap findByTenant();

    ResultBody uploadLicense(MultipartFile file);
}
