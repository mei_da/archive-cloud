package com.meida.module.arc.provider.repository;

import com.meida.module.arc.client.es.ArcOriginalEs;
import org.dromara.easyes.core.core.BaseEsMapper;

/**
 * @author zyf
 */
//public interface ArcOriginalRepository extends ESCRepository<ArcOriginal, Long> {
public interface ArcOriginalRepository extends BaseEsMapper<ArcOriginalEs> {


}
