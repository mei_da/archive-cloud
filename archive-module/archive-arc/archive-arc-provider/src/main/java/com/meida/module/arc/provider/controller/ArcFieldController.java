package com.meida.module.arc.provider.controller;

import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcField;
import com.meida.module.arc.provider.service.ArcFieldService;


/**
 * 字段配置表控制器
 *
 * @author flyme
 * @date 2021-11-22
 */
@RestController
@RequestMapping("/arc/field/")
@Api(tags = "字段配置", value = "字段配置")
public class ArcFieldController extends BaseController<ArcFieldService, ArcField>  {

    @ApiOperation(value = "字段配置表-分页列表", notes = "字段配置表分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "字段配置表-列表", notes = "字段配置表列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "字段配置表-添加", notes = "添加字段配置表")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "字段配置表-更新", notes = "更新字段配置表")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "字段配置表-删除", notes = "删除字段配置表")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "字段配置表-详情", notes = "字段配置表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "字段配置表-上下移动", notes = "字段配置表上下移动")
    @GetMapping(value = "upOrDown")
    public ResultBody upOrDown(@RequestParam(required = false) Map params) {
        return bizService.upOrDown(params);
    }
    /**
    * 导出字段配置表统计
    *
    * @param params
    * @param request
    * @param response
    */
    @PostMapping(value = "export")
    public void export(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        bizService.export(null,params, request, response, "字段配置表统计", "字段配置表统计表");
    }

}
