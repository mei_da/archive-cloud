package com.meida.module.arc.provider.handler;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IORuntimeException;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.constants.FileTypeConstant;
import com.meida.common.utils.RedisUtils;
import com.meida.module.arc.client.entity.ArcOriginal;
import com.meida.module.arc.client.es.ArcOriginalEs;
import com.meida.module.arc.provider.repository.ArcOriginalRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.ofdrw.reader.ContentExtractor;
import org.ofdrw.reader.OFDReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description:
 * @ClassName EsHandler
 * @date: 2023.07.20 14:33
 * @Author: ldd
 */

@Slf4j
@Component
public class EsHandler {

    @Value("${meida.uploadFolder:''}")
    String uploadFolder;
    @Autowired
    public RedisUtils redisUtils;
    @Resource
    ArcOriginalRepository arcOriginalRepository;


    //保存全文索引
    @Async("arcInfoQueryExecutor")
    public void saveEs(ArcOriginal arcOriginal) {

        try {
            String fileType = arcOriginal.getFileType();
            if (checkAllowOcr(fileType) > 0) {
                String tmpFile = uploadFolder + arcOriginal.getFileName();
                HttpUtil.downloadFile(arcOriginal.getOssPath(), tmpFile);

                String tmpText = uploadFolder + arcOriginal.getArcOriginalId() + ".txt";
                String text = "pdf".equals(arcOriginal.getFileType()) ? ExtractAllText(tmpFile) : ExtractOfdAllText(tmpFile);
                //String text = "pdf".equals(arcOriginal.getFileType()) ? ExtractAllText(tmpFile) : "";
                if (StrUtil.isNotEmpty(text) && text.length() > 5) {

                    ArcOriginalEs originalEs = new ArcOriginalEs();
                    originalEs.setArcOriginalId(arcOriginal.getArcOriginalId() + "");
                    originalEs.setArcInfoId(arcOriginal.getArcInfoId() + "");
                    originalEs.setCategoryId(arcOriginal.getCategoryId() + "");
                    originalEs.setQzId(arcOriginal.getQzId() + "");
                    originalEs.setFileName(arcOriginal.getFileName());
                    originalEs.setFileContent(text.replace("\\n", ""));
                    FileUtil.writeUtf8String(text.replace("\\n", ""), tmpText);
                    arcOriginalRepository.insert(originalEs);
                }
                try {
                    FileUtil.del(tmpFile);
                } catch (IORuntimeException e) {
                    e.printStackTrace();
                }
                try {
                    FileUtil.del(tmpText);
                } catch (IORuntimeException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            log.error("原文解析存ES异常" + e.getMessage());
        }
    }


    @Async("arcInfoQueryExecutor")
    public void delEs(List<ArcOriginal> list) {
        try {
            arcOriginalRepository.deleteBatchIds(list.stream().map(ArcOriginal::getArcOriginalId).collect(Collectors.toList()));
        } catch (Exception e) {
            log.error("删除原文ES异常" + e.getMessage());
        }
    }

    @Async("arcInfoQueryExecutor")
    public void delEsByIds(List<Long> ids) {
        try {
            arcOriginalRepository.deleteBatchIds(ids);
        } catch (Exception e) {
            log.error("删除原文ES异常" + e.getMessage());
        }
    }

    public static String ExtractAllText(String fileStr) throws IOException {
        File file = new File(fileStr);
        PDDocument doc = PDDocument.load(file);
        PDFTextStripper textStripper = new PDFTextStripper();
        String text = textStripper.getText(doc);
        doc.close();


        return text;
    }

    public static String ExtractOfdAllText(String fileStr) throws Exception {
        OFDReader reader = new OFDReader(Paths.get(fileStr));
        ContentExtractor extractor = new ContentExtractor(reader);
        return ArrayUtil.join(extractor.extractAll(), "");
    }

    /**
     * 查询上传文件配置
     *
     * @param fileType
     * @return
     */
    private EntityMap getFileType(String fileType) {
        EntityMap fileTypeMap = (EntityMap) redisUtils.get(FileTypeConstant.RedisKey);
        if (FlymeUtils.isNotEmpty(fileTypeMap)) {
            return fileTypeMap.get(fileType);
        }
        return null;
    }

    /**
     * 检测是否允许ocr识别
     *
     * @param fileType
     * @return 1 直接识别，2等待转PDF后识别
     */
    private Integer checkAllowOcr(String fileType) {
        Integer result = 0;
        EntityMap map = getFileType(fileType);
        if (FlymeUtils.isNotEmpty(map)) {
            result = map.getInt("allowOcr", 0);
        }
        return result;
    }

}
