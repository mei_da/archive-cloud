package com.meida.module.arc.provider.controller;

import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.DateUtil;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import com.meida.module.arc.provider.service.ArcCategoryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.Date;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcDestory;
import com.meida.module.arc.provider.service.ArcDestoryService;


/**
 * 档案销毁控制器
 *
 * @author flyme
 * @date 2022-01-11
 */
@RestController
@RequestMapping("/arc/destory/")
public class ArcDestoryController extends BaseController<ArcDestoryService, ArcDestory>  {

    @Autowired
    private ArcCategoryService arcCategoryService;

    @ApiOperation(value = "档案销毁-分页列表", notes = "档案销毁分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "档案销毁-列表", notes = "档案销毁列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "档案销毁-添加", notes = "添加档案销毁")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "档案销毁-更新", notes = "更新档案销毁")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "档案销毁-删除", notes = "删除档案销毁")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "档案销毁-详情", notes = "档案销毁详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "档案销毁-销毁", notes = "档案销毁-销毁")
    @GetMapping(value = "destory")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.DESTORY,optObj="档案销毁")
    public ResultBody destory(@RequestParam(required = false) Map params) {
        return bizService.destory(params);
    }

    @ApiOperation(value = "档案销毁-销毁库门类", notes = "档案销毁-销毁库门类")
    @GetMapping(value = "getDestoryCategory")
    public ResultBody getDestoryCategory(@RequestParam(required = false) Map params) {
        return bizService.getDestoryCategory(params);
    }

    @ApiOperation(value = "档案销毁-审核", notes = "档案销毁-销毁")
    @PostMapping(value = "verify")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.DESTORY,optObj="销毁审核")
    public ResultBody verify(@RequestParam(required = false) Map params) {
        return bizService.verify(params);
    }
    /**
     * 导出档案统计
     *
     * @param params
     * @param request
     * @param response
     */
    @GetMapping(value = "export")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.DESTORY,optObj="导出")
    public void export(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        Object categoryId = params.get("categoryId");
        ApiAssert.isNotEmpty("门类id不能为空",categoryId);
        ArcCategory category = arcCategoryService.getById(Long.parseLong(categoryId.toString()));
        request.setAttribute("reqParam",params);
        Object queryType = params.get("queryType");
        ApiAssert.isNotEmpty("导出类型不能为空",queryType);
        String namePre = "";
        switch(queryType.toString()){
            case "preDestory"://待销毁文件
                namePre = "待销毁文件库";
                params.put("columns","[\n" +
                        "    {\n" +
                        "        \"key\": \"filingYear\",\n" +
                        "        \"name\": \"归档年度\"\n" +
                        "    },{\n" +
                        "        \"key\": \"arcNo\",\n" +
                        "        \"name\": \"档号\"\n" +
                        "    },{\n" +
                        "        \"key\": \"retention\",\n" +
                        "        \"name\": \"保管期限\",\n" +
                        "        \"dict\": \"arcDict\"\n" +
                        "    },{\n" +
                        "        \"key\": \"maintitle\",\n" +
                        "        \"name\": \"文件题名\"\n" +
                        "    },{\n" +
                        "        \"key\": \"responsibleby\",\n" +
                        "        \"name\": \"责任者\"\n" +
                        "    }\n" +
                        "]");
                break;
            case "destoryRecord"://销毁审核
                namePre = "销毁审核";
                params.put("columns","[\n" +
                        "    {\n" +
                        "        \"key\": \"filingYear\",\n" +
                        "        \"name\": \"归档年度\"\n" +
                        "    },{\n" +
                        "        \"key\": \"arcNo\",\n" +
                        "        \"name\": \"档号\"\n" +
                        "    },{\n" +
                        "        \"key\": \"retention\",\n" +
                        "        \"name\": \"保管期限\",\n" +
                        "        \"dict\": \"arcDict\"\n" +
                        "    },{\n" +
                        "        \"key\": \"maintitle\",\n" +
                        "        \"name\": \"文件题名\"\n" +
                        "    },{\n" +
                        "        \"key\": \"responsibleby\",\n" +
                        "        \"name\": \"责任者\"\n" +
                        "    },{\n" +
                        "        \"key\": \"verifyUserName\",\n" +
                        "        \"name\": \"批准人\"\n" +
                        "    },{\n" +
                        "        \"key\": \"supervisorName\",\n" +
                        "        \"name\": \"监管人\"\n" +
                        "    },{\n" +
                        "        \"key\": \"destoryTime\",\n" +
                        "        \"name\": \"销毁日期\"\n" +
                        "    },{\n" +
                        "        \"key\": \"verifyTime\",\n" +
                        "        \"name\": \"批准日期\"\n" +
                        "    },{\n" +
                        "        \"key\": \"reason\",\n" +
                        "        \"name\": \"销毁原因\"\n" +
                        "    }\n" +
                        "]");
                break;
            case "verifyRecord"://销毁记录
                namePre = "销毁记录";
                params.put("columns","[\n" +
                        "    {\n" +
                        "        \"key\": \"filingYear\",\n" +
                        "        \"name\": \"归档年度\"\n" +
                        "    },{\n" +
                        "        \"key\": \"arcNo\",\n" +
                        "        \"name\": \"档号\"\n" +
                        "    },{\n" +
                        "        \"key\": \"retention\",\n" +
                        "        \"name\": \"保管期限\",\n" +
                        "        \"dict\": \"arcDict\"\n" +
                        "    },{\n" +
                        "        \"key\": \"maintitle\",\n" +
                        "        \"name\": \"文件题名\"\n" +
                        "    },{\n" +
                        "        \"key\": \"responsibleby\",\n" +
                        "        \"name\": \"责任者\"\n" +
                        "    },{\n" +
                        "        \"key\": \"verifyUserName\",\n" +
                        "        \"name\": \"批准人\"\n" +
                        "    },{\n" +
                        "        \"key\": \"supervisorName\",\n" +
                        "        \"name\": \"监管人\"\n" +
                        "    },{\n" +
                        "        \"key\": \"destoryTime\",\n" +
                        "        \"name\": \"销毁日期\"\n" +
                        "    },{\n" +
                        "        \"key\": \"verifyTime\",\n" +
                        "        \"name\": \"批准日期\"\n" +
                        "    }\n" +
                        "]");
                break;
        }
        bizService.export(null,params, request, response, namePre+category.getCnName()+ DateUtil.date2Str(new Date(),"yyyyMMddHHmmss"), category.getCnName(),"arcDestoryExportHandler");
    }
}
