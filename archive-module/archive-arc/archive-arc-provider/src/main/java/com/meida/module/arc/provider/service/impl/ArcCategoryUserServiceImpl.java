package com.meida.module.arc.provider.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.client.entity.ArcCategoryUser;
import com.meida.module.arc.provider.mapper.ArcCategoryUserMapper;
import com.meida.module.arc.provider.service.ArcCategoryUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 用户门类接口实现类
 *
 * @author flyme
 * @date 2022-08-07
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcCategoryUserServiceImpl extends BaseServiceImpl<ArcCategoryUserMapper, ArcCategoryUser> implements ArcCategoryUserService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcCategoryUser acu, EntityMap extra) {
        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcCategoryUser> cq, ArcCategoryUser acu, EntityMap requestMap) {
      cq.orderByDesc("acu.createTime");
      return ResultBody.ok();
    }



    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<ArcCategoryUser> cq, ArcCategoryUser t, EntityMap requestMap) {
        Long userId = t.getUserId();
        if(FlymeUtils.isNotEmpty(userId)){
            userId = OpenHelper.getUserId();
        }
        cq.eq("userId",userId);
        cq.eq("qzId");
        return ResultBody.ok();
    }

    @Override
    public ResultBody saveOrUpdate(EntityMap requestMap) {
        String categoryIds = requestMap.get("categoryIds");
        Long qzId = requestMap.getLong("qzId");
        Long roleId = requestMap.getLong("roleId");
        ApiAssert.isNotEmpty("角色id不能为空",roleId);
        QueryWrapper<ArcCategoryUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(ArcCategoryUser::getRoleId,roleId);
        if(FlymeUtils.isNotEmpty(qzId)){
            queryWrapper.lambda().eq(ArcCategoryUser::getQzId,qzId);
        }
        this.remove(queryWrapper);
        if(FlymeUtils.isNotEmpty(categoryIds)){
            String[] categoryIdArr = categoryIds.split(",");
            List<ArcCategoryUser> list = new ArrayList<>();
            for(int i=0;i<categoryIdArr.length;i++){
                ArcCategoryUser categoryUser = new ArcCategoryUser();
                categoryUser.setRoleId(roleId);
                categoryUser.setQzId(qzId);
                categoryUser.setCategoryId(Long.parseLong(categoryIdArr[i]));
                list.add(categoryUser);
            }
            if(FlymeUtils.isNotEmpty(list)){
                this.saveBatch(list);
            }
        }
        return ResultBody.ok();
    }

    @Override
    public List<EntityMap> selectCateGoryByRoleId(Long roleId) {
        CriteriaQuery cq = new CriteriaQuery(ArcCategory.class);
        cq.select(ArcCategory.class, "*");
        cq.eq(true, "acu.roleId", roleId);
        cq.createJoin(ArcCategoryUser.class).setMainField("categoryId").setJoinField("categoryId");
        return selectEntityMap(cq);
    }

    @Override
    public ResultBody saveOrUpdate(Map<String, String> requestMap) {
        String categoryIds = requestMap.get("categoryIds");
        Long qzId = Long.valueOf(requestMap.get("qzId"));
        String roleIds = requestMap.get("roleIds");
        ApiAssert.isNotEmpty("角色id不能为空", roleIds);
        if (FlymeUtils.isNotEmpty(categoryIds)) {
            String[] categoryIdArr = categoryIds.split(",");
            List<String> roleList = StrUtil.split(roleIds, ',');
            List<ArcCategoryUser> list = new ArrayList<>();
            for (int i = 0; i < categoryIdArr.length; i++) {
                for (String roleId : roleList) {
                    ArcCategoryUser categoryUser = new ArcCategoryUser();
                    categoryUser.setCategoryUserId(IdWorker.getId());
                    categoryUser.setRoleId(Long.valueOf(roleId));
                    categoryUser.setQzId(qzId);
                    categoryUser.setCategoryId(Long.parseLong(categoryIdArr[i]));
                    list.add(categoryUser);
                }
            }

            if (FlymeUtils.isNotEmpty(list)) {
                this.mutiInsert(list);
                //this.saveBatch(list);
            }
        }
        return ResultBody.ok();
    }
}
