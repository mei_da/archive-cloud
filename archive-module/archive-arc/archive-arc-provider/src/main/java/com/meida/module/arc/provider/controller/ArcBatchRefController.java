package com.meida.module.arc.provider.controller;

import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.convert.impl.ArcBatchRefLogConvert;
import com.meida.module.arc.provider.log.convert.impl.ArcOriginalLogConvert;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcBatchRef;
import com.meida.module.arc.provider.service.ArcBatchRefService;


/**
 * 批量关联控制器
 *
 * @author flyme
 * @date 2021-11-29
 */
@RestController
@RequestMapping("/arc/batch/ref/")
@Api(tags = "批量关联", value = "批量关联")
public class ArcBatchRefController extends BaseController<ArcBatchRefService, ArcBatchRef>  {

    @ApiOperation(value = "批量关联-分页列表", notes = "批量关联分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "批量关联-列表", notes = "批量关联列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "批量关联-添加", notes = "添加批量关联")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "批量关联-更新", notes = "更新批量关联")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "批量关联-删除", notes = "删除批量关联")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "批量关联-详情", notes = "批量关联详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "批量关联-添加或修改", notes = "批量关联-添加或修改")
    @PostMapping(value = "saveOrUpdate")
    public ResultBody saveOrUpdate(@RequestParam(required = false) Map params) {
        return bizService.saveOrUpdate(params);
    }

    @ApiOperation(value = "批量关联-获取目标门类信息", notes = "批量关联-获取目标门类信息")
    @PostMapping(value = "targetCategoryInfo")
    public ResultBody targetCategoryInfo(@RequestParam(required = false) Map params) {
        return bizService.targetCategoryInfo(params);
    }

    @ApiOperation(value = "批量关联-批量关联", notes = "批量关联-批量关联")
    @PostMapping(value = "batchRef")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.BATCH_REF,optObj="批量关联",convert = ArcBatchRefLogConvert.class)
    public ResultBody batchRef(@RequestParam(required = false) Map params) {
        return bizService.batchRef(params);
    }

    @ApiOperation(value = "批量关联-批量关联字段", notes = "批量关联-批量关联字段")
    @GetMapping(value = "batchRefField")
    public ResultBody batchRefField(@RequestParam(required = false) Map params) {
        return bizService.batchRefField(params);
    }
}
