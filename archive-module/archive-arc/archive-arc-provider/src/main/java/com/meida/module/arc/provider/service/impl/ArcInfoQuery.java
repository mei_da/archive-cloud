package com.meida.module.arc.provider.service.impl;

import lombok.Data;

/**
 * @Description: 档案个性查询
 * @ClassName ArcInfoQuery
 * @date: 2023.07.20 15:09
 * @Author: ldd
 */
@Data
public class ArcInfoQuery {

    String before;
    String fieldName;
    String value;
    String after;
    Integer isBlob;


}
