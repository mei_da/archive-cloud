package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcDb;
import com.meida.module.arc.client.entity.ArcHelp;

import java.util.Map;

/**
 * 数据库备份配置 接口
 *
 * @author flyme
 * @date 2021-12-25
 */
public interface ArcHelpService extends IBaseService<ArcHelp> {

    public ResultBody<ArcHelp> getHelpBySysCode(Map params);

}
