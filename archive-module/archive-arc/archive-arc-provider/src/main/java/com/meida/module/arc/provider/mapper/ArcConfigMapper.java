package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcConfig;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 归档配置 Mapper 接口
 * @author flyme
 * @date 2021-11-25
 */
public interface ArcConfigMapper extends SuperMapper<ArcConfig> {

}
