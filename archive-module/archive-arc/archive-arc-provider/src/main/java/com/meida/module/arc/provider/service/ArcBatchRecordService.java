package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcBatchRecord;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.Map;

/**
 * 批量关联记录 接口
 *
 * @author flyme
 * @date 2021-12-20
 */
public interface ArcBatchRecordService extends IBaseService<ArcBatchRecord> {
    ResultBody back(Map var1);
}
