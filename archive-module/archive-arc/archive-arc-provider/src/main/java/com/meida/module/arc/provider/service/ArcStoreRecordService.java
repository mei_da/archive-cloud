package com.meida.module.arc.provider.service;

import com.meida.module.arc.client.entity.ArcStoreRecord;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 出入库记录 接口
 *
 * @author flyme
 * @date 2022-01-03
 */
public interface ArcStoreRecordService extends IBaseService<ArcStoreRecord> {

}
