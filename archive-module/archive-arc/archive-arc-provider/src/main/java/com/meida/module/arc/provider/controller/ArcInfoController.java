package com.meida.module.arc.provider.controller;

import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.DateUtil;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.client.entity.ArcInfo;
import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.constants.LogOptObjConstants;
import com.meida.module.arc.provider.log.convert.impl.ArcOptLogConvert;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import com.meida.module.arc.provider.service.ArcCategoryService;
import com.meida.module.arc.provider.service.ArcInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;


/**
 * 档案信息表控制器
 *
 * @author flyme
 * @date 2021-12-05
 */
@RestController
@RequestMapping("/arc/info/")
public class ArcInfoController extends BaseController<ArcInfoService, ArcInfo>  {

    @Autowired
    private ArcCategoryService arcCategoryService;

    @ApiOperation(value = "档案信息表-分页列表", notes = "档案信息表分页列表")
    @GetMapping(value = "page")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optObj= LogOptObjConstants.ARC_QUERY,convert = ArcOptLogConvert.class)
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        Object sort = params.get("sort");
        Object order = params.get("order");
        if(FlymeUtils.isNotEmpty(sort)){
            params.remove("sort");
            params.put("sorts",sort);
        }
        if(FlymeUtils.isNotEmpty(order)){
            params.remove("order");
            params.put("orders",order);
        }
        return bizService.pageList(params);
    }

    @ApiOperation(value = "档案信息表-列表", notes = "档案信息表列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "档案信息表-添加", notes = "添加档案信息表")
    @PostMapping(value = "save")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optObj=LogOptObjConstants.ARC_ADD,convert = ArcOptLogConvert.class)
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }


    @ApiOperation(value = "档案信息表-更新", notes = "更新档案信息表")
    @PostMapping(value = "update")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optObj=LogOptObjConstants.ARC_UPDATE,convert = ArcOptLogConvert.class)
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "档案信息表-删除", notes = "删除档案信息表")
    @PostMapping(value = "remove")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optObj=LogOptObjConstants.ARC_DEL,convert = ArcOptLogConvert.class)
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "档案信息表-详情", notes = "档案信息表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "档案信息表-全文检索", notes = "全文检索")
    @GetMapping(value = "allCategoryQuery")
    public ResultBody allCategoryQuery(@RequestParam(required = false) Map params) {
        return bizService.allCategoryQuery(params);
    }

    @ApiOperation(value = "档案信息表-归档状态", notes = "归档状态")
    @PostMapping(value = "arcStatusChange")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,convert = ArcOptLogConvert.class)
    public ResultBody arcStatusChange(@RequestParam(required = false) Map params){
        return bizService.arcStatusChange(params);
    }

    @ApiOperation(value = "档案信息表-批量修改", notes = "批量修改")
    @PostMapping(value = "batchUpdate")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optObj="批量修改",convert = ArcOptLogConvert.class)
    public ResultBody batchUpdate(@RequestParam(required = false) Map params){
        return bizService.batchUpdate(params);
    }


    @ApiOperation(value = "档案信息表-生成件号", notes = "生成件号")
    @PostMapping(value = "genPieceNo")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optObj="生成件号",convert = ArcOptLogConvert.class)
    public ResultBody genPieceNo(@RequestParam(required = false) Map params){
        return bizService.genPieceNo(params);
    }

    @ApiOperation(value = "档案编研-移除", notes = "档案编研-移除")
    @GetMapping(value = "removeCompilation")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optObj="移除编研",convert = ArcOptLogConvert.class)
    public ResultBody removeCompilation(@RequestParam(required = false) Map params) {
        Object arcInfoIds = params.get("arcInfoIds");
        Object categoryId = params.get("categoryId");
        ApiAssert.isNotEmpty("档案id不能为空",arcInfoIds);
        ApiAssert.isNotEmpty("门类id不能为空",categoryId);
        return bizService.removeCompilation(arcInfoIds.toString(),Long.parseLong(categoryId.toString()));
    }

    @ApiOperation(value = "档案销毁-还原", notes = "档案销毁-还原")
    @GetMapping(value = "destoryBack")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optObj="销毁还原",convert = ArcOptLogConvert.class)
    public ResultBody destoryBack(@RequestParam(required = false) Map params) {

        return bizService.destoryBack(params);
    }

    @ApiOperation(value = "档案-批量调入调出", notes = "档案-批量调入调出")
    @PostMapping(value = "batchInOrOut")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optObj="调入调出",convert = ArcOptLogConvert.class)
    public ResultBody batchInOrOut(@RequestParam(required = false) Map params){
        return bizService.batchInOrOut(params);
    }

    @ApiOperation(value = "档案-批量更新档号", notes = "档案-批量更新档号")
    @PostMapping(value = "batchUpdateArcNo")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optObj="批量生成档号",convert = ArcOptLogConvert.class)
    public ResultBody batchUpdateArcNo(@RequestParam(required = false) Map params){
        return bizService.batchUpdateArcNo(params);
    }

    /**
     * 导出档案统计
     *
     * @param params
     * @param request
     * @param response
     */
    @PostMapping(value = "export")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optObj="档案导出",convert = ArcOptLogConvert.class)
    public ResultBody export(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        Object categoryId = params.get("categoryId");
        ApiAssert.isNotEmpty("门类id不能为空",categoryId);
        ArcCategory category = arcCategoryService.getById(Long.parseLong(categoryId.toString()));
        request.setAttribute("reqParam", params);
        params.put("columns", "");
        String info = bizService.export(null, params, request, response, category.getCnName() + DateUtil.date2Str(new Date(), "yyyyMMddHHmmss"), category.getCnName(), "arcInfoExportHandler");
        if (info.equals("data is empty")) {
            return ResultBody.failed("暂无可导出档案数据");
        }
        return ResultBody.ok();
    }


    @ApiOperation(value = "档案信息表-分页列表", notes = "档案信息表分页列表")
    @GetMapping(value = "espage")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = LogOptObjConstants.ARC_QUERY, convert = ArcOptLogConvert.class)
    public ResultBody espageList(@RequestParam(required = false) Map params) {
        Object sort = params.get("sort");
        Object order = params.get("order");
        if (FlymeUtils.isNotEmpty(sort)) {
            params.remove("sort");
            params.put("sorts", sort);
        }
        if (FlymeUtils.isNotEmpty(order)) {
            params.remove("order");
            params.put("orders", order);
        }
        return bizService.espageList(params);
    }

    @ApiOperation(value = "档案信息表-全文检索", notes = "全文检索")
    @GetMapping(value = "esCategoryQuery")
    public ResultBody esCategoryQuery(@RequestParam(required = false) Map params) {
        return bizService.esCategoryQuery(params);
    }

    /**
     * 出入库记录导出
     *
     * @param params
     * @param request
     * @param response
     */
    @RequestMapping(value = "exportStore")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "档案导出", convert = ArcOptLogConvert.class)
    public ResultBody exportStoreroom(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        Object categoryId = params.get("categoryId");
        ApiAssert.isNotEmpty("门类id不能为空", categoryId);
        ArcCategory category = arcCategoryService.getById(Long.parseLong(categoryId.toString()));
        request.setAttribute("reqParam", params);
        params.put("columns", "");
        String info = bizService.export(null, params, request, response, category.getCnName() + DateUtil.date2Str(new Date(), "yyyyMMddHHmmss"), category.getCnName(), "arcInfoStoreExportHandler");
        if (info.equals("data is empty")) {
            return ResultBody.failed("暂无可导出档案数据");
        }
        return ResultBody.ok();
    }

}
