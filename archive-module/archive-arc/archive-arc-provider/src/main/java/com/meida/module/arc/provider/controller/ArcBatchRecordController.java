package com.meida.module.arc.provider.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcBatchRecord;
import com.meida.module.arc.provider.service.ArcBatchRecordService;


/**
 * 批量关联记录控制器
 *
 * @author flyme
 * @date 2021-12-20
 */
@RestController
@RequestMapping("/arc/batch/record/")
public class ArcBatchRecordController extends BaseController<ArcBatchRecordService, ArcBatchRecord>  {

    @ApiOperation(value = "批量关联记录-分页列表", notes = "批量关联记录分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "批量关联记录-列表", notes = "批量关联记录列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "批量关联记录-添加", notes = "添加批量关联记录")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "批量关联记录-更新", notes = "更新批量关联记录")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "批量关联记录-删除", notes = "删除批量关联记录")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "批量关联记录-详情", notes = "批量关联记录详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "批量关联记录-取消关联", notes = "取消关联")
    @GetMapping(value = "back")
    public ResultBody back(@RequestParam(required = false) Map params) {
        return this.bizService.back(params);
    }

}
