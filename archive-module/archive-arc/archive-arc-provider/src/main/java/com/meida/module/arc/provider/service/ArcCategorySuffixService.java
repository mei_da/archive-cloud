package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.arc.client.entity.ArcCategorySuffix;

/**
 * 门类表名后缀 接口
 *
 * @author flyme
 * @date 2021-11-22
 */
public interface ArcCategorySuffixService extends IBaseService<ArcCategorySuffix> {

    void delCategory(Long categoryId);
}
