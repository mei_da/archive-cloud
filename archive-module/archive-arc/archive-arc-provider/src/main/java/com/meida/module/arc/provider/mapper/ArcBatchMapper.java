package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcBatch;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 批量关联 Mapper 接口
 * @author flyme
 * @date 2021-12-20
 */
public interface ArcBatchMapper extends SuperMapper<ArcBatch> {

}
