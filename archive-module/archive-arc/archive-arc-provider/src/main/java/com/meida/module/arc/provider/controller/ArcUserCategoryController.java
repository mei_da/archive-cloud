package com.meida.module.arc.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcUserCategory;
import com.meida.module.arc.provider.service.ArcUserCategoryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 个人档案查询控制器
 *
 * @author flyme
 * @date 2023-07-19
 */
@RestController
@RequestMapping("/arc/auc/")
public class ArcUserCategoryController extends BaseController<ArcUserCategoryService, ArcUserCategory> {

    @ApiOperation(value = "个人档案查询-分页列表", notes = "个人档案查询分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "个人档案查询-列表", notes = "个人档案查询列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "个人档案查询-添加", notes = "添加个人档案查询")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "个人档案查询-更新", notes = "更新个人档案查询")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "个人档案查询-删除", notes = "删除个人档案查询")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "个人档案查询-详情", notes = "个人档案查询详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }


    @ApiOperation(value = "个人档案查询设置-上下移动", notes = "个人档案查询设置上下移动")
    @PostMapping(value = "upOrDown")
    public ResultBody upOrDown(@RequestParam(required = false) Map params) {
        return bizService.upOrDown(params);
    }

}
