package com.meida.module.arc.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcReport;
import com.meida.module.arc.provider.service.ArcReportService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


/**
 * 报表模板控制器
 *
 * @author flyme
 * @date 2021-12-26
 */
@RestController
@RequestMapping("/arc/report/")
public class ArcReportController extends BaseController<ArcReportService, ArcReport>  {

    @ApiOperation(value = "报表模板-分页列表", notes = "报表模板分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "报表模板-列表", notes = "报表模板列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
        //return bizService.list4Redis(params);

    }

    @ApiOperation(value = "报表模板-缓存列表", notes = "报表模板缓存列表")
    @GetMapping(value = "list4Redis")
    public ResultBody list4Redis(@RequestParam(required = false) Map params) {
        return bizService.list4Redis(params);
    }

    @ApiOperation(value = "报表模板-添加", notes = "添加报表模板")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "报表模板-更新", notes = "更新报表模板")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "报表模板-删除", notes = "删除报表模板")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "报表模板-详情", notes = "报表模板详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "报表模板-上下移动", notes = "报表模板-上下移动")
    @RequestMapping(value = "upOrDown", method = RequestMethod.POST)
    public ResultBody upOrDown(@RequestParam(required = false) Map params) {
        return this.bizService.upOrdown(params);
    }

    @ApiOperation(value = "报表模板-导出", notes = "报表模板导出")
    @RequestMapping(value = "export")
    public void export(@RequestParam(required = false) Map params,HttpServletRequest request,HttpServletResponse response) {
        bizService.export(params,request,response);
    }

    @ApiOperation(value = "报表模板-模板字段", notes = "报表模板字段")
    @GetMapping(value = "getReportFieldByType")
    public ResultBody getReportFieldByType(@RequestParam(required = false) Map params) {
        return bizService.getReportFieldByType(params);
    }

}
