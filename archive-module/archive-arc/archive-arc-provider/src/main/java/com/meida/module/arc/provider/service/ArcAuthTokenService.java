package com.meida.module.arc.provider.service;

import com.meida.common.base.handler.BaseCheckAuthHandler;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.virbox.provider.entity.AuthToken;

import java.io.File;

/**
 * <b>功能名：UserUnitService</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-01-28 jiabing
 */
public interface ArcAuthTokenService extends BaseCheckAuthHandler {

    /**
     * 校验门类数量
     * @return
     */
    void checkCategoryCount();

    public AuthToken getLicense();

    public ResultBody checkTokenCode();

    public AuthToken readTokenFromTokenFile(File licenseFile);
}
