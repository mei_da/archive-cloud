package com.meida.module.arc.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcDb;
import com.meida.module.arc.client.entity.ArcHelp;
import com.meida.module.arc.provider.service.ArcDbService;
import com.meida.module.arc.provider.service.ArcHelpService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 帮助信息控制器
 *
 * @author flyme
 * @date 2021-12-25
 */
@RestController
@RequestMapping("/arc/help/")
public class ArcHelpController extends BaseController<ArcHelpService, ArcHelp>  {

    @ApiOperation(value = "帮助信息-分页列表", notes = "帮助信息分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "帮助信息-列表", notes = "帮助信息列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "帮助信息-添加", notes = "添加帮助信息")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "帮助信息-更新", notes = "更新帮助信息")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "帮助信息-删除", notes = "删除帮助信息")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "帮助信息-详情", notes = "帮助信息详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "帮助信息-详情", notes = "帮助信息详情")
    @GetMapping(value = "getHelpBySysCode")
    public ResultBody getHelpBySysCode(@RequestParam(required = false) Map params) {
        return bizService.getHelpBySysCode(params);
    }

}
