package com.meida.module.arc.provider.controller;

import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcDbRecord;
import com.meida.module.arc.provider.service.ArcDbRecordService;


/**
 * 备份记录控制器
 *
 * @author flyme
 * @date 2021-12-25
 */
@RestController
@RequestMapping("/arc/db/record/")
public class ArcDbRecordController extends BaseController<ArcDbRecordService, ArcDbRecord>  {

    @ApiOperation(value = "备份记录-分页列表", notes = "备份记录分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "备份记录-列表", notes = "备份记录列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "备份记录-添加", notes = "添加备份记录")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "备份记录-更新", notes = "更新备份记录")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "备份记录-删除", notes = "删除备份记录")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "备份记录-详情", notes = "备份记录详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "备份记录-备份还原", notes = "备份还原")
    @GetMapping(value = "back")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.DB,optObj="备份还原")
    public ResultBody back(@RequestParam(required = false) Map params) {

        return bizService.back(params);
    }

    @ApiOperation(value = "备份记录-手工备份", notes = "手工备份")
    @GetMapping(value = "doDbRecord")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.DB,optObj="手工备份")
    public ResultBody doDbRecord(@RequestParam(required = false) Map params) {

        return bizService.doDbRecord(params);
    }

}
