package com.meida.module.arc.provider.controller;

import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcDb;
import com.meida.module.arc.provider.service.ArcDbService;


/**
 * 数据库备份配置控制器
 *
 * @author flyme
 * @date 2021-12-25
 */
@RestController
@RequestMapping("/arc/db/")
public class ArcDbController extends BaseController<ArcDbService, ArcDb>  {

    @ApiOperation(value = "数据库备份配置-分页列表", notes = "数据库备份配置分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "数据库备份配置-列表", notes = "数据库备份配置列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "数据库备份配置-添加", notes = "添加数据库备份配置")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "数据库备份配置-更新", notes = "更新数据库备份配置")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "数据库备份配置-删除", notes = "删除数据库备份配置")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "数据库备份配置-详情", notes = "数据库备份配置详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "数据库备份配置-详情", notes = "数据库备份配置详情")
    @GetMapping(value = "getSetting")
    public ResultBody getSetting(@RequestParam(required = false) Map params) {
        return bizService.getSetting(params);
    }

}
