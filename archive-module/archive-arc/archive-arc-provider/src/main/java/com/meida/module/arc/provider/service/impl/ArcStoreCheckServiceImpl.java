package com.meida.module.arc.provider.service.impl;

import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcDestory;
import com.meida.module.arc.client.entity.ArcStoreCheck;
import com.meida.module.arc.client.entity.ArcStoreRecord;
import com.meida.module.arc.client.entity.ArcStoreTemperature;
import com.meida.module.arc.provider.mapper.ArcStoreCheckMapper;
import com.meida.module.arc.provider.service.ArcStoreCheckService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.provider.service.UserUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.transaction.annotation.Propagation;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 库房检查接口实现类
 *
 * @author flyme
 * @date 2022-01-03
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcStoreCheckServiceImpl extends BaseServiceImpl<ArcStoreCheckMapper, ArcStoreCheck> implements ArcStoreCheckService {

    @Autowired
    private UserUnitService userUnitService;
    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcStoreCheck asc, EntityMap extra) {
        ApiAssert.isNotEmpty("所属部门不能为空",asc);
        ApiAssert.isNotEmpty("所属部门不能为空",asc.getUnitId());
        ApiAssert.isNotEmpty("全宗id不能为空",asc.getQzId());
        return ResultBody.ok();
    }
    @Override
    public ResultBody beforeEdit(CriteriaUpdate<ArcStoreCheck> cu, ArcStoreCheck asr, EntityMap extra) {
        ApiAssert.isNotEmpty("编辑对象不能为空",asr);
        ApiAssert.isNotEmpty("编辑对象不能为空",asr.getCheckId());

        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcStoreCheck> cq, ArcStoreCheck asc, EntityMap requestMap) {
        ApiAssert.isNotEmpty("全宗id不能为空",asc.getQzId());
        cq.eq("qzId");
        cq.eq("unitId",asc.getUnitId());
        cq.like("checkUserName",asc.getCheckUserName());
        cq.like("checkTime",asc.getCheckTime());
        cq.like("checkName",asc.getCheckName());
        cq.like("checkResult",asc.getCheckResult());
        cq.like("remark",asc.getRemark());
        //过滤用户负责机构的数据
        List<Long> unitIds = userUnitService.getLoginUserUnitIds(asc.getQzId(),null);
        if(FlymeUtils.isNotEmpty(unitIds)){
            cq.lambda().in(ArcStoreCheck::getUnitId,unitIds);
        }
        String begin = requestMap.get("checkTimeBegin");
        String end = requestMap.get("checkTimeEnd");
        if(FlymeUtils.isNotEmpty(begin)){
            cq.ge("checkTime",begin);
        }
        if(FlymeUtils.isNotEmpty(end)){
            cq.le("checkTime",end);
        }
        Object checkIds = requestMap.get("checkIds");
        if (FlymeUtils.isNotEmpty(checkIds)){
            List<Long> ids = Arrays.asList(checkIds.toString().split(",")).stream().map(item->{
                return Long.parseLong(item);
            }).collect(Collectors.toList());
            cq.lambda().in(ArcStoreCheck::getCheckId,ids);
        }
        cq.orderByDesc("ch.createTime");
        return ResultBody.ok();
    }
}
