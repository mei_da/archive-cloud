package com.meida.module.arc.provider.log.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 档案相关枚举
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum LogOptTypeEnum {
    NULL(0,"待定"),
    PREARC_PRE(1,"预归档-文件级"),
    PREARC_ARC(2,"预归档-档案管理"),
    BATCH_REF(3,"批量关联配置"),
    CATEGORY(4,"门类配置"),
    COMPILATION_TYPE(5,"素材类型"),
    ARC_CONFIG(6,"归档配置"),
    DB(7,"数据备份"),
    DESTORY(8,"档案销毁库"),
    DICT(9,"字典管理"),
    FIELD(10,"门类字段"),
    IMPORT(11,"EXCEL导入"),
    CHECK(12,"库房检查"),
    STORERECORD(13,"出入库记录"),
    STOREROOM(14,"库房管理"),
    STORETEMP(15,"库房温湿度记录")
    ;

    LogOptTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static LogOptTypeEnum matchCode(Integer code){
        for(LogOptTypeEnum obj: LogOptTypeEnum.values()){
            if(obj.getCode().equals(code)){
                return obj;
            }
        }
        return null;
    }


     public static LogOptTypeEnum getValue(int code){
        for (LogOptTypeEnum color: values()) {
             if(color.getCode().equals(code)){
                     return  color;
             }
         }
         return null;
    }
}
