package com.meida.module.arc.provider.controller;

import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcDict;
import com.meida.module.arc.provider.service.ArcDictService;


/**
 * 档案字典控制器
 *
 * @author flyme
 * @date 2021-11-17
 */
@RestController
@RequestMapping("/arc/dict/")
@Api(tags = "档案字典", value = "档案字典")
public class ArcDictController extends BaseController<ArcDictService, ArcDict>  {

    @ApiOperation(value = "档案字典-分页列表", notes = "档案字典分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "档案字典-列表", notes = "档案字典列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "档案字典-添加", notes = "添加档案字典")
    @PostMapping(value = "save")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.DICT,optObj="添加字典")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "档案字典-更新", notes = "更新档案字典")
    @PostMapping(value = "update")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.DICT,optObj="更新字典")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "档案字典-删除", notes = "删除档案字典")
    @PostMapping(value = "remove")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.DICT,optObj="删除字典")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "档案字典-详情", notes = "档案字典详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "根据字典编码或父id获取tree", notes = "根据字典编码或父id获取tree")
    @GetMapping(value = "getTreeByParentIdOrParentDictCode")
    public ResultBody getTreeByParentIdOrParentDictCode(@RequestParam(required = false) Map params) {
        return bizService.getTreeByParentIdOrParentDictCode(params);
    }

    @ApiOperation(value = "档案字典-上下移动", notes = "档案字典-上下移动")
    @GetMapping(value = "upOrDown")
    public ResultBody upOrDown(@RequestParam(required = false) Map params) {
        return bizService.upOrDown(params);
    }
    //TODO 树形结构查询
    /**
    * 导出档案字典统计
    *
    * @param params
    * @param request
    * @param response
    */
    @PostMapping(value = "export")
    public void export(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        bizService.export(null,params, request, response, "档案字典统计", "档案字典统计表");
    }

}
