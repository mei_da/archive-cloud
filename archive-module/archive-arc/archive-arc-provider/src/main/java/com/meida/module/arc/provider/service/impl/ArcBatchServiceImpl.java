package com.meida.module.arc.provider.service.impl;

import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.mybatis.vo.JoinBean;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcBatch;
import com.meida.module.arc.client.entity.ArcBatchRecord;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.client.enums.CategoryTypeEnum;
import com.meida.module.arc.provider.mapper.ArcBatchMapper;
import com.meida.module.arc.provider.service.ArcBatchRecordService;
import com.meida.module.arc.provider.service.ArcBatchService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.provider.service.ArcCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.transaction.annotation.Propagation;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

/**
 * 批量关联接口实现类
 *
 * @author flyme
 * @date 2021-12-20
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcBatchServiceImpl extends BaseServiceImpl<ArcBatchMapper, ArcBatch> implements ArcBatchService {

    @Autowired
    private ArcCategoryService arcCategoryService;
    @Autowired
    private ArcBatchRecordService arcBatchRecordService;
    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcBatch batch, EntityMap extra) {
        ApiAssert.isNotEmpty("批量关联信息不能为空",batch);
        ApiAssert.isNotEmpty("全宗id不能为空",batch.getQzId());
        ApiAssert.isNotEmpty("名称不能为空",batch.getName());
        ApiAssert.isNotEmpty("源门类不能为空",batch.getSrcCategoryId());
        ApiAssert.isNotEmpty("下级门类不能为空",batch.getTargetCategoryId());
        //校验源门类和目标门类是否已配置了匹配关系
        CriteriaQuery<ArcBatch> query = new CriteriaQuery<ArcBatch>(ArcBatch.class);
        query.lambda().eq(ArcBatch::getSrcCategoryId,batch.getSrcCategoryId())
                .eq(ArcBatch::getTargetCategoryId,batch.getTargetCategoryId());
        Long count = this.count(query);
        if(count>0){
            return ResultBody.failed("源门类和下级已配置过关联，不能重复配置");
        }
        //校验源门类和目标门类的对应关系是否正确
        ArcCategory srcCategory = arcCategoryService.getById(batch.getSrcCategoryId());
        if(!CategoryTypeEnum.CATEGORY_TYPE_1.getCode().equals(srcCategory.getType())&&
            !CategoryTypeEnum.CATEGORY_TYPE_2.getCode().equals(srcCategory.getType())){
            return ResultBody.failed("源门类类型只能是项目或案卷");
        }
        ArcCategory targetCategory = arcCategoryService.getById(batch.getTargetCategoryId());
        if(!targetCategory.getParentId().equals(srcCategory.getCategoryId())){
            return ResultBody.failed("下级门类必须为源门的子级");
        }
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeEdit(CriteriaUpdate<ArcBatch> cu, ArcBatch t, EntityMap extra) {
        ApiAssert.isNotEmpty("批量关联ID不能为空",t);
        ApiAssert.isNotEmpty("批量关联ID不能为空",t.getBatchId());
        ApiAssert.isNotEmpty("名称不能为空",t.getName());
        ArcBatch beforeEdit = this.getById(t.getBatchId());
        if(FlymeUtils.isNotEmpty(t.getSrcCategoryId())
                &&!t.getSrcCategoryId().equals(beforeEdit.getSrcCategoryId())){
            return ResultBody.failed("源门类不能修改");
        }
        if(FlymeUtils.isNotEmpty(t.getTargetCategoryId())
                &&!t.getTargetCategoryId().equals(beforeEdit.getTargetCategoryId())){
            return ResultBody.failed("=下级门类不能修改");
        }
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeDelete(CriteriaDelete<ArcBatch> cd) {
        ApiAssert.isNotEmpty("批量关联ID不能为空",cd.getIdValue());
        CriteriaQuery<ArcBatchRecord> query = new CriteriaQuery<ArcBatchRecord>(ArcBatchRecord.class);
        query.lambda().eq(ArcBatchRecord::getArcBatchId,cd.getIdValue());
        Long count = this.arcBatchRecordService.count(query);
        if(count>0){
            return ResultBody.failed("该配置已执行过批量关联，请先还原批量关联再删除");
        }

        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcBatch> cq, ArcBatch batch, EntityMap requestMap) {
      cq.orderByDesc("batch.createTime");
      return ResultBody.ok();
    }
    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<ArcBatch> cq, ArcBatch t, EntityMap requestMap) {
        ApiAssert.isNotEmpty("全宗id不能为空",t.getQzId());
        cq.eq(ArcBatch.class,"qzId");

        cq.select(ArcBatch.class,"batchId");
        cq.select(ArcBatch.class,"srcCategoryId");
        cq.select(ArcBatch.class,"targetCategoryId");
        cq.select(ArcBatch.class,"qzId");
        cq.select(ArcBatch.class,"name");

        JoinBean srcCategoryJoin = cq.createJoin(ArcCategory.class);
        srcCategoryJoin.setJoinAlias("srcCategory");
        srcCategoryJoin.setMainField("srcCategoryId");
        cq.addSelect("srcCategory.cnName as srcCategoryName");

        JoinBean targetCategoryJoin = cq.createJoin(ArcCategory.class);
        targetCategoryJoin.setJoinAlias("targetCategory");
        targetCategoryJoin.setMainField("targetCategoryId");
        cq.addSelect("targetCategory.cnName as targetCategoryName");

        cq.orderByDesc("batch.createTime");
        return ResultBody.ok();
    }
}
