package com.meida.module.arc.provider.log.convert.impl;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.utils.SpringContextHolder;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.client.entity.ArcLog;
import com.meida.module.arc.provider.log.convert.LogConvert;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.service.ArcCategoryService;

import java.util.Map;

/**
 * <b>功能名：ArcUpdateLogConvert</b><br>
 * <b>说明：档案日志转换类</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-01-27 jiabing
 */
public class ArcBatchRefLogConvert implements LogConvert {
    private ArcCategoryService arcCategoryService;
    public ArcBatchRefLogConvert(){
        this.initService();
    }
    private void initService(){
        ArcCategoryService arcCategoryService = (ArcCategoryService) SpringContextHolder.getHandler("arcCategoryServiceImpl", ArcCategoryService.class);
        this.arcCategoryService = arcCategoryService;

    }
    @Override
    public boolean convert(Object[] args, ArcLog log) {
        //判断预归档/档案管理等等
        if(LogOptTypeEnum.NULL.getName().equals(log.getOptType())){
            if(args!=null&&args.length>0){
                if(args.length==1) {

                    Map param = (Map) args[0];

                    if(FlymeUtils.isNotEmpty(param.get("isLog"))&&"0".equals(param.get("isLog").toString())){
                        return false;
                    }
                    Object categoryIdObj = param.get("categoryId");
                    if(FlymeUtils.isEmpty(categoryIdObj)){
                        categoryIdObj = param.get("targetCategoryId");
                    }
                    ArcCategory category = null;

                    if (FlymeUtils.isNotEmpty(categoryIdObj)) {
                        category = arcCategoryService.getById(Long.parseLong(categoryIdObj.toString()));
                    }
                    if (FlymeUtils.isNotEmpty(category)) {
                        log.setOptType(log.getOptType() + "-" + category.getCnName());
                    }
                }
            }else{
                return false;
            }
        }
        return true;
    }

}