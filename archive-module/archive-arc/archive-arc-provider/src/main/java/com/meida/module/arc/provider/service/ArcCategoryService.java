package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcCategory;

import java.util.Map;

/**
 * 门类 接口
 *
 * @author flyme
 * @date 2021-11-22
 */
public interface ArcCategoryService extends IBaseService<ArcCategory> {

    ResultBody getTableName(Map var1);

    ResultBody upOrDown(Map var1);

    ResultBody getTreeByParentId(Map var1);

    void genCategoryTable(String tableNameSuffix);

    void rollBackGenCategoryTable(String tableNameSuffix);

    public void addArcCategorySuffix(String tableName, Long qzId);

    /**
     * 回收站下门类
     *
     * @param params
     * @return
     */
    ResultBody recycleCategory(Map params);

    /**
     * description: 收藏下门类
     * date: 2023年-05月-30日 14:49
     * author: ldd
     *
     * @param params
     * @return com.meida.common.mybatis.model.ResultBody
     */
    ResultBody collectCategory(Map params);
}
