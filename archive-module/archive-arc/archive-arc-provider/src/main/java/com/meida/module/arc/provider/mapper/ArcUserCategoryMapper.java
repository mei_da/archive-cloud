package com.meida.module.arc.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.arc.client.entity.ArcUserCategory;

/**
 * 个人档案查询 Mapper 接口
 *
 * @author flyme
 * @date 2023-07-19
 */
public interface ArcUserCategoryMapper extends SuperMapper<ArcUserCategory> {

}
