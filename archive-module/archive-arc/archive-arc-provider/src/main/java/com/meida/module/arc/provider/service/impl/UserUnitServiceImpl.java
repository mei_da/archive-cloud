package com.meida.module.arc.provider.service.impl;

import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.RedisUtils;
import com.meida.module.admin.provider.service.BaseUserService;
import com.meida.module.arc.provider.service.UserUnitService;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>功能名：UserUnitServiceImpl</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-01-28 jiabing
 */
@Service
public class UserUnitServiceImpl implements UserUnitService {


    @Autowired
    private SysDeptService deptService;
    @Autowired
    private RedisUtils redisUtils;

    @Override
    public List<Long> getLoginUserUnitIds(Long qzId,Long userId) {
        if(FlymeUtils.isEmpty(userId)){
            userId= OpenHelper.getUserId();
        }
        if(FlymeUtils.isEmpty(userId)){
            Long threadZId = Thread.currentThread().getId();
            String userIdStr = redisUtils.getString(threadZId.toString());
            if (FlymeUtils.isNotEmpty(userIdStr)){
                redisUtils.del(threadZId.toString());
                userId = Long.parseLong(userIdStr);
            }
        }
        List<Long> userUnitIds = deptService.selectDeptByUserId(userId,qzId);
        if(FlymeUtils.isEmpty(userUnitIds)){
            userUnitIds = new ArrayList<>();
            userUnitIds.add(18888L);
        }
        return userUnitIds;
    }
}