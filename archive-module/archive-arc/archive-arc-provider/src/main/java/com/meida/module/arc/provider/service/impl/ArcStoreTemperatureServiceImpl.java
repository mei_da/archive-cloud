package com.meida.module.arc.provider.service.impl;

import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcStoreCheck;
import com.meida.module.arc.client.entity.ArcStoreRecord;
import com.meida.module.arc.client.entity.ArcStoreTemperature;
import com.meida.module.arc.provider.mapper.ArcStoreTemperatureMapper;
import com.meida.module.arc.provider.service.ArcStoreTemperatureService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.provider.service.UserUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.transaction.annotation.Propagation;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 温湿度记录接口实现类
 *
 * @author flyme
 * @date 2022-01-03
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcStoreTemperatureServiceImpl extends BaseServiceImpl<ArcStoreTemperatureMapper, ArcStoreTemperature> implements ArcStoreTemperatureService {
    @Autowired
    private UserUnitService userUnitService;
    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcStoreTemperature asc, EntityMap extra) {
        ApiAssert.isNotEmpty("所属部门不能为空",asc);
        ApiAssert.isNotEmpty("所属部门不能为空",asc.getUnitId());
        ApiAssert.isNotEmpty("全宗id不能为空",asc.getQzId());
        return ResultBody.ok();
    }
    @Override
    public ResultBody beforeEdit(CriteriaUpdate<ArcStoreTemperature> cu, ArcStoreTemperature asr, EntityMap extra) {
        ApiAssert.isNotEmpty("编辑对象不能为空",asr);
        ApiAssert.isNotEmpty("编辑对象不能为空",asr.getTemperatureId());
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcStoreTemperature> cq, ArcStoreTemperature asc, EntityMap requestMap) {
        ApiAssert.isNotEmpty("全宗id不能为空",asc.getQzId());
        cq.eq("qzId");
        cq.eq("unitId",asc.getUnitId());
        cq.like("storeName",asc.getStoreName());
        cq.like("checkTime",asc.getCheckTime());
        cq.like("temperature",asc.getTemperature());
        cq.like("humidity",asc.getHumidity());
        cq.like("remark",asc.getRemark());
    //过滤用户负责机构的数据
        List<Long> unitIds = userUnitService.getLoginUserUnitIds(asc.getQzId(),null);
        if(FlymeUtils.isNotEmpty(unitIds)){
            cq.lambda().in(ArcStoreTemperature::getUnitId,unitIds);
        }
        String begin = requestMap.get("checkTimeBegin");
        String end = requestMap.get("checkTimeEnd");
        if(FlymeUtils.isNotEmpty(begin)){
            cq.ge("checkTime",begin);
        }
        if(FlymeUtils.isNotEmpty(end)){
            cq.le("checkTime",end);
        }
        Object temperatureIds = requestMap.get("temperatureIds");
        if (FlymeUtils.isNotEmpty(temperatureIds)){
            List<Long> ids = Arrays.asList(temperatureIds.toString().split(",")).stream().map(item->{
                return Long.parseLong(item);
            }).collect(Collectors.toList());
            cq.lambda().in(ArcStoreTemperature::getTemperatureId,ids);
        }
        cq.orderByDesc("ast.createTime");
        return ResultBody.ok();
    }
}
