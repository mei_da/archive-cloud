package com.meida.module.arc.provider.log.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 档案相关枚举
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum LogTypeEnum {
    LOG_LOGIN(0,"系统登录日志"),
    LOG_OPERATE(1,"操作日志");

    LogTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static LogTypeEnum matchCode(Integer code){
        for(LogTypeEnum obj: LogTypeEnum.values()){
            if(obj.getCode().equals(code)){
                return obj;
            }
        }
        return null;
    }


     public static LogTypeEnum getValue(int code){
        for (LogTypeEnum color: values()) {
             if(color.getCode().equals(code)){
                     return  color;
             }
         }
         return null;
    }
}
