package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcInfoRemind;

import java.util.Map;

/**
 * 档案到期提醒 接口
 *
 * @author flyme
 * @date 2023-07-24
 */
public interface ArcInfoRemindService extends IBaseService<ArcInfoRemind> {

    ResultBody saves(Map params);

    Long getArcInfoCollect(Long arcInfoId);
}
