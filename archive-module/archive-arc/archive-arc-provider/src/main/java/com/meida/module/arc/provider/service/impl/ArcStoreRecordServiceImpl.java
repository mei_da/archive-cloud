package com.meida.module.arc.provider.service.impl;

import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcStoreCheck;
import com.meida.module.arc.client.entity.ArcStoreRecord;
import com.meida.module.arc.client.entity.ArcStoreRoom;
import com.meida.module.arc.provider.mapper.ArcStoreRecordMapper;
import com.meida.module.arc.provider.service.ArcStoreRecordService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.provider.service.UserUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.transaction.annotation.Propagation;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 出入库记录接口实现类
 *
 * @author flyme
 * @date 2022-01-03
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcStoreRecordServiceImpl extends BaseServiceImpl<ArcStoreRecordMapper, ArcStoreRecord> implements ArcStoreRecordService {

    @Autowired
    private UserUnitService userUnitService;
    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcStoreRecord asr, EntityMap extra) {
        ApiAssert.isNotEmpty("所属部门不能为空",asr);
        ApiAssert.isNotEmpty("所属部门不能为空",asr.getUnitId());
        ApiAssert.isNotEmpty("全宗id不能为空",asr.getQzId());
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeEdit(CriteriaUpdate<ArcStoreRecord> cu, ArcStoreRecord asr, EntityMap extra) {
        ApiAssert.isNotEmpty("编辑对象不能为空",asr);
        ApiAssert.isNotEmpty("编辑对象不能为空",asr.getRecordId());
        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcStoreRecord> cq, ArcStoreRecord asr, EntityMap requestMap) {
        ApiAssert.isNotEmpty("全宗id不能为空",asr.getQzId());
        cq.eq("qzId");
        cq.eq("unitId",asr.getUnitId());
        cq.like("handlerPerson",asr.getHandlerPerson());
        cq.like("arcNum",asr.getArcNum());
        cq.like("arcType",asr.getArcType());
        cq.eq("operatorType",asr.getOperatorType());
        cq.like("reason",asr.getReason());
        cq.like("operatorName",asr.getOperatorName());
        cq.like("remark",asr.getRemark());
        cq.like("operatorTime",asr.getOperatorTime());
        String begin = requestMap.get("operatorTimeBegin");
        String end = requestMap.get("operatorTimeEnd");
        cq.orderByDesc("asr.createTime");
        //过滤用户负责机构的数据
        List<Long> unitIds = userUnitService.getLoginUserUnitIds(asr.getQzId(),null);
        if(FlymeUtils.isNotEmpty(unitIds)){
            cq.lambda().in(ArcStoreRecord::getUnitId,unitIds);
        }
        if(FlymeUtils.isNotEmpty(begin)){
            cq.ge("operatorTime",begin);
        }
        if(FlymeUtils.isNotEmpty(end)){
            cq.le("operatorTime",end);
        }
        Object recordIds = requestMap.get("recordIds");
        if (FlymeUtils.isNotEmpty(recordIds)){
            List<Long> ids = Arrays.asList(recordIds.toString().split(",")).stream().map(item->{
                return Long.parseLong(item);
            }).collect(Collectors.toList());
            cq.lambda().in(ArcStoreRecord::getRecordId,ids);
        }
        return ResultBody.ok();
    }
}
