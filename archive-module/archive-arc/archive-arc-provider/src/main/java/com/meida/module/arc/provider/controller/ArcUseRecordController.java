package com.meida.module.arc.provider.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcUseRecord;
import com.meida.module.arc.provider.service.ArcUseRecordService;


/**
 * 档案借阅记录控制器
 *
 * @author flyme
 * @date 2022-01-06
 */
@RestController
@RequestMapping("/arc/use/record/")
public class ArcUseRecordController extends BaseController<ArcUseRecordService, ArcUseRecord>  {

    @ApiOperation(value = "档案借阅记录-分页列表", notes = "档案借阅记录分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "档案借阅记录-列表", notes = "档案借阅记录列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "档案借阅记录-添加", notes = "添加档案借阅记录")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "档案借阅记录-更新", notes = "更新档案借阅记录")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "档案借阅记录-删除", notes = "删除档案借阅记录")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "档案借阅记录-详情", notes = "档案借阅记录详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "档案借阅记录-借阅变更", notes = "档案借阅记录-借阅变更")
    @PostMapping(value = "changeStatus")
    public ResultBody changeStatus(@RequestParam(required = false) Map params) {

        return bizService.changeStatus(params);
    }

    @ApiOperation(value = "档案借阅记录-审核", notes = "档案借阅记录-审核")
    @PostMapping(value = "verify")
    public ResultBody verify(@RequestParam(required = false) Map params) {

        return bizService.verify(params);
    }

    @ApiOperation(value = "档案借阅目的-统计", notes = "档案借阅目的-统计核")
    @PostMapping(value = "usePurposeStatistics")
    public ResultBody usePurposeStatistics(@RequestParam(required = false) Map params) {

        return bizService.usePurposeStatistics(params);
    }

    @ApiOperation(value = "档案借阅目的-统计", notes = "档案借阅目的-统计核")
    @GetMapping(value = "useStatistics")
    public ResultBody useStatistics(@RequestParam(required = false) Map params) {

        return bizService.useStatistics(params);
    }

}
