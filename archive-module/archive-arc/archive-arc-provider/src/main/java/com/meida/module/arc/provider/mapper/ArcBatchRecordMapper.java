package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcBatchRecord;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 批量关联记录 Mapper 接口
 * @author flyme
 * @date 2021-12-20
 */
public interface ArcBatchRecordMapper extends SuperMapper<ArcBatchRecord> {

}
