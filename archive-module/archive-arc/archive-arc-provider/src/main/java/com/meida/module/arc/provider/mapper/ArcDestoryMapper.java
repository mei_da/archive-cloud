package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcDestory;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 档案销毁 Mapper 接口
 * @author flyme
 * @date 2022-01-11
 */
public interface ArcDestoryMapper extends SuperMapper<ArcDestory> {

}
