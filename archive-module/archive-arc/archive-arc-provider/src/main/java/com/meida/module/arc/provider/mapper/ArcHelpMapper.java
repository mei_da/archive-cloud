package com.meida.module.arc.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.arc.client.entity.ArcHelp;

/**
 * 帮助信息 Mapper 接口
 * @author flyme
 * @date 2021-12-25
 */
public interface ArcHelpMapper extends SuperMapper<ArcHelp> {

}
