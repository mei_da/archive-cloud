package com.meida.module.arc.provider.controller;

import com.meida.common.base.entity.EntityMap;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcSecondField;
import com.meida.module.arc.provider.service.ArcSecondFieldService;


/**
 * 二级分类字段配置控制器
 *
 * @author flyme
 * @date 2022-08-07
 */
@RestController
@RequestMapping("/arc/second/field/")
public class ArcSecondFieldController extends BaseController<ArcSecondFieldService, ArcSecondField>  {

    @ApiOperation(value = "二级分类字段配置-分页列表", notes = "二级分类字段配置分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "二级分类字段配置-列表", notes = "二级分类字段配置列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "二级分类字段配置-添加", notes = "添加二级分类字段配置")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "二级分类字段配置-更新", notes = "更新二级分类字段配置")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "二级分类字段配置-添加修改", notes = "添加修改")
    @PostMapping(value = "saveOrUpdate")
    public ResultBody saveOrUpdate(@RequestParam(required = false) Map params) {
        return bizService.saveOrUpdate(new EntityMap(params));
    }

    @ApiOperation(value = "二级分类字段配置-删除", notes = "删除二级分类字段配置")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "二级分类字段配置-详情", notes = "二级分类字段配置详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "门类-上下移动", notes = "们配排序-上下移动")
    @GetMapping(value = "upOrDown")
    public ResultBody upOrDown(@RequestParam(required = false) Map params) {
        return bizService.upOrDown(params);
    }



}
