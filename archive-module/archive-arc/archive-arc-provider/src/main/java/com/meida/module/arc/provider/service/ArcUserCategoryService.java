package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcUserCategory;

import java.util.Map;

/**
 * 个人档案查询 接口
 *
 * @author flyme
 * @date 2023-07-19
 */
public interface ArcUserCategoryService extends IBaseService<ArcUserCategory> {
    /**
     * description: 个人档案查询设置上下移动
     * date: 2023年-07月-19日 11:05
     * author: ldd
     *
     * @param params
     * @return com.meida.common.mybatis.model.ResultBody
     */
    ResultBody upOrDown(Map params);
}
