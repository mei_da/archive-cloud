package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcCategorySuffix;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 门类表名后缀 Mapper 接口
 * @author flyme
 * @date 2021-11-22
 */
public interface ArcCategorySuffixMapper extends SuperMapper<ArcCategorySuffix> {

}
