package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcStoreRecord;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 出入库记录 Mapper 接口
 * @author flyme
 * @date 2022-01-03
 */
public interface ArcStoreRecordMapper extends SuperMapper<ArcStoreRecord> {

}
