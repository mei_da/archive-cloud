package com.meida.module.arc.provider.service.impl;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.module.arc.client.entity.ArcCategorySuffix;
import com.meida.module.arc.provider.mapper.ArcCategorySuffixMapper;
import com.meida.module.arc.provider.service.ArcCategorySuffixService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 门类表名后缀接口实现类
 *
 * @author flyme
 * @date 2021-11-22
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class  ArcCategorySuffixServiceImpl extends BaseServiceImpl<ArcCategorySuffixMapper, ArcCategorySuffix> implements ArcCategorySuffixService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcCategorySuffix acs, EntityMap extra) {
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcCategorySuffix> cq, ArcCategorySuffix acs, EntityMap requestMap) {
        cq.orderByDesc("acs.createTime");
        return ResultBody.ok();
    }

    @Override
    public void delCategory(Long categoryId) {
        CriteriaQuery<ArcCategorySuffix> fdQuery = new CriteriaQuery<>(ArcCategorySuffix.class);
        fdQuery.lambda().eq(ArcCategorySuffix::getCategoryId, categoryId);
        this.baseMapper.delete(fdQuery);
    }
}
