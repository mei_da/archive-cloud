package com.meida.module.arc.provider.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcUse;
import com.meida.module.arc.provider.service.ArcUseService;


/**
 * 档案利用库控制器
 *
 * @author flyme
 * @date 2022-01-06
 */
@RestController
@RequestMapping("/arc/use/")
public class ArcUseController extends BaseController<ArcUseService, ArcUse>  {

    @ApiOperation(value = "档案利用库-分页列表", notes = "档案利用库分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "档案利用库-列表", notes = "档案利用库列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "档案利用库-添加", notes = "添加档案利用库")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "档案利用库-更新", notes = "更新档案利用库")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "档案利用库-删除", notes = "删除档案利用库")
    @GetMapping(value = "remove")
    public ResultBody remove(@RequestParam String useIds) {
        return bizService.remove(useIds);
    }


    @ApiOperation(value = "档案利用库-详情", notes = "档案利用库详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

}
