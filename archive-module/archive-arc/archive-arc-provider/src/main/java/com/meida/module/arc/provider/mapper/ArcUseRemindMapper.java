package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcUseRemind;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 借阅消息提醒 Mapper 接口
 * @author flyme
 * @date 2022-01-07
 */
public interface ArcUseRemindMapper extends SuperMapper<ArcUseRemind> {

}
