package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcConfigRef;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.Map;

/**
 * 归档配置关联 接口
 *
 * @author flyme
 * @date 2021-11-25
 */
public interface ArcConfigRefService extends IBaseService<ArcConfigRef> {
    ResultBody saveOrUpdate(Map var1);
}
