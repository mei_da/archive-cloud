package com.meida.module.arc.provider.service;

import com.meida.module.arc.client.entity.ArcBatch;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 批量关联 接口
 *
 * @author flyme
 * @date 2021-12-20
 */
public interface ArcBatchService extends IBaseService<ArcBatch> {

}
