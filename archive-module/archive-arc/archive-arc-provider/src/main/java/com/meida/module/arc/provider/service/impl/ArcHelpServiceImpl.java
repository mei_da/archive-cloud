package com.meida.module.arc.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcDb;
import com.meida.module.arc.client.entity.ArcHelp;
import com.meida.module.arc.provider.mapper.ArcDbMapper;
import com.meida.module.arc.provider.mapper.ArcHelpMapper;
import com.meida.module.arc.provider.service.ArcDbRecordService;
import com.meida.module.arc.provider.service.ArcDbService;
import com.meida.module.arc.provider.service.ArcHelpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 帮助信息
 *
 * @author flyme
 * @date 2021-12-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcHelpServiceImpl extends BaseServiceImpl<ArcHelpMapper, ArcHelp> implements ArcHelpService {


    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcHelp t, EntityMap extra) {
        ApiAssert.isNotEmpty("帮助信息不能为空",t);
        ApiAssert.isNotEmpty("帮助标识不能为空",t.getSysCode());
        QueryWrapper<ArcHelp> queryWrapper = new QueryWrapper<ArcHelp>();
        queryWrapper.lambda().eq(ArcHelp::getSysCode,t.getSysCode());
        Long count = this.count(queryWrapper);
        if(FlymeUtils.isNotEmpty(count)&&count>0){
            ApiAssert.failure("帮助标识不能重复");
        }
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeEdit(CriteriaUpdate<ArcHelp> cu, ArcHelp t, EntityMap extra) {
        ApiAssert.isNotEmpty("帮助信息不能为空",t);
        ApiAssert.isNotEmpty("帮助信息不能为空",t.getHelpId());
        if(FlymeUtils.isNotEmpty(t.getSysCode())){
            ArcHelp help = this.getById(t.getHelpId());
            if(!t.getSysCode().equals(help.getSysCode())){
                QueryWrapper<ArcHelp> queryWrapper = new QueryWrapper<ArcHelp>();
                queryWrapper.lambda().eq(ArcHelp::getSysCode,t.getSysCode())
                        .ne(ArcHelp::getHelpId,help.getHelpId());
                Long count = this.count(queryWrapper);
                if(FlymeUtils.isNotEmpty(count)&&count>0){
                    ApiAssert.failure("帮助标识不能重复");
                }
            }
        }
        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcHelp> cq, ArcHelp db, EntityMap requestMap) {
      cq.orderByDesc("arcHelp.createTime");
      return ResultBody.ok();
    }


    @Override
    public ResultBody<ArcHelp> getHelpBySysCode(Map params) {
        Object sysCode = params.get("sysCode");
        ApiAssert.isNotEmpty("帮助标识不能为空",sysCode);
        QueryWrapper<ArcHelp> queryWrapper = new QueryWrapper<ArcHelp>();
        queryWrapper.lambda().eq(ArcHelp::getSysCode,sysCode.toString());
        List<ArcHelp> list = this.list(queryWrapper);
        if(FlymeUtils.isEmpty(list)){
            return ResultBody.failed("没有匹配的帮助标识");
        }
        if(list.size()>1){
            return ResultBody.failed("帮助标识对应多条数据，请联系管理员");
        }
        return ResultBody.ok(list.get(0));
    }
}
