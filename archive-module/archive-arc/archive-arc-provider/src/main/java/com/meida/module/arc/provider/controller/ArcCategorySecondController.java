package com.meida.module.arc.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcCategorySecond;
import com.meida.module.arc.provider.service.ArcCategorySecondService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 二级分类控制器
 *
 * @author flyme
 * @date 2022-08-07
 */
@RestController
@RequestMapping("/arc/category/second/")
public class ArcCategorySecondController extends BaseController<ArcCategorySecondService, ArcCategorySecond>  {

    @ApiOperation(value = "二级分类-分页列表", notes = "二级分类分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "二级分类-列表", notes = "二级分类列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
        //return bizService.list4Redis(params);

    }

    @ApiOperation(value = "二级分类-添加", notes = "添加二级分类")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "二级分类-更新", notes = "更新二级分类")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "二级分类-删除", notes = "删除二级分类")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "二级分类-详情", notes = "二级分类详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "门类-上下移动", notes = "们配排序-上下移动")
    @GetMapping(value = "upOrDown")
    public ResultBody upOrDown(@RequestParam(required = false) Map params) {
        return bizService.upOrDown(params);
    }

}
