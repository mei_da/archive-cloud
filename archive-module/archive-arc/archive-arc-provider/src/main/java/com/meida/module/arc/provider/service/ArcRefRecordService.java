package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcRefRecord;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.Map;

/**
 * 关联记录 接口
 *
 * @author flyme
 * @date 2022-09-05
 */
public interface ArcRefRecordService extends IBaseService<ArcRefRecord> {
    public ResultBody getArcRefStatistics(Map param);

    public ResultBody batchSave(Map param);

    public ResultBody clearRef(Map param);

    public void caculateArcRefCount(Long arcInfoId,Long categoryId);
}
