package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcUse;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 档案利用库 Mapper 接口
 * @author flyme
 * @date 2022-01-06
 */
public interface ArcUseMapper extends SuperMapper<ArcUse> {

}
