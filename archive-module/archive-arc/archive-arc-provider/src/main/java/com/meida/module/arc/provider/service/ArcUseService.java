package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcInfo;
import com.meida.module.arc.client.entity.ArcUse;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 档案利用库 接口
 *
 * @author flyme
 * @date 2022-01-06
 */
public interface ArcUseService extends IBaseService<ArcUse> {

    void addArcUse(List<ArcInfo> arcInfos);

    ResultBody remove(String useIds);

}
