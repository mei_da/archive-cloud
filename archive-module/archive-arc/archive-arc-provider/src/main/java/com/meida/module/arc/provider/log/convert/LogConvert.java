package com.meida.module.arc.provider.log.convert;

import com.meida.module.arc.client.entity.ArcLog;

/**
 * <b>功能名：LogConvert</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-01-23 jiabing
 */
public interface LogConvert {

    boolean convert(Object[] args, ArcLog log);
}   