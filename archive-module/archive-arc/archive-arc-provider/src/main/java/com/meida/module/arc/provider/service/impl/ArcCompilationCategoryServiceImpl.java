package com.meida.module.arc.provider.service.impl;

import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.mybatis.vo.JoinBean;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.client.entity.ArcCompilationCategory;
import com.meida.module.arc.client.entity.ArcCompilationRef;
import com.meida.module.arc.client.entity.ArcStoreRecord;
import com.meida.module.arc.provider.mapper.ArcCompilationCategoryMapper;
import com.meida.module.arc.provider.service.ArcCategoryService;
import com.meida.module.arc.provider.service.ArcCompilationCategoryService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.provider.service.ArcCompilationRefService;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.transaction.annotation.Propagation;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

/**
 * 素材类型接口实现类
 *
 * @author flyme
 * @date 2022-01-04
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcCompilationCategoryServiceImpl extends BaseServiceImpl<ArcCompilationCategoryMapper, ArcCompilationCategory> implements ArcCompilationCategoryService {

    @Autowired
    private ArcCompilationRefService arcCompilationRefService;

    @Autowired
    private ArcCategoryService arcCategoryService;

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcCompilationCategory acc, EntityMap extra) {
        ApiAssert.isNotEmpty("名称不能为空",acc);
        ApiAssert.isNotEmpty("名称不能为空",acc.getName());
        ApiAssert.isNotEmpty("全宗id不能为空",acc.getQzId());
        CriteriaQuery<ArcCompilationCategory> query = new CriteriaQuery<ArcCompilationCategory>(ArcCompilationCategory.class);
        query.limit(1);
        query.orderByDesc("seq");
        ArcCompilationCategory ca = getOne(query);
        if(FlymeUtils.isEmpty(ca)){
            acc.setSeq(0);
        }else{
            acc.setSeq(ca.getSeq()+1);
        }
        acc.setParentId(0L);

        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeEdit(CriteriaUpdate<ArcCompilationCategory> cu, ArcCompilationCategory acc, EntityMap extra) {
        ApiAssert.isNotEmpty("编辑对象不能为空",acc);
        ApiAssert.isNotEmpty("编辑对象不能为空",acc.getCompilationId());
        return ResultBody.ok();
    }

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<ArcCompilationCategory> cq, ArcCompilationCategory t, EntityMap requestMap) {
        ApiAssert.isNotEmpty("全宗id不能为空",t.getQzId());
        //cq.like(FlymeUtils.isNotEmpty(t.getName()),"name",t.getName());
        cq.orderByAsc("seq");
        return ResultBody.ok();
    }
    @Override
    public ResultBody beforeDelete(CriteriaDelete<ArcCompilationCategory> cd) {
        ApiAssert.isNotEmpty("删除对象为空",cd.getIdValue());
        CriteriaQuery<ArcCompilationRef> query = new CriteriaQuery<ArcCompilationRef>(ArcCompilationRef.class);
        query.lambda().eq(ArcCompilationRef::getCompilationId,cd.getIdValue());
        Long count = this.arcCompilationRefService.count(query);
        if(count>0){
            ApiAssert.failure("已关联素材，请先清除素材再删除");
        }
        return ResultBody.ok();
    }




    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcCompilationCategory> cq, ArcCompilationCategory acc, EntityMap requestMap) {
        ApiAssert.isNotEmpty("全宗id不能为空",acc.getQzId());
        cq.eq("qzId");
      cq.orderByDesc("acc.createTime");
      return ResultBody.ok();
    }

}
