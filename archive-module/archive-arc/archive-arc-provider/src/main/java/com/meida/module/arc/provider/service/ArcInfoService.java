package com.meida.module.arc.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.module.arc.client.entity.ArcField;
import com.meida.module.arc.client.entity.ArcInfo;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * 档案信息表 接口
 *
 * @author flyme
 * @date 2021-12-08
 */
public interface ArcInfoService extends IBaseService<ArcInfo> {

    /**
     * 归档类型变更
     *
     * @param param
     * @return
     */
    ResultBody arcStatusChange(Map param);

    ArcInfo getArcInfo(Long arcInfoId, Long categoryId);

    /**
     * 根据档号和门类ID查询档案
     * @param arcNo
     * @param categoryId
     * @return
     */
    ArcInfo getArcInfoByArcNoAndCateGoryId(String arcNo, Long categoryId);

    /**
     * 档案批量修改
     *
     * @param param
     * @return
     */
    ResultBody batchUpdate(Map param);

    /**
     * 生成件号
     *
     * @param param
     * @return
     */
    ResultBody genPieceNo(Map param);


    /**
     * 创建档案初始化默认字段
     *
     * @param arcInfo
     */
    public void initArcInfoPreField(ArcInfo arcInfo);

    public Object changeFieldValueType(String type, Object value);

    String getFieldType(Field field, String defaultType);

    public ResultBody removeCompilation(String arcInfoIds, Long categoryId);

    /**
     * 更新原文数量
     *
     * @param categoryId
     * @param arcInfoId
     * @param count
     * @return
     */
    boolean updateOriginalCount(String categoryId, String arcInfoId, Integer count);

    boolean updateCommentCount(Long categoryId, Long arcInfoId, Long count);

    /**
     * 更新原文数量
     *
     * @param categoryId
     * @param arcInfoId
     * @return
     */
    boolean updateOriginalCount(String categoryId, String arcInfoId);

    /**

     */
    ResultBody destoryBack(Map param);

    /**
     * 批量调入调出
     * @param param
     * @return
     */
    ResultBody batchInOrOut(Map param);

    /**
     * 批量更新档号
     * @param param
     * @return
     */
    ResultBody batchUpdateArcNo(Map param);


    public ResultBody allCategoryQuery(Map param);



    public Map buildAllArcInfoQuery(CriteriaQuery<ArcInfo> cq, Long categoryId, String queryStr, EntityMap param);

    ArcInfo getArcInfoByField(ArcField arcField,Long userId, String val, Long categoryId,Boolean isLike);


    ResultBody espageList(Map params);

    ResultBody esCategoryQuery(Map params);
}
