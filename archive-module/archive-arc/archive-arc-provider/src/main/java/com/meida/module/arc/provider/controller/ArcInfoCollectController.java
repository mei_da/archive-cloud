package com.meida.module.arc.provider.controller;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcInfoCollect;
import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import com.meida.module.arc.provider.service.ArcInfoCollectService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 档案收藏控制器
 *
 * @author flyme
 * @date 2023-05-29
 */
@RestController
@RequestMapping("/arc/aic/")
public class ArcInfoCollectController extends BaseController<ArcInfoCollectService, ArcInfoCollect> {

    @ApiOperation(value = "档案收藏-分页列表", notes = "档案收藏分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "档案收藏-列表", notes = "档案收藏列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "档案收藏-添加", notes = "添加档案收藏")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "档案收藏-更新", notes = "更新档案收藏")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "档案收藏-删除", notes = "删除档案收藏")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "档案收藏-详情", notes = "档案收藏详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "档案-收藏", notes = "档案收藏")
    @PostMapping(value = "collect")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optType = LogOptTypeEnum.PREARC_ARC, optObj = "档案收藏")
    public ResultBody updateCollect(@RequestParam(required = false) Map params) {
        return bizService.updateCollect(params);
    }
}
