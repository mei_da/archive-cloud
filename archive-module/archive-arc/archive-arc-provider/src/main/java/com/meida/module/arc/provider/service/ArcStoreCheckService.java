package com.meida.module.arc.provider.service;

import com.meida.module.arc.client.entity.ArcStoreCheck;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 库房检查 接口
 *
 * @author flyme
 * @date 2022-01-03
 */
public interface ArcStoreCheckService extends IBaseService<ArcStoreCheck> {

}
