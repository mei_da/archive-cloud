package com.meida.module.arc.provider.service;

import com.meida.module.arc.client.entity.ArcFieldSys;
import com.meida.common.mybatis.base.service.IBaseService;

/**
 * 系统字段表 接口
 *
 * @author flyme
 * @date 2021-11-22
 */
public interface ArcFieldSysService extends IBaseService<ArcFieldSys> {

}
