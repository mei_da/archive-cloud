package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcInfo;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 档案信息表 Mapper 接口
 * @author flyme
 * @date 2021-12-08
 */
public interface ArcInfoMapper extends SuperMapper<ArcInfo> {

}
