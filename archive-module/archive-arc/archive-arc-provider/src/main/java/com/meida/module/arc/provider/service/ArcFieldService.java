package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.client.entity.ArcField;

import java.util.List;
import java.util.Map;

/**
 * 字段配置表 接口
 *
 * @author flyme
 * @date 2021-11-22
 */
public interface ArcFieldService extends IBaseService<ArcField> {

    void batchAddArcField(Long categoryId);

    ResultBody upOrDown(Map var1);

    List<ArcField> listByCategoryId(Long categoryId);

    ArcField getArcField(Long categoryId);

    void copyArcCategoryField(ArcCategory srcCategory,ArcCategory copyCategory);

    void delArcField(Long categoryId);
}
