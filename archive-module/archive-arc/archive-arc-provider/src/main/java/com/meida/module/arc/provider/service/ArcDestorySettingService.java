package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcDestorySetting;
import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.module.arc.client.entity.ArcInfo;

import java.util.List;
import java.util.Map;

/**
 * 销毁配置 接口
 *
 * @author flyme
 * @date 2022-01-11
 */
public interface ArcDestorySettingService extends IBaseService<ArcDestorySetting> {

    ResultBody defaultSetting(Map param);

}
