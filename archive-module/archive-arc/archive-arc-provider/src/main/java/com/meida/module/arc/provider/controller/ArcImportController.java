package com.meida.module.arc.provider.controller;

import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.DateUtil;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.convert.impl.ArcOptLogConvert;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.Date;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcImport;
import com.meida.module.arc.provider.service.ArcImportService;
import org.springframework.web.multipart.MultipartFile;


/**
 * excel导入记录控制器
 *
 * @author flyme
 * @date 2021-12-26
 */
@RestController
@RequestMapping("/arc/import/")
public class ArcImportController extends BaseController<ArcImportService, ArcImport>  {

    @ApiOperation(value = "excel导入记录-分页列表", notes = "excel导入记录分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "excel导入记录-列表", notes = "excel导入记录列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "excel导入记录-添加", notes = "添加excel导入记录")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "excel导入记录-更新", notes = "更新excel导入记录")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "excel导入记录-删除", notes = "删除excel导入记录")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "excel导入记录-详情", notes = "excel导入记录详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "excel导入记录-上传文件", notes = "excel导入记录-上传文件")
    @RequestMapping(value = "upload", method = RequestMethod.POST)
    public ResultBody fileUpload(@RequestParam(value = "file") MultipartFile file) {
        return this.bizService.fileUpload(file);
    }

    @ApiOperation(value = "excel导入记录-上传校验", notes = "excel导入记录-上传校验")
    @RequestMapping(value = "check", method = RequestMethod.POST)
    public ResultBody check(@RequestParam(value = "filePath") String filePath
            , @RequestParam(value = "categoryId") Long categoryId
            , @RequestParam(value = "unitId") Long unitId
            , @RequestParam(value = "config") String config                ) {
        return this.bizService.check(filePath, categoryId, unitId,config);
    }

    @ApiOperation(value = "excel导入记录-上传", notes = "excel导入记录-上传")
    @RequestMapping(value = "doImport", method = RequestMethod.POST)
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.IMPORT,optObj="excel导入")
    public ResultBody doImport(@RequestParam(value = "filePath") String filePath
            , @RequestParam(value = "categoryId") Long categoryId
            , @RequestParam(value = "unitId") Long unitId
            , @RequestParam(value = "config") String config                ) {
        return this.bizService.doImport(filePath, categoryId, unitId,config);
    }

    @ApiOperation(value = "excel导入记录-回朔", notes = "excel导入记录回朔")
    @GetMapping(value = "back")
    public ResultBody back(@RequestParam(value = "importId") Long importId) {
        return bizService.back(importId);
    }

    /**
     * 导出模板
     *
     * @param params
     * @param request
     * @param response
     */
    @GetMapping(value = "exportTmp")
    public void exportTmp(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        Object categoryId = params.get("categoryId");
        ApiAssert.isNotEmpty("门类id不能为空",categoryId);
        bizService.exportTmp(Long.parseLong(categoryId.toString()),request,response);
    }

}
