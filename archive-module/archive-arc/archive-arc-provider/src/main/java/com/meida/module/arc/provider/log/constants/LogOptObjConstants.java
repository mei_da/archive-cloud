package com.meida.module.arc.provider.log.constants;

/**
 * <b>功能名：LogOptObjConstants</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-02-03 jiabing
 */
public class LogOptObjConstants {

    public static final String ARC_ADD = "档案添加";

    public static final String ARC_DEL = "档案删除";

    public static final String ARC_QUERY = "档案查询";

    public static final String ARC_UPDATE = "档案更新";

}   