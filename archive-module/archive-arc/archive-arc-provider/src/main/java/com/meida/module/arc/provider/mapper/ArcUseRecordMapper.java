package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcUseRecord;
import com.meida.common.mybatis.base.mapper.SuperMapper;

import java.util.List;
import java.util.Map;

/**
 * 档案借阅记录 Mapper 接口
 * @author flyme
 * @date 2022-01-06
 */
public interface ArcUseRecordMapper extends SuperMapper<ArcUseRecord> {
    public List<Map> usePurposeStatistics(Map param);
}
