package com.meida.module.arc.provider.log.enums.bizenums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 档案操作类型枚举
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ArcOptObjEnum {
    NULL(0,"待定"),
    PREARC_PRE(1,"预归档-文件级"),
    PREARC_ARC(2,"预归档-档案管理");

    ArcOptObjEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static ArcOptObjEnum matchCode(Integer code){
        for(ArcOptObjEnum obj: ArcOptObjEnum.values()){
            if(obj.getCode().equals(code)){
                return obj;
            }
        }
        return null;
    }


     public static ArcOptObjEnum getValue(int code){
        for (ArcOptObjEnum color: values()) {
             if(color.getCode().equals(code)){
                     return  color;
             }
         }
         return null;
    }
}
