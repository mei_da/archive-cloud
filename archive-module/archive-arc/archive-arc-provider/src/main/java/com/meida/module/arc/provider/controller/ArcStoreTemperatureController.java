package com.meida.module.arc.provider.controller;

import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.DateUtil;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.Date;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcStoreTemperature;
import com.meida.module.arc.provider.service.ArcStoreTemperatureService;


/**
 * 温湿度记录控制器
 *
 * @author flyme
 * @date 2022-01-03
 */
@RestController
@RequestMapping("/arc/store/temperature/")
public class ArcStoreTemperatureController extends BaseController<ArcStoreTemperatureService, ArcStoreTemperature>  {

    @ApiOperation(value = "温湿度记录-分页列表", notes = "温湿度记录分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "温湿度记录-列表", notes = "温湿度记录列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "温湿度记录-添加", notes = "添加温湿度记录")
    @PostMapping(value = "save")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.STORETEMP,optObj="添加温湿度记录")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "温湿度记录-更新", notes = "更新温湿度记录")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "温湿度记录-删除", notes = "删除温湿度记录")
    @PostMapping(value = "remove")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.STORETEMP,optObj="删除温湿度记录")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "温湿度记录-详情", notes = "温湿度记录详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    /**
     * 导出档案统计
     *
     * @param params
     * @param request
     * @param response
     */
    @GetMapping(value = "export")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.STORETEMP,optObj="导出温湿度记录")
    public void export(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute("reqParam",params);
        ApiAssert.isNotEmpty("全宗id不能为空",params.get("qzId"));
        params.put("columns","[\n" +
                "    {\n" +
                "        \"key\": \"unitId\",\n" +
                "        \"name\": \"所属部门\",\n" +
                "        \"dict\": \"unitId\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"storeName\",\n" +
                "        \"name\": \"库房名称\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"checkTime\",\n" +
                "        \"name\": \"检查时间\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"temperature\",\n" +
                "        \"name\": \"温度（°C）\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"humidity\",\n" +
                "        \"name\": \"湿度（%RH）\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"remark\",\n" +
                "        \"name\": \"备注\"\n" +
                "    }\n" +
                "]");
        bizService.export(null,params, request, response, "温湿度记录"+ DateUtil.date2Str(new Date(),"yyyyMMddHHmmss"), "温湿度记录","arcStoreTemperatureExportHandler");
    }

}
