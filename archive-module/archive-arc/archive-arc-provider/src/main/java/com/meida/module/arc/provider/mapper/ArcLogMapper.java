package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcLog;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 日志 Mapper 接口
 * @author flyme
 * @date 2022-01-25
 */
public interface ArcLogMapper extends SuperMapper<ArcLog> {

}
