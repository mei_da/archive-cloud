package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcStoreRoom;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.Map;

/**
 * 档案库房 接口
 *
 * @author flyme
 * @date 2022-01-02
 */
public interface ArcStoreRoomService extends IBaseService<ArcStoreRoom> {

    ResultBody upOrDown(Map var1);

    //校验是否可以入库
    //如果库存状态是空，则更新为有数据但是未满
    String checkStoreAndChangeIsFull(Long storeId);

    ResultBody moveout(Map var1);

}
