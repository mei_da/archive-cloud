package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcFieldSys;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 系统字段表 Mapper 接口
 * @author flyme
 * @date 2021-11-22
 */
public interface ArcFieldSysMapper extends SuperMapper<ArcFieldSys> {

}
