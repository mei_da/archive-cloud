package com.meida.module.arc.provider.service;

import com.meida.common.mybatis.base.service.IBaseService;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcUseRemind;

import java.util.Map;

/**
 * 借阅消息提醒 接口
 *
 * @author flyme
 * @date 2022-01-07
 */
public interface ArcUseRemindService extends IBaseService<ArcUseRemind> {

    /**
     * description: 生成消息记录
     * date: 2023年-07月-25日 16:30
     * author: ldd
     *
     * @param type
     * @return void
     */
    void createRemid(int type, String title, Long qzId, Long userId, Long verifyUserId, Long arcInfoId);

    void createRemid(int type, String title, String content, Long qzId, Long userId, Long verifyUserId, Long arcInfoId);

    /**
     * description: 全部已读
     * date: 2023年-07月-26日 11:20
     * author: ldd
     *
     * @param params
     * @return com.meida.common.mybatis.model.ResultBody
     */
    ResultBody allRead(Map params);

    ResultBody allUnRead(Map params);
}
