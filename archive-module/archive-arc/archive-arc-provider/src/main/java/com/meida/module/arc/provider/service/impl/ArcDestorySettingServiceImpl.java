package com.meida.module.arc.provider.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcDestorySetting;
import com.meida.module.arc.client.enums.ArchiveEnumInteger;
import com.meida.module.arc.provider.mapper.ArcDestorySettingMapper;
import com.meida.module.arc.provider.service.ArcDestorySettingService;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import org.springframework.stereotype.Service;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.query.CriteriaQuery;
import org.springframework.transaction.annotation.Propagation;
import com.meida.common.mybatis.query.CriteriaSave;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 销毁配置接口实现类
 *
 * @author flyme
 * @date 2022-01-11
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcDestorySettingServiceImpl extends BaseServiceImpl<ArcDestorySettingMapper, ArcDestorySetting> implements ArcDestorySettingService {

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcDestorySetting ads, EntityMap extra) {
        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcDestorySetting> cq, ArcDestorySetting ads, EntityMap requestMap) {
      cq.orderByDesc("ads.createTime");
      return ResultBody.ok();
    }

    @Override
    public ResultBody beforeEdit(CriteriaUpdate<ArcDestorySetting> cu, ArcDestorySetting t, EntityMap extra) {
        ApiAssert.isNotEmpty("编辑对象不能为空",t);
        ApiAssert.isNotEmpty("全宗id不能为空",t.getDestorySettingId());
        return ResultBody.ok();
    }

    @Override
    public ResultBody defaultSetting(Map param) {
        ApiAssert.isNotEmpty("全宗id不能为空",param);
        Object qzId = param.get("qzId");
        ApiAssert.isNotEmpty("全宗id不能为空",qzId);
        LambdaQueryWrapper<ArcDestorySetting> settingLambdaQueryWrapper = new LambdaQueryWrapper<>();
        settingLambdaQueryWrapper.eq(ArcDestorySetting::getQzId,Long.parseLong(qzId.toString()));
        List<ArcDestorySetting> list = this.list(settingLambdaQueryWrapper);
        if(FlymeUtils.isEmpty(list)){
            ArcDestorySetting setting = new ArcDestorySetting();
            setting.setHoldBackDay(30);
            setting.setQzId(Long.parseLong(qzId.toString()));
            setting.setIsThorough(ArchiveEnumInteger.IS_FALSE.getCode());
            setting.setAutoDestoryType(1);
            setting.setAutoDestoryTime(12);
            this.save(setting);
            return ResultBody.ok(setting);
        }else{
            if(list.size()>1){
                for(int i=1;i<list.size();i++){
                    this.removeById(list.get(i).getDestorySettingId());
                }
                return ResultBody.ok(list.get(0));
            }else{
                return ResultBody.ok(list.get(0));
            }
        }
    }
}
