package com.meida.module.arc.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.arc.client.entity.ArcInfoComment;

/**
 * 档案评论 Mapper 接口
 *
 * @author flyme
 * @date 2023-07-24
 */
public interface ArcInfoCommentMapper extends SuperMapper<ArcInfoComment> {

}
