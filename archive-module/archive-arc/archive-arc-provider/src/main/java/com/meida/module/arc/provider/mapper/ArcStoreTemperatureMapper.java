package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcStoreTemperature;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 温湿度记录 Mapper 接口
 * @author flyme
 * @date 2022-01-03
 */
public interface ArcStoreTemperatureMapper extends SuperMapper<ArcStoreTemperature> {

}
