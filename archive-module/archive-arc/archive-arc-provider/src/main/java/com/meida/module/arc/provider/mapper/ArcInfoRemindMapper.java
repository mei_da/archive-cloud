package com.meida.module.arc.provider.mapper;

import com.meida.common.mybatis.base.mapper.SuperMapper;
import com.meida.module.arc.client.entity.ArcInfoRemind;

/**
 * 档案到期提醒 Mapper 接口
 *
 * @author flyme
 * @date 2023-07-24
 */
public interface ArcInfoRemindMapper extends SuperMapper<ArcInfoRemind> {

}
