package com.meida.module.arc.provider.mapper;

import com.meida.module.arc.client.entity.ArcReport;
import com.meida.common.mybatis.base.mapper.SuperMapper;
/**
 * 报表模板 Mapper 接口
 * @author flyme
 * @date 2022-01-13
 */
public interface ArcReportMapper extends SuperMapper<ArcReport> {

}
