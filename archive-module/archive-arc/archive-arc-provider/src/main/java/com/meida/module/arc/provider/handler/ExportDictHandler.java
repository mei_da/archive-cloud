package com.meida.module.arc.provider.handler;

import cn.afterturn.easypoi.handler.inter.IExcelDictHandler;
import com.meida.common.base.utils.FlymeUtils;

import java.util.Map;

/**
 * <b>功能名：ArcInfoExportDictHandler</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-02-11 jiabing
 */
public class ExportDictHandler implements IExcelDictHandler {

    public static String defaultDictKey = "DEFAULT_DICT_KEY_";

    private Map<String, Map<String,Object>> dictMap;

    public ExportDictHandler(Map<String, Map<String,Object>> dict){
        this.dictMap = dict;
    }

    @Override
    public String toName(String dict, Object obj, String name, Object value) {
        if(FlymeUtils.isEmpty(dict)||FlymeUtils.isEmpty(value)){
            return value==null?"":value.toString();
        }
        if(FlymeUtils.isNotEmpty(dictMap)){
            Map<String,Object> map = dictMap.get(dict.toString());
            if(FlymeUtils.isEmpty(map)){
                map = dictMap.get(defaultDictKey);
            }
            if(FlymeUtils.isNotEmpty(map)){
                Object dictValue = map.get(value.toString());
                return dictValue==null?"":dictValue.toString();
            }
        }
        return value==null?"":value.toString();
    }

    @Override
    public String toValue(String s, Object o, String s1, Object o1) {
        return null;
    }
}