package com.meida.module.arc.provider.handler;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.handler.AdminUserInfoHandler;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.enums.StateEnum;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcUseRecord;
import com.meida.module.arc.provider.service.ArcUseRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * <b>功能名：UseUserUpdateBlackHandler</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-02-28 jiabing
 */
@Component
public class UseUserUpdateBlackHandler implements AdminUserInfoHandler {

    @Autowired
    private ArcUseRecordService arcUseRecordService;

    /**
     * 加入黑名单之前校验用户是否有未归还文档
     * @param map
     * @param userId
     * @return
     */
    @Override
    public ResultBody beforeUpdateState(EntityMap map, Long userId) {
        Integer status = map.get("status");
        String userType = map.get("userType");
        //从正常设置为黑名单，利用人账户 userType=2 代表是利用人类型
        if(FlymeUtils.isNotEmpty(status)&& StateEnum.ENABLE.getCode().equals(status)&&userType.equals("2")){
            LambdaQueryWrapper<ArcUseRecord> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            //verifyType 审核状态 1待审核2驳回3待归还4已归还5逾期归还
            lambdaQueryWrapper.in(ArcUseRecord::getVerifyType, Arrays.asList(1,3,5));
            Long count = this.arcUseRecordService.count(lambdaQueryWrapper);
            if(FlymeUtils.isNotEmpty(count)&&count>0){
                return ResultBody.failed("用户有待归还或待审批的申请，无法加入黑名单");
            }
        }
        return ResultBody.ok();
    }
}   