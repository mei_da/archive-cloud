package com.meida.module.arc.provider.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcInfoCollect;
import com.meida.module.arc.provider.mapper.ArcInfoCollectMapper;
import com.meida.module.arc.provider.service.ArcInfoCollectService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 档案收藏接口实现类
 *
 * @author flyme
 * @date 2023-05-29
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcInfoCollectServiceImpl extends BaseServiceImpl<ArcInfoCollectMapper, ArcInfoCollect> implements ArcInfoCollectService {


    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcInfoCollect aic, EntityMap extra) {
        return ResultBody.ok();
    }


    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcInfoCollect> cq, ArcInfoCollect aic, EntityMap requestMap) {
        cq.orderByDesc("aic.createTime");
        return ResultBody.ok();
    }


    @Override
    public ResultBody updateCollect(Map param) {
        ApiAssert.isNotEmpty("参数不能为空", param.get("qzId"));
        ApiAssert.isNotEmpty("参数不能为空", param.get("categoryId"));
        ApiAssert.isNotEmpty("参数不能为空", param.get("arcInfoId"));
        ApiAssert.isNotEmpty("参数不能为空", param.get("collect"));

        QueryWrapper<ArcInfoCollect> lambdaQueryWrapper = new QueryWrapper<>();
        lambdaQueryWrapper.eq(ObjectUtil.isNotNull(param.get("qzId")), "qzId", param.get("qzId"));
        lambdaQueryWrapper.eq(ObjectUtil.isNotNull(param.get("categoryId")), "categoryId", param.get("categoryId"));
        lambdaQueryWrapper.in(ObjectUtil.isNotNull(param.get("arcInfoId")), "arcInfoId", StrUtil.split(param.get("arcInfoId").toString(), ','));
        lambdaQueryWrapper.eq("userId", OpenHelper.getUserId());
        if ("1".equals(param.get("collect").toString())) {
            List<String> arcInfoId = StrUtil.split(param.get("arcInfoId").toString(), ',').stream().distinct().collect(Collectors.toList());
            List<ArcInfoCollect> list = new ArrayList<>();
            for (String i : arcInfoId) {
                list.add(ArcInfoCollect.builder().userId(OpenHelper.getUserId()).qzId(MapUtil.getLong(param, "qzId")).categoryId(MapUtil.getLong(param, "categoryId")).arcInfoId(Long.valueOf(i)).build());
            }
            return ResultBody.ok(this.saveBatch(list));
        } else {
            return ResultBody.ok(this.remove(lambdaQueryWrapper));
        }

    }

    @Override
    public List<Long> getArcInfoIds(EntityMap param, Long userId) {
        QueryWrapper<ArcInfoCollect> lambdaQueryWrapper = new QueryWrapper<>();
        lambdaQueryWrapper.eq(ObjectUtil.isNotNull(param.get("qzId")), "qzId", param.get("qzId"));
        lambdaQueryWrapper.eq(ObjectUtil.isNotNull(param.get("categoryId")), "categoryId", param.get("categoryId"));
        lambdaQueryWrapper.eq(ObjectUtil.isNotNull(param.get("arcInfoId")), "arcInfoId", param.get("arcInfoId"));
        lambdaQueryWrapper.eq("userId", userId);

        List<ArcInfoCollect> list = list(lambdaQueryWrapper);
        return CollUtil.isEmpty(list) ? Collections.emptyList() : list.stream().map(ArcInfoCollect::getArcInfoId).collect(Collectors.toList());
    }

    @Override
    public long getArcInfoCollect(Long arcInfoId, Long userId) {
        return count(new QueryWrapper<ArcInfoCollect>().lambda().eq(ArcInfoCollect::getUserId, userId).eq(ArcInfoCollect::getArcInfoId, arcInfoId));
    }

    @Override
    public List<Long> getCategoryIds(Map param, Long userId) {
        QueryWrapper<ArcInfoCollect> lambdaQueryWrapper = new QueryWrapper<>();
        lambdaQueryWrapper.eq(ObjectUtil.isNotNull(param.get("qzId")), "qzId", param.get("qzId"));
        lambdaQueryWrapper.eq(ObjectUtil.isNotNull(param.get("categoryId")), "categoryId", param.get("categoryId"));
        lambdaQueryWrapper.eq("userId", userId);

        List<ArcInfoCollect> list = list(lambdaQueryWrapper);
        return CollUtil.isEmpty(list) ? Collections.emptyList() : list.stream().map(ArcInfoCollect::getCategoryId).distinct().collect(Collectors.toList());
    }


}
