package com.meida.module.arc.provider.log.convert.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.utils.SpringContextHolder;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.client.entity.ArcField;
import com.meida.module.arc.client.entity.ArcLog;
import com.meida.module.arc.provider.log.convert.LogConvert;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.service.ArcCategoryService;
import com.meida.module.arc.provider.service.ArcDictService;
import com.meida.module.arc.provider.service.ArcFieldService;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysDeptService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <b>功能名：ArcUpdateLogConvert</b><br>
 * <b>说明：档案日志转换类</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-01-27 jiabing
 */
public class ArcOriginalLogConvert implements LogConvert {
    private ArcCategoryService arcCategoryService;
    public ArcOriginalLogConvert(){
        this.initService();
    }
    private void initService(){
        ArcCategoryService arcCategoryService = (ArcCategoryService) SpringContextHolder.getHandler("arcCategoryServiceImpl", ArcCategoryService.class);
        this.arcCategoryService = arcCategoryService;

    }
    @Override
    public boolean convert(Object[] args, ArcLog log) {
        //判断预归档/档案管理等等
        if(LogOptTypeEnum.NULL.getName().equals(log.getOptType())){
            if(args!=null&&args.length>0){
                if(args.length==1) {

                    EntityMap param = new EntityMap();
                    if(args[0] instanceof Map) {
                        param.putAll((Map) args[0]);
                    }else{
                        param.put("logOptType",args[0]);
                        param.put("categoryId",args[1]);
                    }

                    if(FlymeUtils.isNotEmpty(param.get("isLog"))&&"0".equals(param.get("isLog").toString())){
                        return false;
                    }
                    Object categoryIdObj = param.get("categoryId");
                    Object logOptType = param.remove("logOptType");
                    ArcCategory category = null;

                    if (FlymeUtils.isNotEmpty(categoryIdObj)) {
                        category = arcCategoryService.getById(Long.parseLong(categoryIdObj.toString()));
                    }
                    if (FlymeUtils.isNotEmpty(logOptType)) {
                        log.setOptType(logOptType.toString());
                    }
                    if (FlymeUtils.isNotEmpty(category)) {
                        log.setOptType(log.getOptType() + "-" + category.getCnName());
                    }
                }
            }else{
                return false;
            }
        }
        return true;
    }

}