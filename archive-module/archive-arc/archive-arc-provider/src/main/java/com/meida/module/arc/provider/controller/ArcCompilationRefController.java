package com.meida.module.arc.provider.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcCompilationRef;
import com.meida.module.arc.provider.service.ArcCompilationRefService;


/**
 * 编研素材关联控制器
 *
 * @author flyme
 * @date 2022-01-04
 */
@RestController
@RequestMapping("/arc/acr/")
public class ArcCompilationRefController extends BaseController<ArcCompilationRefService, ArcCompilationRef>  {

    @ApiOperation(value = "编研素材关联-分页列表", notes = "编研素材关联分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "编研素材关联-列表", notes = "编研素材关联列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "编研素材关联-添加", notes = "添加编研素材关联")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "编研素材关联-更新", notes = "更新编研素材关联")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "编研素材关联-删除", notes = "删除编研素材关联")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "编研素材关联-详情", notes = "编研素材关联详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

}
