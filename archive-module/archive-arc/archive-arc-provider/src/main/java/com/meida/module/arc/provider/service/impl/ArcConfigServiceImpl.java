package com.meida.module.arc.provider.service.impl;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.base.service.impl.BaseServiceImpl;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.common.mybatis.vo.JoinBean;
import com.meida.common.utils.ApiAssert;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.client.entity.ArcConfig;
import com.meida.module.arc.client.entity.ArcConfigRef;
import com.meida.module.arc.provider.mapper.ArcConfigMapper;
import com.meida.module.arc.provider.service.ArcCategoryService;
import com.meida.module.arc.provider.service.ArcConfigRefService;
import com.meida.module.arc.provider.service.ArcConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 归档配置接口实现类
 *
 * @author flyme
 * @date 2021-11-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArcConfigServiceImpl extends BaseServiceImpl<ArcConfigMapper, ArcConfig> implements ArcConfigService {

    @Autowired
    private ArcCategoryService arcCategoryService;

    @Autowired
    private ArcConfigRefService arcConfigRefService;

    @Override
    public ResultBody beforeAdd(CriteriaSave cs, ArcConfig config, EntityMap extra) {
        ApiAssert.isNotEmpty("全宗id不能为空",config.getQzId());
        ApiAssert.isNotEmpty("来源门类不能为空",config.getSrcCategoryId());
        ApiAssert.isNotEmpty("目标门类不能为空",config.getTargetCategoryId());
        //校验改全宗下来源门类是否已配置了目标门类
        CriteriaQuery<ArcConfig> query = new CriteriaQuery<ArcConfig>(ArcConfig.class);
        query.lambda().eq(ArcConfig::getQzId,config.getQzId())
                .eq(ArcConfig::getSrcCategoryId,config.getSrcCategoryId());
        Long count = this.count(query);
        if(count>0){
            ApiAssert.failure("该全宗下来源门类已有归档配置");
        }

        ArcCategory src = arcCategoryService.getById(config.getSrcCategoryId());
        ArcCategory target = arcCategoryService.getById(config.getTargetCategoryId());

        config.setSrcCategoryDb(src.getDbName());
        config.setSrcCategoryName(src.getCnName());
        config.setSrcCategoryTable(src.getTableName());

        config.setTargetCategoryDb(target.getDbName());
        config.setTargetCategoryName(target.getCnName());
        config.setTargetCategoryTable(target.getTableName());

        return ResultBody.ok();
    }



    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public ResultBody beforePageList(CriteriaQuery<ArcConfig> cq, ArcConfig config, EntityMap requestMap) {
      cq.orderByDesc("config.createTime");
      return ResultBody.ok();
    }

    @Override
    public ResultBody beforeListEntityMap(CriteriaQuery<ArcConfig> cq, ArcConfig t, EntityMap requestMap) {
        ApiAssert.isNotEmpty("全宗id不能为空",t.getQzId());
        cq.eq(ArcConfig.class,"qzId",t.getQzId());
        cq.select(ArcConfig.class,"name");
        cq.select(ArcConfig.class,"srcCategoryId");
        cq.select(ArcConfig.class,"targetCategoryId");
        cq.select(ArcConfig.class,"configId");


        JoinBean srcCategoryJoin = cq.createJoin(ArcCategory.class);
        srcCategoryJoin.setJoinAlias("srcCategory");
        srcCategoryJoin.setMainField("srcCategoryId");
        cq.addSelect("srcCategory.cnName as srcCategoryName");

        JoinBean targetCategoryJoin = cq.createJoin(ArcCategory.class);
        targetCategoryJoin.setJoinAlias("targetCategory");
        targetCategoryJoin.setMainField("targetCategoryId");
        cq.addSelect("targetCategory.cnName as targetCategoryName");



        cq.orderByDesc("config.createTime");
        return ResultBody.ok();
    }
    @Override
    public ResultBody beforeEdit(CriteriaUpdate<ArcConfig> cu, ArcConfig t, EntityMap extra) {
        ApiAssert.isEmpty("只能修改名称",t.getSrcCategoryId());
        ApiAssert.isEmpty("只能修改名称",t.getTargetCategoryId());
        return ResultBody.ok();
    }

    @Override
    public ResultBody afterDelete(CriteriaDelete cd, Long[] ids) {
        //级联删除所有的配置信息
        Long configId = cd.getIdValue();
        CriteriaQuery<ArcConfigRef> query = new CriteriaQuery<ArcConfigRef>(ArcConfigRef.class);
        query.lambda().eq(ArcConfigRef::getConfigId,configId);
        arcConfigRefService.remove(query);
        return ResultBody.msg("删除成功");
    }

    @Override
    public ResultBody config(Map var1) {
        Object srcCategoryId = var1.get("srcCategoryId");
        ApiAssert.isNotEmpty("源门类id不能为空", srcCategoryId);
        CriteriaQuery<ArcConfig> query = new CriteriaQuery<ArcConfig>(ArcConfig.class);
        query.lambda().eq(ArcConfig::getSrcCategoryId, Long.parseLong(srcCategoryId.toString()));
        List<ArcConfig> config = this.list(query);
        if (FlymeUtils.isEmpty(config)) {
            return ResultBody.failed("未配置归档目标门类");
        }
        if (config.size() > 1) {
            return ResultBody.failed("同一个源门类只能归档一个目标门类");
        }
        return ResultBody.ok(config.get(0));
    }
}
