package com.meida.module.arc.provider.service;

import com.meida.module.arc.client.entity.ArcCompilationRef;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.List;

/**
 * 编研素材关联 接口
 *
 * @author flyme
 * @date 2022-01-04
 */
public interface ArcCompilationRefService extends IBaseService<ArcCompilationRef> {

    void addCompilationRef(Long compilationId, Long categoryId, List<Long> arcInfoIds);

    void removeCompilation(List<Long> arcInfoIds);

}
