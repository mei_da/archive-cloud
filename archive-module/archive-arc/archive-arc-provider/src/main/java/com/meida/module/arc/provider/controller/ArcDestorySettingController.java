package com.meida.module.arc.provider.controller;

import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcDestorySetting;
import com.meida.module.arc.provider.service.ArcDestorySettingService;


/**
 * 销毁配置控制器
 *
 * @author flyme
 * @date 2022-01-11
 */
@RestController
@RequestMapping("/arc/destory/setting/")
public class ArcDestorySettingController extends BaseController<ArcDestorySettingService, ArcDestorySetting>  {

    @ApiOperation(value = "销毁配置-分页列表", notes = "销毁配置分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "销毁配置-列表", notes = "销毁配置列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "销毁配置-添加", notes = "添加销毁配置")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "销毁配置-更新", notes = "更新销毁配置")
    @PostMapping(value = "update")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE,optType = LogOptTypeEnum.DESTORY,optObj="销毁配置变更")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "销毁配置-删除", notes = "删除销毁配置")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "销毁配置-详情", notes = "销毁配置详情")
    @GetMapping(value = "defaultSetting")
    public ResultBody defaultSetting(@RequestParam(required = false) Map params) {
        return bizService.defaultSetting(params);
    }

}
