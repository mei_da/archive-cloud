package com.meida.module.arc.provider.service;

import java.util.List;

/**
 * <b>功能名：UserUnitService</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-01-28 jiabing
 */
public interface UserUnitService {

    List<Long> getLoginUserUnitIds(Long qzId,Long userId);
}