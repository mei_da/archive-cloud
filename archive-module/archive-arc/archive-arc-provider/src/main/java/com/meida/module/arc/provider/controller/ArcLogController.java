package com.meida.module.arc.provider.controller;

import com.meida.common.security.OpenHelper;
import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.DateUtil;
import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.enums.LogOptTypeEnum;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.meida.common.mybatis.model.*;

import java.util.Date;
import java.util.Map;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RestController;
import com.meida.common.springmvc.base.BaseController;
import com.meida.module.arc.client.entity.ArcLog;
import com.meida.module.arc.provider.service.ArcLogService;


/**
 * 日志控制器
 *
 * @author flyme
 * @date 2022-01-25
 */
@RestController
@RequestMapping("/arc/log/")
public class ArcLogController extends BaseController<ArcLogService, ArcLog>  {

    @ApiOperation(value = "日志-分页列表", notes = "日志分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "日志-列表", notes = "日志列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "日志-添加", notes = "添加日志")
    @PostMapping(value = "save")
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "日志-更新", notes = "更新日志")
    @PostMapping(value = "update")
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "日志-删除", notes = "删除日志")
    @PostMapping(value = "remove")
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.delete(params);
    }


    @ApiOperation(value = "日志-详情", notes = "日志详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    /**
     * 导出档案统计
     *
     * @param params
     * @param request
     * @param response
     */
    @GetMapping(value = "export")
    public void export(@RequestParam(required = false) Map params, HttpServletRequest request, HttpServletResponse response) {
//        request.setAttribute("reqParam",params);
        params.put("exportKey","arcLogOpt:"+ OpenHelper.getUserId());
        params.put("columns","[\n" +
                "    {\n" +
                "        \"key\": \"optType\",\n" +
                "        \"name\": \"操作模块\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"optObj\",\n" +
                "        \"name\": \"操作类型\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"optContent\",\n" +
                "        \"name\": \"操作内容\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"optParam\",\n" +
                "        \"name\": \"操作参数\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"optTime\",\n" +
                "        \"name\": \"操作时间\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"key\": \"nickName\",\n" +
                "        \"name\": \"操作人\"\n" +
                "    }\n" +
                "]");
        bizService.export(null,params, request, response, "操作日志记录"+ DateUtil.date2Str(new Date(),"yyyyMMddHHmmss"), "操作日志记录","arcLogExportHandler");
    }

}
