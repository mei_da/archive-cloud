package com.meida.module.arc.provider.service;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.module.arc.client.entity.ArcSecondField;
import com.meida.common.mybatis.base.service.IBaseService;

import java.util.Map;

/**
 * 二级分类字段配置 接口
 *
 * @author flyme
 * @date 2022-08-07
 */
public interface ArcSecondFieldService extends IBaseService<ArcSecondField> {

     ResultBody saveOrUpdate(EntityMap requestMap);

     ResultBody upOrDown(Map var1);
}
