package com.meida.module.arc.provider.handler;

import com.meida.common.base.handler.AdminLoginHandler;
import com.meida.common.base.module.LoginParams;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.DateUtils;
import com.meida.common.utils.RedisUtils;
import com.meida.module.arc.provider.service.ArcAuthTokenService;
import com.meida.module.virbox.provider.entity.AuthToken;
import com.meida.module.virbox.provider.virboxProvider.model.SoftwareLicense;
import com.meida.module.virbox.provider.virboxProvider.service.LicenseProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * 登录前校验
 * @author zyf
 */
@Component
public class AdminLoginVirboxHanadler implements AdminLoginHandler {
    @Autowired
    private LicenseProvider licenseProvider;
    @Autowired
    RedisUtils redisUtils;

    @Autowired
    private ArcAuthTokenService arcAuthTokenService;


    @Override
    public ResultBody beforLogin(LoginParams loginParams) {
        AuthToken authToken= arcAuthTokenService.getLicense();
        if(FlymeUtils.isNotEmpty(authToken)){
            Integer onlineCount=FlymeUtils.getInteger(authToken.getOnlineCount(),0);
            System.out.println("授权用户数量###########################################"+onlineCount);
            //校验在线用户
            Boolean tag=arcAuthTokenService.checkOnlineCount(onlineCount);
            if(!tag){
                return  ResultBody.failed("在线用户已达最大授权数量");
            }
        }

        return new ResultBody();
    }

    @Override
    public ResultBody afterLogin(Map<String, Object> map, LoginParams loginParams) {
        return ResultBody.ok();
    }
}
