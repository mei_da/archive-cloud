package com.meida.module.arc.provider.controller;

import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenHelper;
import com.meida.common.springmvc.base.BaseController;
import com.meida.common.thread.IThreadPoolExecutorService;
import com.meida.module.arc.client.entity.ArcOriginal;
import com.meida.module.arc.provider.log.annotation.LogAspect;
import com.meida.module.arc.provider.log.convert.impl.ArcOriginalLogConvert;
import com.meida.module.arc.provider.log.enums.LogTypeEnum;
import com.meida.module.arc.provider.service.ArcOriginalService;
import com.meida.module.file.client.entity.SysFile;
import com.meida.module.file.provider.service.SysFileService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


/**
 * 档案原文表控制器
 *
 * @author flyme
 * @date 2021-12-05
 */
@RestController
@RequestMapping("/arc/original/")
public class ArcOriginalController extends BaseController<ArcOriginalService, ArcOriginal> {
    @Autowired
    private IThreadPoolExecutorService threadPoolExecutorService;

    @Autowired(required = false)
    private SysFileService fileService;


    @ApiOperation(value = "档案原文表-分页列表", notes = "档案原文表分页列表")
    @GetMapping(value = "page")
    public ResultBody pageList(@RequestParam(required = false) Map params) {
        return bizService.pageList(params);
    }

    @ApiOperation(value = "档案原文表-列表", notes = "档案原文表列表")
    @GetMapping(value = "list")
    public ResultBody list(@RequestParam(required = false) Map params) {
        return bizService.listEntityMap(params);
    }

    @ApiOperation(value = "档案原文表-添加", notes = "添加档案原文表")
    @PostMapping(value = "save")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "原文添加", optContent = "添加原文", convert = ArcOriginalLogConvert.class)
    public ResultBody save(@RequestParam(required = false) Map params) {
        return bizService.add(params);
    }

    @ApiOperation(value = "档案原文表-更新", notes = "更新档案原文表")
    @PostMapping(value = "update")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "原文更新", optContent = "更新原文", convert = ArcOriginalLogConvert.class)
    public ResultBody update(@RequestParam(required = false) Map params) {
        return bizService.edit(params);
    }

    @ApiOperation(value = "档案原文表-删除", notes = "删除档案原文表")
    @PostMapping(value = "remove")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "原文删除", optContent = "删除原文", convert = ArcOriginalLogConvert.class)
    public ResultBody remove(@RequestParam(required = false) Map params) {
        return bizService.removeOriginal(params);
    }


    @ApiOperation(value = "档案原文表-详情", notes = "档案原文表详情")
    @GetMapping(value = "get")
    public ResultBody get(@RequestParam(required = false) Map params) {
        return bizService.get(params);
    }

    @ApiOperation(value = "档案原文表-上传", notes = "档案原文表-上传")
    @RequestMapping(value = "upload", method = RequestMethod.POST)
    public ResultBody fileUpload(
            HttpServletRequest request,
            @RequestParam(value = "files") MultipartFile[] files,
            @RequestParam(value = "fileIds") Long[] fileIds
            , @RequestParam(value = "fileType", defaultValue = "image") String fileType
            , @RequestParam(value = "busId", required = false) Long busId
            , @RequestParam(value = "categoryId", required = false) Long categoryId
            , @RequestParam(value = "busType", required = false) String busType
    ) {
        EntityMap map = bizService.upload(request, files, fileIds, categoryId, busId, busType);
        return ResultBody.ok(map);
    }

    @ApiOperation(value = "档案原文表-批量挂接", notes = "批量挂接")
    @PostMapping(value = "hookUp")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "批量挂接", optContent = "批量挂接", convert = ArcOriginalLogConvert.class)
    public ResultBody hookUp(@RequestParam(required = false) String logOptType, @RequestParam(required = false) Long categoryId) {
        return bizService.batchHookUp(categoryId);
    }

    @ApiOperation(value = "档案原文表-批量挂接检测", notes = "批量挂接检测")
    @GetMapping(value = "checkHookUp")
    public ResultBody checkHookUp() {
        return bizService.checkHookUp();
    }

    @ApiOperation(value = "档案原文表-批量挂接", notes = "批量挂接")
    @PostMapping(value = "hookUpSubmit")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "批量挂接", optContent = "批量挂接", convert = ArcOriginalLogConvert.class)
    public ResultBody hookUpSubmit(@RequestParam(required = false) Integer hookUpType, @RequestParam(required = false) Long categoryId) {
        return bizService.batchHookUp2(categoryId, hookUpType);
    }


    @ApiOperation(value = "档案原文表-更新名称和排序值", notes = "更新名称和排序值")
    @PostMapping(value = "updateArcOriginalInfo")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "原文更新", optContent = "更新原文属性", convert = ArcOriginalLogConvert.class)
    public ResultBody updateArcOriginalInfo(@RequestParam(required = false) Map params) {
        return bizService.updateArcOriginalInfo(params);
    }

    @ApiOperation(value = "档案原文表-上下移动", notes = "档案原文表上下移动")
    @PostMapping(value = "upOrDown")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "档案原文表上下移动", optContent = "档案原文表上下移动", convert = ArcOriginalLogConvert.class)
    public ResultBody upOrDown(@RequestParam(required = false) Map params) {
        return bizService.upOrDown(params);
    }

    //https://blog.csdn.net/maphc/article/details/129778386
    @ApiOperation(value = "档案原文表-文件转换和合成", notes = "文件转换和合成")
    @PostMapping(value = "convertArcOriginal")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "原文更新", optContent = "文件转换和合成", convert = ArcOriginalLogConvert.class)
    public ResultBody convertArcOriginal(HttpServletRequest request,
                                         @RequestParam(required = false) Map params) {
        return bizService.convertArcOriginal(request, params);
    }

    @ApiOperation(value = "档案原文表-下载", notes = "下载原文")
    @RequestMapping(value = "downloadArcOriginal")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "下载原文", optContent = "下载原文", convert = ArcOriginalLogConvert.class)
    public void downloadArcOriginal(HttpServletRequest request, HttpServletResponse response,
                                    @RequestParam(required = false) Map params) {
        bizService.downloadArcOriginal(request, response, params, null, false);
    }

    @ApiOperation(value = "档案原文表-打印", notes = "打印原文")
    @RequestMapping(value = "printArcOriginal")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "打印原文", optContent = "打印原文", convert = ArcOriginalLogConvert.class)
    public ResultBody printArcOriginal(HttpServletRequest request, HttpServletResponse response,
                                       @RequestParam(required = false) Map params) {
        return ResultBody.ok(
                bizService.printArcOriginal(request, response, params));
    }

    @ApiOperation(value = "档案原文表-异步下载", notes = "异步下载")
    @RequestMapping(value = "syncDownloadArcOriginal")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "下载原文", optContent = "下载原文", convert = ArcOriginalLogConvert.class)
    public void syncDownloadArcOriginal(HttpServletRequest request, HttpServletResponse response,
                                              @RequestParam(required = false) Map params) {
        String fileName = MapUtil.getStr(params, "fileName") + ".zip";
        Long fileSize = MapUtil.getLong(params, "fileSize");
        SysFile sysFile = new SysFile();
        sysFile.setFileId(IdWorker.getId());
        sysFile.setFileName(fileName);
        sysFile.setFileSize(fileSize);
        sysFile.setCreateUser(OpenHelper.getUserId());
        sysFile.setFileExt("zip");
        sysFile.setDownloadCount(0);
        fileService.save(sysFile);
        threadPoolExecutorService.singleExecute(() -> bizService.downloadArcOriginal(request, response, params, sysFile, true));
        //return ResultBody.msg("下载任务已提交");
    }

    @ApiOperation(value = "档案原文表-下载次数检测", notes = "下载次数检测")
    @RequestMapping(value = "downloadCheck")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "下载次数检测", optContent = "下载次数检测", convert = ArcOriginalLogConvert.class)
    public ResultBody downloadCheck(@RequestParam(required = false) Map params) {
        Long fileId = MapUtil.getLong(params, "fileId");
        bizService.downloadCheckCount(fileId);
        return ResultBody.ok();
    }

    @ApiOperation(value = "档案原文表-清空已下载", notes = "清空已下载")
    @RequestMapping(value = "clearDownLoad")
    @LogAspect(logType = LogTypeEnum.LOG_OPERATE, optObj = "清空已下载", optContent = "清空已下载", convert = ArcOriginalLogConvert.class)
    public ResultBody clearDownLoad(@RequestParam(required = false) Map params) {
        Long fileId = MapUtil.getLong(params, "fileId");
        bizService.clearDownLoad(fileId);
        return ResultBody.ok();
    }
}
