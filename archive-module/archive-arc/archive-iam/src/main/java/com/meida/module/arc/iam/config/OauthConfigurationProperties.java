package com.meida.module.arc.iam.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "oauth.config")
public class OauthConfigurationProperties {

    private String clientId;

    private String clientSecret;

    private String serverUrlPrefix;

    private String tokenUrl;

    private String profileUrl;

    private String redirectUri;

}
