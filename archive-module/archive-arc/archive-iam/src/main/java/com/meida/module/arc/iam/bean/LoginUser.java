package com.meida.module.arc.iam.bean;

import lombok.Data;

@Data
public class LoginUser {
    private String id;
    private String employeeNum;
    private String realName;
    private String phone;
    private String email;
    private String account;
}
