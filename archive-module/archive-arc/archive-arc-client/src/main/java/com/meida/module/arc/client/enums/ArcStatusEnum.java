package com.meida.module.arc.client.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 档案相关枚举
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ArcStatusEnum {
    /**
     * 归档标识0预归档-文件级1预归档-档案级
     */
    ARC_STATUS_0(0,"预归档-文件级"),
    ARC_STATUS_1(1,"预归档-档案级"),
    ARC_STATUS_2(2,"已经归档");

    ArcStatusEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static ArcStatusEnum matchCode(Integer code){
        for(ArcStatusEnum obj: ArcStatusEnum.values()){
            if(obj.getCode().equals(code)){
                return obj;
            }
        }
        return null;
    }
}
