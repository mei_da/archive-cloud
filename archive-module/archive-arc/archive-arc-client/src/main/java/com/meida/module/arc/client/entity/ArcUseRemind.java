package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 借阅消息提醒
 *
 * @author flyme
 * @date 2022-01-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_use_remind")
@TableAlias("aur")
@ApiModel(value="ArcUseRemind对象", description="借阅消息提醒")
public class ArcUseRemind extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "remindId", type = IdType.ASSIGN_ID)
    private Long remindId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "1，系统提醒 2，人工提醒")
    private Integer type;

    @ApiModelProperty(value = "提醒用户id")
    private Long userId;

    @ApiModelProperty(value = "是否已读")
    private Integer isRead;

    @ApiModelProperty(value = "首次读取时间")
    private Date readTime;

    @ApiModelProperty(value = "关联利用库id")
    private Long useId;

    @ApiModelProperty(value = "关联借阅记录id")
    private Long useRecordId;

    @ApiModelProperty(value = "审批人ID")
    private Long verifyUserId;

    @ApiModelProperty(value = "档案ID")
    private Long arcInfoId;


}
