package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 数据库备份配置
 *
 * @author flyme
 * @date 2021-12-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_db")
@TableAlias("db")
@ApiModel(value="ArcDb对象", description="数据库备份配置")
public class ArcDb extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "dbId", type = IdType.ASSIGN_ID)
    private Long dbId;

    @ApiModelProperty(value = "0关闭自动1自动备份")
    private Integer auto;

    @JsonFormat(
            timezone = "GMT+8",
            pattern = "HH:mm:ss"
    )
    @ApiModelProperty(value = "自动备份时间 HH:mm:ss")
    private Date autoTime;

    @ApiModelProperty(value = "备份路径")
    private String path;

    @ApiModelProperty(value = "1覆盖2增量")
    private Integer type;

}
