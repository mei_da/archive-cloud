package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 档案评论
 *
 * @author flyme
 * @date 2023-07-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_info_comment")
@TableAlias("aic")
@ApiModel(value = "ArcInfoComment对象", description = "档案评论")
public class ArcInfoComment extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "commentId", type = IdType.ASSIGN_ID)
    private Long commentId;

    @ApiModelProperty(value = "档案Id")
    private Long arcInfoId;

    @ApiModelProperty(value = "门类id")
    private Long categoryId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "评论人")
    private Long replyUserId;

    @ApiModelProperty(value = "评论ID")
    private Long replyCommentId;

    @ApiModelProperty(value = "被评论人")
    private Long replyToUserId;

    @ApiModelProperty(value = "评论图片")
    private String replyImages;

    @ApiModelProperty(value = "评论内容")
    private String replyContent;

    @ApiModelProperty(value = "评论状态")
    private Integer replyState;

    @ApiModelProperty(value = "点赞数量")
    private Integer dzNum;

}
