package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 二级分类字段配置
 *
 * @author flyme
 * @date 2022-08-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_second_field")
@TableAlias("asf")
@ApiModel(value="ArcSecondField对象", description="二级分类字段配置")
public class ArcSecondField extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "secondFieldId", type = IdType.ASSIGN_ID)
    private Long secondFieldId;

    @ApiModelProperty(value = "门类id")
    private Long categoryId;

    @ApiModelProperty(value = "二级门类id")
    private Long categorySecondId;

    private Long qzId;

    @ApiModelProperty(value = "主键")
    private Long fieldId;

    @ApiModelProperty(value = "字段英文名称")
    private String fieldName;

    @ApiModelProperty(value = "字段中文名称")
    private String fieldCnName;

    @ApiModelProperty(value = "值类型1字符串2数字")
    private Integer dataType;

    @ApiModelProperty(value = "默认值")
    private String defaultValue;

    @ApiModelProperty(value = "长度")
    private Integer columnLength;

    @ApiModelProperty(value = "著录类型1文本框2数字框3日期框4下拉框5文本域6弹出框")
    private Integer inType;

    @ApiModelProperty(value = "字典编码")
    private String dictCode;

    private Integer seq;

    @ApiModelProperty(value = "1等于 2包含 3大于 4小于 5介于 6大于等于 7小于等于")
    private Integer queryType;

    @ApiModelProperty(value = "查询值")
    private String queryVal;

    @ApiModelProperty(value = "查询值结束")
    private String queryValEnd;

}
