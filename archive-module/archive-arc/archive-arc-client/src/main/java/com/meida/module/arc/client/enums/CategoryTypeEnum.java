package com.meida.module.arc.client.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 档案相关枚举
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum CategoryTypeEnum {
    /**
     * 门类类型
     * "类型0文件夹1项目2案卷3卷内4简化"
     */
    CATEGORY_TYPE_0(0,"文件夹"),
    CATEGORY_TYPE_1(1,"项目"),
    CATEGORY_TYPE_2(2,"案卷"),
    CATEGORY_TYPE_3(3,"卷内"),
    CATEGORY_TYPE_4(4,"简化"),
    CATEGORY_TYPE_5(5,"模板");

    CategoryTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static CategoryTypeEnum matchCode(Integer code){
        for(CategoryTypeEnum obj:CategoryTypeEnum.values()){
            if(obj.getCode().equals(code)){
                return obj;
            }
        }
        return null;
    }

    public static CategoryTypeEnum getValue(int code){
        for (CategoryTypeEnum  color: values()) {
            if(color.getCode() == code){
                return  color;
            }
        }
        return null;
    }
}
