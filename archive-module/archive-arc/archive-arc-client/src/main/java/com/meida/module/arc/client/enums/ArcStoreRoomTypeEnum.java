package com.meida.module.arc.client.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 档案库房类型枚举
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ArcStoreRoomTypeEnum {
    /**
     * 库房类型
     * 1库房2排3架4层
     */
    ROOM_TYPE_1(1,"库房"),
    ROOM_TYPE_2(2,"排"),
    ROOM_TYPE_3(3,"架"),
    ROOM_TYPE_4(4,"层");

    ArcStoreRoomTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

}
