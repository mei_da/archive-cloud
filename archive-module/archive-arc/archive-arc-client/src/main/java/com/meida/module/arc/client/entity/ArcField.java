package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 字段配置表
 *
 * @author flyme
 * @date 2021-11-22
 */
//@Getter
//@Setter
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_field")
@TableAlias("field")
@ApiModel(value="ArcField对象", description="字段配置表")
public class ArcField extends AbstractEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "门类id")
    private Long categoryId;

    @ApiModelProperty(value = "主键")
    @TableId(value = "fieldId", type = IdType.ASSIGN_ID)
    private Long fieldId;

    @ApiModelProperty(value = "字段英文名称")
    private String fieldName;

    @ApiModelProperty(value = "字段中文名称")
    private String fieldCnName;

    @ApiModelProperty(value = "值类型1字符串2数字")
    private Integer dataType;

    @ApiModelProperty(value = "默认值")
    private String defaultValue;

    @ApiModelProperty(value = "长度")
    private Integer columnLength;

    @ApiModelProperty(value = "著录类型1文本框2数字框3日期框4下拉框5文本域6弹出框")
    private Integer inType;

    @ApiModelProperty(value = "字典编码")
    private String dictCode;

    @ApiModelProperty(value = "著录排序")
    private Integer inSeq;

    @ApiModelProperty(value = "输入框长度")
    private Integer inLength;

    @ApiModelProperty(value = "输入最大长度")
    private Integer inMaxLength;

    @ApiModelProperty(value = "输入最小长度")
    private Integer inMinLength;

    @ApiModelProperty(value = "是否一行")
    private Integer inLine;

    @ApiModelProperty(value = "是否输入字段")
    private Integer inShow;

    @ApiModelProperty(value = "是否必填")
    private Integer inRequire;

    @ApiModelProperty(value = "是否查询")
    private Integer inSearch;

    @ApiModelProperty(value = "自动生成")
    private Integer inAutoGen;

    @ApiModelProperty(value = "自动加1")
    private Integer inAutoPlus;

    @ApiModelProperty(value = "继承")
    private Integer inInherit;

    @ApiModelProperty(value = "自动补零")
    private Integer inAutoZero;

    @ApiModelProperty(value = "补零位数")
    private Integer inZeroNum;

    @ApiModelProperty(value = "值不重复")
    private Integer inNoRepeat;

    @ApiModelProperty(value = "浏览排序")
    private Integer outSeq;

    @ApiModelProperty(value = "显示长度")
    private Integer outLength;

    @ApiModelProperty(value = "显示格式（1左2中3右）")
    private Integer outFormat;

    @ApiModelProperty(value = "列表是否展示")
    private Integer outIsShow;

    @ApiModelProperty(value = "是否排序")
    private Integer outIsOrder;

    @ApiModelProperty(value = "排序序号")
    private Integer outOrderSeq;

    @ApiModelProperty(value = "排序方式(ase,desc)")
    private String outOrderType;

    @ApiModelProperty(value = "是否档号")
    private Integer isArcNum;

    @ApiModelProperty(value = "连接符")
    private String arcConnect;

    @ApiModelProperty(value = "档号排序")
    private Integer arcSeq;

    @ApiModelProperty(value = "序号")
    private Integer seq;

    @ApiModelProperty(value = "是否blob字段")
    private Integer isBlob;

    @ApiModelProperty(value = "是否系统默认")
    private Integer isSystem;

    @ApiModelProperty(value = "是否挂接字段")
    private Integer isMount;

    @ApiModelProperty(value = "挂接字段排序-暂未实现")
    private Integer mountSeq;

}
