package com.meida.module.arc.client.vo;

import lombok.Data;

import java.util.List;

/**
 * @Description: 水印参数配置
 * @ClassName ArcWaterMarkJson
 * @date: 2023.08.10 11:42
 * @Author: ldd
 */
@Data
public class ArcWaterMarkJson {
    //    {"original":["preview","print","export","use","download"],"screen":0,"transparent":20,"img":0,"imgContent":{"imgUrl":"http://139.196.136.137:85/archive/2023/05/31/f4af28b3d18347ca8db7cb0da4141cf3.jpg","position":"leftTop"},"text":1,"textContent":{"value":"涉密文件","family":"Vedana","font":14,"rotate":20,"lineHeight":20,"position":"tile","uuid":1,"date":1},"footer":"1","footerContent":{"family":"微软雅黑","font":14}}


    private int screen;
    private int transparent;
    private int img;
    private ImgContentBean imgContent;
    private int text;
    private TextContentBean textContent;
    private String footer;
    private FooterContentBean footerContent;
    public List<String> original;


    public class ImgContentBean {
        public String imgUrl;
        public String position;
    }

    public class TextContentBean {
        public String value;
        public String family;
        public int font;
        public int rotate;
        public int lineHeight;
        public String position;
        public int uuid;
        public int date;


    }

    public class FooterContentBean {
        public String family;
        public int font;

    }


}
