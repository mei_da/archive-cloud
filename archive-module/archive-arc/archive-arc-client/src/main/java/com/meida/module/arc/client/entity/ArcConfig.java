package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 归档配置
 *
 * @author flyme
 * @date 2021-11-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_config")
@TableAlias("config")
@ApiModel(value="ArcConfig对象", description="归档配置")
public class ArcConfig extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务id")
    @TableId(value = "configId", type = IdType.ASSIGN_ID)
    private Long configId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "值")
    private String value;

    @ApiModelProperty(value = "来源门类表")
    private String srcCategoryTable;

    @ApiModelProperty(value = "目标门类表")
    private String targetCategoryTable;

    @ApiModelProperty(value = "来源门类id")
    private Long srcCategoryId;

    @ApiModelProperty(value = "目标门类id")
    private Long targetCategoryId;

    @ApiModelProperty(value = "来源门类名称")
    private String srcCategoryName;

    @ApiModelProperty(value = "目标门类名称")
    private String targetCategoryName;

    @ApiModelProperty(value = "源门类数据库")
    private String srcCategoryDb;

    @ApiModelProperty(value = "目标门类数据库")
    private String targetCategoryDb;

}
