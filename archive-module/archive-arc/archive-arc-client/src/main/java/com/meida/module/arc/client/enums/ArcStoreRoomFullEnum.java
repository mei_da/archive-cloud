package com.meida.module.arc.client.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 档案库房存储状态
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ArcStoreRoomFullEnum {
    /**
     * 库房存储状态
     * 0 空 1 有但未满 2已满
     */
    ROOM_FULL_0(0,"空"),
    ROOM_FULL_1(1,"有但未满"),
    ROOM_FULL_2(2,"已满");

    ArcStoreRoomFullEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

}
