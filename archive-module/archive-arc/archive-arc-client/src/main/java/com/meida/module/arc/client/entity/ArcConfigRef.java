package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

import java.util.Objects;

/**
 * 归档配置关联
 *
 * @author flyme
 * @date 2021-11-25
 */
@Data
@Accessors(chain = true)
@TableName("arc_config_ref")
@TableAlias("acr")
@ApiModel(value="ArcConfigRef对象", description="归档配置关联")
public class ArcConfigRef extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务id")
    @TableId(value = "configRefId", type = IdType.ASSIGN_ID)
    private Long configRefId;

    @ApiModelProperty(value = "归档id")
    private Long configId;

    @ApiModelProperty(value = "来源门类id")
    private Long srcCategoryId;

    @ApiModelProperty(value = "目标门类id")
    private Long targetCategoryId;

    @ApiModelProperty(value = "来源门类表")
    private String srcCategoryTable;

    @ApiModelProperty(value = "目标们类表")
    private String targetCategoryTable;

    @ApiModelProperty(value = "来源门类字段id")
    private Long srcFieldId;

    @ApiModelProperty(value = "目标门类字段id")
    private Long targetFieldId;

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ArcConfigRef that = (ArcConfigRef) o;
        return srcFieldId.equals(that.srcFieldId) &&
                targetFieldId.equals(that.targetFieldId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), srcFieldId, targetFieldId);
    }

//    @ApiModelProperty(value = "来源门类字段名称")
//    private String srcFieldName;
//
//    @ApiModelProperty(value = "目标门类字段名称")
//    private String targetFieldName;
//
//    @ApiModelProperty(value = "来源门类字段中文名称")
//    private String srcFieldCnName;
//
//    @ApiModelProperty(value = "目标门类字段中文名称")
//    private String targetFieldCnName;
//
//    @ApiModelProperty(value = "来源字段长度")
//    private Integer srcColumnLength;
//
//    @ApiModelProperty(value = "目标字段长度")
//    private Integer targetColumnLength;
//
//    @ApiModelProperty(value = "来源字段类型")
//    private Integer srcDataType;
//
//    @ApiModelProperty(value = "目标字段类型")
//    private Integer targetDataType;

}
