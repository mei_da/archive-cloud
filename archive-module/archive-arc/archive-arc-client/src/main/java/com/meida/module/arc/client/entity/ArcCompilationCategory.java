package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 素材类型
 *
 * @author flyme
 * @date 2022-01-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_compilation_category")
@TableAlias("acc")
@ApiModel(value="ArcCompilationCategory对象", description="素材类型")
public class ArcCompilationCategory extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "素材类型id")
    @TableId(value = "compilationId", type = IdType.ASSIGN_ID)
    private Long compilationId;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "序号")
    private Integer seq;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "父id")
    private Long parentId;

    @ApiModelProperty(value = "系统编码")
    private String sysCode;

}
