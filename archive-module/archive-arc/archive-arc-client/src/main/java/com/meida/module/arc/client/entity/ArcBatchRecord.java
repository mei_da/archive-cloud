package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 批量关联记录
 *
 * @author flyme
 * @date 2021-12-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_batch_record")
@TableAlias("abr")
@ApiModel(value="ArcBatchRecord对象", description="批量关联记录")
public class ArcBatchRecord extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "批量关联记录id")
    @TableId(value = "recordId", type = IdType.ASSIGN_ID)
    private Long recordId;

    @ApiModelProperty(value = "批量关联id")
    private Long arcBatchId;

    @ApiModelProperty(value = "批量关联号")
    private String batchNo;

    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @JsonFormat(
            timezone = "GMT+8",
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @ApiModelProperty(value = "批量关联时间")
    private Date batchTime;

    @ApiModelProperty(value = "父级关联字段")
    private String parentFieldName;

    @ApiModelProperty(value = "子级关联字段")
    private String childFieldName;

    @ApiModelProperty(value = "父级门类类型")
    private Integer parentType;

    @ApiModelProperty(value = "子级门类类型")
    private Integer childType;

    @ApiModelProperty(value = "父级匹配条数")
    private Integer parentCount;

    @ApiModelProperty(value = "子级匹配条数")
    private Integer childCount;

}
