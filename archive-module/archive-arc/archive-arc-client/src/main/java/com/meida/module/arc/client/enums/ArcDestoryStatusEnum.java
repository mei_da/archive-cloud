package com.meida.module.arc.client.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 档案销毁状态
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ArcDestoryStatusEnum {
    /**
     */
    ARC_STATUS_0(0,"已还原"),
    ARC_STATUS_1(1,"已加入"),
    ARC_STATUS_2(2,"待审核"),
    ARC_STATUS_3(3,"已通过"),
    ARC_STATUS_4(4,"已驳回");

    ArcDestoryStatusEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static ArcDestoryStatusEnum matchCode(Integer code){
        for(ArcDestoryStatusEnum obj: ArcDestoryStatusEnum.values()){
            if(obj.getCode().equals(code)){
                return obj;
            }
        }
        return null;
    }
}
