package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.ParentId;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 档案库房
 *
 * @author flyme
 * @date 2022-01-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_store_room")
@TableAlias("asr")
@ApiModel(value="ArcStoreRoom对象", description="档案库房")
public class ArcStoreRoom extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "库房id")
    @TableId(value = "storeId", type = IdType.ASSIGN_ID)
    private Long storeId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "是否已满 0 空 1 有但未满 2已满")
    private Integer isFull;

    @ApiModelProperty(value = "父id")
    @ParentId
    private Long parentId;

    @ApiModelProperty(value = "1，库房2排3架4层")
    private Integer type;

    @ApiModelProperty(value = "序号")
    private Integer seq;
    @ApiModelProperty(value = "系统编码")
    private String sysCode;

}
