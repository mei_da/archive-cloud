package com.meida.module.arc.client.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <b>功能名：ForEachIndex</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-01-02 jiabing
 */
public class ForEachIndex {
    private Integer index = 0;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public void indexPlus(){
        index++;
    }

    public void indexPlusNum(Integer plusNum){
        index = index+plusNum;
    }
}   