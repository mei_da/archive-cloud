package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * 档案收藏
 *
 * @author flyme
 * @date 2023-05-29
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_info_collect")
@TableAlias("aic")
@ApiModel(value = "ArcInfoCollect对象", description = "档案收藏")
public class ArcInfoCollect extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "创建人")
    private Long createBy;

    @ApiModelProperty(value = "更新人")
    private Long updateBy;

    @ApiModelProperty(value = "主键")
    @TableId(value = "collectId", type = IdType.ASSIGN_ID)
    private Long collectId;

    @ApiModelProperty(value = "用户ID ")
    private Long userId;

    @ApiModelProperty(value = "档案ID")
    private Long arcInfoId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "门类id")
    private Long categoryId;

}
