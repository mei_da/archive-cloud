package com.meida.module.arc.client.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <b>功能名：ArcConfigRefJson</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2021-11-27 jiabing
 */
@Data
@Accessors(chain = true)
public class ImportExcelMatchJson {
    private Long fieldId;
    private String matchExcelHead;
    //private String excelHead;
    private String defaultValue;
}   