package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.annotation.TableAlias;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 档案信息表
 *
 * @author flyme
 * @date 2021-12-08
 */

@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_info")
@TableAlias("info")
@ApiModel(value="ArcInfo对象", description="档案信息表")
public class ArcInfo extends Model implements Serializable {

    private static final long serialVersionUID = 1L;
    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @JsonFormat(
            timezone = "GMT+8",
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @TableField(
            fill = FieldFill.INSERT
    )
    @ApiModelProperty(
            hidden = true
    )
    public Date createTime;
    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @JsonFormat(
            timezone = "GMT+8",
            pattern = "yyyyMMdd"
    )
    @TableField(
            fill = FieldFill.INSERT_UPDATE
    )
    @ApiModelProperty(
            hidden = true
    )
    public Date updateTime;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "arcInfoId", type = IdType.ASSIGN_ID)
    private Long arcInfoId;

    @ApiModelProperty(value = "扩展字段")
    private String expand;

    @ApiModelProperty(value = "父id")
    private Long parentId;

    @ApiModelProperty(value = "外部来源id")
    private Long outSideId;

    @ApiModelProperty(value = "删除时间")
    private String deleteTime;

    @ApiModelProperty(value = "创建批次")
    private String createBatch;

    @ApiModelProperty(value = "调出时间")
    private String adjustTime;

    @ApiModelProperty(value = "查看原文次数")
    private Integer viewFileCount;

    @ApiModelProperty(value = "查看条目次数")
    private Integer viewCount;

    @ApiModelProperty(value = "库房id")
    private Long storeRoomId;

    @ApiModelProperty(value = "索引状态")
    private Integer indexStatus;

    @ApiModelProperty(value = "原文数量")
    private Integer originalCount;

    @ApiModelProperty(value = "门类id")
    private Long categoryId;

    @ApiModelProperty(value = "是否利用")
    private Integer isUse;

    @ApiModelProperty(value = "是否入库")
    private Integer isStore;

    @ApiModelProperty(value = "是否回收站")
    private Integer isRecycle;

    @ApiModelProperty(value = "是否销毁")
    private Integer isDestory;

    @ApiModelProperty(value = "是否编研")
    private Integer isCompilation;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "单位id")
    private Long unitId;

    @ApiModelProperty(value = "存放位置")
    private String storageLocationName;

    @ApiModelProperty(value = "编研素材类型名称")
    private String compilationName;


    @ApiModelProperty(value = "顺序号")
    private String seq;

    @ApiModelProperty(value = "子条目数")
    private Integer childCount;

    @ApiModelProperty(value = "关联数量")
    private Integer refCount;

    @ApiModelProperty(value = "原移交单位")
    private String handouverUnit;

    @ApiModelProperty(value = "二级全宗号")
    private String fondsNo;

    @ApiModelProperty(value = "份数")
    private Integer numberNo;

    @ApiModelProperty(value = "归档标识0未归档1待归档2已归档")
    private String arcFlag;

    @ApiModelProperty(value = "档号")
    private String arcNo;

    @ApiModelProperty(value = "归档年度")
    private String filingYear;

    @ApiModelProperty(value = "页数")
    private Integer pageNo;

    @ApiModelProperty(value = "保管期限（保管期限Y永久，D30定期30年，D10定期10年）")
    private String retention;

    @ApiModelProperty(value = "文件形成时间")
    private String createdDate;

    @ApiModelProperty(value = "移交时间")
    private String handoverTime;

    @ApiModelProperty(value = "分类号")
    private String arcCtgNo;

    @ApiModelProperty(value = "件号")
    private String pieceNo;

    @ApiModelProperty(value = "题名")
    private String maintitle;

    @ApiModelProperty(value = "密级")
    private String securityClass;

    @ApiModelProperty(value = "存放位置")
    private String folderLocation;

    @ApiModelProperty(value = "归档日期")
    private String pigeonholeDate;

    @ApiModelProperty(value = "部门名称")
    private String department;

    @ApiModelProperty(value = "目录号")
    private String contentNo;

    @ApiModelProperty(value = "批量关联号")
    private String relevanceNo;

    @ApiModelProperty(value = "责任者")
    private String responsibleby;

    @ApiModelProperty(value = "设置提醒 1是 0否")
    private Integer isRemind;


    @ApiModelProperty(value = "评论数量")
    private int commentCount;

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public Integer getIsRemind() {
        return isRemind;
    }

    public void setIsRemind(Integer isRemind) {
        this.isRemind = isRemind;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public Integer getRevision() {
        return revision;
    }

    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Long getArcInfoId() {
        return arcInfoId;
    }

    public void setArcInfoId(Long arcInfoId) {
        this.arcInfoId = arcInfoId;
    }

    public String getExpand() {
        return expand;
    }

    public void setExpand(String expand) {
        if(FlymeUtils.isEmpty(expand)){
            this.expand = "{}";
        }else{
            this.expand = expand;
        }
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getOutSideId() {
        return outSideId;
    }

    public void setOutSideId(Long outSideId) {
        this.outSideId = outSideId;
    }

    public String getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(String deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getCreateBatch() {
        return createBatch;
    }

    public void setCreateBatch(String createBatch) {
        this.createBatch = createBatch;
    }

    public String getAdjustTime() {
        return adjustTime;
    }

    public void setAdjustTime(String adjustTime) {
        this.adjustTime = adjustTime;
    }

    public Integer getViewFileCount() {
        return viewFileCount;
    }

    public void setViewFileCount(Integer viewFileCount) {
        this.viewFileCount = viewFileCount;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Long getStoreRoomId() {
        return storeRoomId;
    }

    public void setStoreRoomId(Long storeRoomId) {
        this.storeRoomId = storeRoomId;
    }

    public Integer getIndexStatus() {
        return indexStatus;
    }

    public void setIndexStatus(Integer indexStatus) {
        this.indexStatus = indexStatus;
    }

    public Integer getOriginalCount() {
        return originalCount;
    }

    public void setOriginalCount(Integer originalCount) {
        this.originalCount = originalCount;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getIsUse() {
        return isUse;
    }

    public void setIsUse(Integer isUse) {
        this.isUse = isUse;
    }

    public Integer getIsStore() {
        return isStore;
    }

    public void setIsStore(Integer isStore) {
        this.isStore = isStore;
    }

    public Integer getIsRecycle() {
        return isRecycle;
    }

    public void setIsRecycle(Integer isRecycle) {
        this.isRecycle = isRecycle;
    }

    public Integer getIsDestory() {
        return isDestory;
    }

    public void setIsDestory(Integer isDestory) {
        this.isDestory = isDestory;
    }

    public Integer getIsCompilation() {
        return isCompilation;
    }

    public void setIsCompilation(Integer isCompilation) {
        this.isCompilation = isCompilation;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getQzId() {
        return qzId;
    }

    public void setQzId(Long qzId) {
        this.qzId = qzId;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public String getStorageLocationName() {
        return storageLocationName;
    }

    public void setStorageLocationName(String storageLocationName) {
        this.storageLocationName = storageLocationName;
    }

    public String getCompilationName() {
        return compilationName;
    }

    public void setCompilationName(String compilationName) {
        this.compilationName = compilationName;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public Integer getChildCount() {
        return childCount;
    }

    public void setChildCount(Integer childCount) {
        this.childCount = childCount;
    }

    public Integer getRefCount() {
        return refCount;
    }

    public void setRefCount(Integer refCount) {
        this.refCount = refCount;
    }

    public String getHandouverUnit() {
        return handouverUnit;
    }

    public void setHandouverUnit(String handouverUnit) {
        this.handouverUnit = handouverUnit;
    }

    public String getFondsNo() {
        return fondsNo;
    }

    public void setFondsNo(String fondsNo) {
        this.fondsNo = fondsNo;
    }

    public Integer getNumberNo() {
        return numberNo;
    }

    public void setNumberNo(Integer numberNo) {
        this.numberNo = numberNo;
    }

    public String getArcFlag() {
        return arcFlag;
    }

    public void setArcFlag(String arcFlag) {
        this.arcFlag = arcFlag;
    }

    public String getArcNo() {
        return arcNo;
    }

    public void setArcNo(String arcNo) {
        this.arcNo = arcNo;
    }

    public String getFilingYear() {
        return filingYear;
    }

    public void setFilingYear(String filingYear) {
        this.filingYear = filingYear;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public String getRetention() {
        return retention;
    }

    public void setRetention(String retention) {
        this.retention = retention;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getHandoverTime() {
        return handoverTime;
    }

    public void setHandoverTime(String handoverTime) {
        this.handoverTime = handoverTime;
    }

    public String getArcCtgNo() {
        return arcCtgNo;
    }

    public void setArcCtgNo(String arcCtgNo) {
        this.arcCtgNo = arcCtgNo;
    }

    public String getPieceNo() {
        return pieceNo;
    }

    public void setPieceNo(String pieceNo) {
        this.pieceNo = pieceNo;
    }

    public String getMaintitle() {
        return maintitle;
    }

    public void setMaintitle(String maintitle) {
        this.maintitle = maintitle;
    }

    public String getSecurityClass() {
        return securityClass;
    }

    public void setSecurityClass(String securityClass) {
        this.securityClass = securityClass;
    }

    public String getFolderLocation() {
        return folderLocation;
    }

    public void setFolderLocation(String folderLocation) {
        this.folderLocation = folderLocation;
    }

    public String getPigeonholeDate() {
        return pigeonholeDate;
    }

    public void setPigeonholeDate(String pigeonholeDate) {
        this.pigeonholeDate = pigeonholeDate;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getContentNo() {
        return contentNo;
    }

    public void setContentNo(String contentNo) {
        this.contentNo = contentNo;
    }

    public String getRelevanceNo() {
        return relevanceNo;
    }

    public void setRelevanceNo(String relevanceNo) {
        this.relevanceNo = relevanceNo;
    }

    public String getResponsibleby() {
        return responsibleby;
    }

    public void setResponsibleby(String responsibleby) {
        this.responsibleby = responsibleby;
    }
}
