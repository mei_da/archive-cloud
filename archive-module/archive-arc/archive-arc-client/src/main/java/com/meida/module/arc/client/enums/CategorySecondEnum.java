package com.meida.module.arc.client.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 二级门类枚举
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum CategorySecondEnum {
    /**
     * 门类类型
     * "类型0文件夹1项目"
     */
    CATEGORY_SECOND_0(0,"文件夹"),
    CATEGORY_SECOND_1(1,"项目");

    CategorySecondEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static CategorySecondEnum matchCode(Integer code){
        for(CategorySecondEnum obj: CategorySecondEnum.values()){
            if(obj.getCode().equals(code)){
                return obj;
            }
        }
        return null;
    }

    public static CategorySecondEnum getValue(int code){
        for (CategorySecondEnum color: values()) {
            if(color.getCode() == code){
                return  color;
            }
        }
        return null;
    }
}
