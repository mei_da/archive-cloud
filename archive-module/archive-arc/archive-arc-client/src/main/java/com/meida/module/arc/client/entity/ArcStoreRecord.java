package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 出入库记录
 *
 * @author flyme
 * @date 2022-01-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_store_record")
@TableAlias("asr")
@ApiModel(value="ArcStoreRecord对象", description="出入库记录")
public class ArcStoreRecord extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "recordId", type = IdType.ASSIGN_ID)
    private Long recordId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "机构id")
    private Long unitId;

    @ApiModelProperty(value = "库房id")
    private Long storeId;

    @ApiModelProperty(value = "档号")
    private String arcNum;

    @ApiModelProperty(value = "门类")
    private Long categoryId;

    @ApiModelProperty(value = "操作员名称")
    private String operatorName;

    @ApiModelProperty(value = "操作类型")
    private Integer operatorType;

    @ApiModelProperty(value = "经手人")
    private String handlerPerson;

    @ApiModelProperty(value = "档案类型")
    private String arcType;

    @ApiModelProperty(value = "操作时间")
    private String operatorTime;

    @ApiModelProperty(value = "原因")
    private String reason;

    @ApiModelProperty(value = "备注")
    private String remark;

}
