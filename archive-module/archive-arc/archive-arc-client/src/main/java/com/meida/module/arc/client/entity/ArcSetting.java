package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 系统配置
 *
 * @author flyme
 * @date 2021-11-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_setting")
@TableAlias("setting")
@ApiModel(value="ArcSetting对象", description="系统配置")
public class ArcSetting extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务id")
    @TableId(value = "settingId", type = IdType.ASSIGN_ID)
    private Long settingId;

    @ApiModelProperty(value = "系统名称")
    private String name;

    @ApiModelProperty(value = "版权")
    private String copyright;

    @ApiModelProperty(value = "版本号")
    private String version;

    @ApiModelProperty(value = "版本名称")
    private String versionName;

    @ApiModelProperty(value = "数据库版本")
    private String dbVersion;

    @ApiModelProperty(value = "用户识别码")
    private String idCode;

    @ApiModelProperty(value = "公司名称")
    private String companyName;

    @ApiModelProperty(value = "logo")
    private String logo;

    @ApiModelProperty(value = "首页logo")
    private String indexLogo;

    @ApiModelProperty(value = "联系我们logo")
    private String relationLogo;

    @ApiModelProperty(value = "展示页面（json表示）")
    private String preview;

    @ApiModelProperty(value = "首页背景（json表示）")
    private String background;

    @ApiModelProperty(value = "主题配置（json结构）")
    private String theme;

    @ApiModelProperty(value = "上传文件类型（json结构）")
    private String uploadType;

    @ApiModelProperty(value = "是否校验上传类型")
    private Integer checkUploadType;

    @ApiModelProperty(value = "登录配置（json结构）")
    private String loginConfig;

    @ApiModelProperty(value = "字号")
    private Integer font;

    @ApiModelProperty(value = "联系电话")
    private String phone;

    @ApiModelProperty(value = "邮箱地址")
    private String mail;

    @ApiModelProperty(value = "官网地址")
    private String website;

    @ApiModelProperty(value = "授权用户数")
    private Integer authPerson;

    @ApiModelProperty(value = "授权全宗数")
    private Integer authQz;

    @ApiModelProperty(value = "授权门类数")
    private Integer authCategory;

    @ApiModelProperty(value = "授权机构数量")
    private String authUnit;

    @ApiModelProperty(value = "每个门类授权档案数")
    private Integer authDa;

    @ApiModelProperty(value = "绑定url（saas版本基于该url区分）")
    private String bindUrl;

    @ApiModelProperty(value = "是否收费版")
    private Integer isOem;

    @ApiModelProperty(value = "是否默认")
    private Integer isDefault;

    @ApiModelProperty(value = "是否永久")
    private Integer authForever;

    @ApiModelProperty(value = "授权时间")
    private Date authTime;

    @ApiModelProperty(value = "jwtToken")
    private String token;

    @ApiModelProperty(value = "是否显示帮助")
    private Integer helpShow;

    @ApiModelProperty(value = "首页小图标")
    private String indexsmallLogo;

    @ApiModelProperty(value = "自定义JSON格式数据")
    private String customExpand;

}
