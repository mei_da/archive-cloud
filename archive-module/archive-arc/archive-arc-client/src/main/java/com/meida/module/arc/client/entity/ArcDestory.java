package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 档案销毁
 *
 * @author flyme
 * @date 2022-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_destory")
@TableAlias("destory")
@ApiModel(value="ArcDestory对象", description="档案销毁")
public class ArcDestory extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务id")
    @TableId(value = "destoryId", type = IdType.ASSIGN_ID)
    private Long destoryId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "门类id")
    private Long categoryId;

    @ApiModelProperty(value = "档案id")
    private Long arcInfoId;

    @ApiModelProperty(value = "机构id")
    private Long unitId;

    @ApiModelProperty(value = "档号")
    private String arcNo;

    @ApiModelProperty(value = "原文数量")
    private Integer originalCount;

    @ApiModelProperty(value = "年度")
    private String filingYear;

    @ApiModelProperty(value = "保管期限")
    private String retention;

    @ApiModelProperty(value = "题名")
    private String maintitle;

    @ApiModelProperty(value = "责任者")
    private String responsibleby;

    @ApiModelProperty(value = "0已还原1待审核2已销毁3已驳回")
    private Integer status;

    @ApiModelProperty(value = "销毁申请人id")
    private Long destoryUserId;

    @ApiModelProperty(value = "审核人id")
    private Long verifyUserId;

    @ApiModelProperty(value = "批准人名称")
    private String verifyUserName;

    @ApiModelProperty(value = "监管人姓名")
    private String supervisorName;

    @ApiModelProperty(value = "批准日期")
    private String verifyTime;

    @ApiModelProperty(value = "销毁时间")
    private String destoryTime;

    @ApiModelProperty(value = "销毁状态 0，未完成销毁原文1已完成销毁原文")
    private Integer destoryType;

    @ApiModelProperty(value = "销毁原因")
    private String reason;

}
