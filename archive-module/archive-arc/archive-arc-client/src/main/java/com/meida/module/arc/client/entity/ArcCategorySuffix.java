package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 门类表名后缀
 *
 * @author flyme
 * @date 2021-11-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_category_suffix")
@TableAlias("acs")
@ApiModel(value="ArcCategorySuffix对象", description="门类表名后缀")
public class ArcCategorySuffix extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "suffixId", type = IdType.ASSIGN_ID)
    private Long suffixId;

    @ApiModelProperty(value = "后缀")
    private String suffix;

    @ApiModelProperty(value = "状态0未使用1已使用")
    private Integer status;

    @ApiModelProperty(value = "后缀编码")
    private Integer suffixNum;

    @ApiModelProperty(value = "全宗id")
    private Long categoryId;

    @ApiModelProperty(value = "分片数量（0代表部分片，1代表1个分片）")
    private Integer sideCar;

}
