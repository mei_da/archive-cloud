package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 档案原文表
 *
 * @author flyme
 * @date 2021-12-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_original")
@TableAlias("original")
@ApiModel(value = "ArcOriginal对象", description = "档案原文表")
//@ESMetaData(indexName = "index_original", number_of_shards = 3, number_of_replicas = 0, printLog = true)
public class ArcOriginal extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "arcOriginalId", type = IdType.ASSIGN_ID)
    //@ESID
    private Long arcOriginalId;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "档案业务id")
    private Long arcInfoId;

    @ApiModelProperty(value = "门类ID")
    private Long categoryId;

    @ApiModelProperty(value = "原文名称")
    //@ESMapping(datatype = DataType.text_type)
    private String fileName;

    @ApiModelProperty(value = "原文注释")
    private String fileNote;

    @ApiModelProperty(value = "像素")
    private String filePixel;

    @ApiModelProperty(value = "分辨率")
    private String fileDpi;

    @ApiModelProperty(value = "原文大小")
    private Long fileSize;

    @ApiModelProperty(value = "原文类型")
    private String fileType;

    @ApiModelProperty(value = "访问路径")
    private String filePath;

    @ApiModelProperty(value = "MD5")
    private String md5;

    @ApiModelProperty(value = "索引状态")
    private Integer indexStatus;

    @ApiModelProperty(value = "原文序号")
    private Integer seq = 999999;

    @ApiModelProperty(value = "原文内容类型")
    private String fileContenttype;

    @ApiModelProperty(value = "本地路径")
    private String localPath;

    @ApiModelProperty(value = "oss路径")
    private String ossPath;

    @TableField(exist = false)
    private String fileContent;

    @ApiModelProperty(value = "全宗ID")
    private Long qzId;

}
