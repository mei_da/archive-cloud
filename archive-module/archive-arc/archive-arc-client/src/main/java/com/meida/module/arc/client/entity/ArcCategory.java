package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.ParentId;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 门类
 *
 * @author flyme
 * @date 2021-11-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_category")
@TableAlias("category")
@ApiModel(value="ArcCategory对象", description="门类")
public class ArcCategory extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "categoryId", type = IdType.ASSIGN_ID)
    private Long categoryId;

    @ApiModelProperty(value = "名称")
    private String cnName;

    @ApiModelProperty(value = "英文名称")
    private String enName;

    @ApiModelProperty(value = "系统编码")
    private String sysCode;

    @ApiModelProperty(value = "父id")
    @ParentId
    private Long parentId;

    @ApiModelProperty(value = "全宗ID")
    private Long qzId;

    @ApiModelProperty(value = "类型0文件夹1项目2案卷3卷内4简化")
    private Integer type;

    @ApiModelProperty(value = "1文件2档案3oa")
    private Integer categoryType;

    @ApiModelProperty(value = "序号")
    private Integer seq;

    @ApiModelProperty(value = "排序名称")
    private String sortName;

    @ApiModelProperty(value = "排序方式")
    private String sortOrder;

    @ApiModelProperty(value = "是否显示")
    private String isShow;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "数据库名称")
    private String dbName;

    @ApiModelProperty(value = "表名")
    private String tableName;

    @ApiModelProperty(value = "是否系统")
    private Integer isSystem;

}
