package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 日志
 *
 * @author flyme
 * @date 2022-01-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_log")
@TableAlias("log")
@ApiModel(value="ArcLog对象", description="日志")
public class ArcLog extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "日志id")
    @TableId(value = "logId", type = IdType.ASSIGN_ID)
    private Long logId;

    @ApiModelProperty(value = "操作用户id")
    private Long userId;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "用户名称")
    private String userName;

    @ApiModelProperty(value = "登录类型")
    private Integer loginType;

    @ApiModelProperty(value = "登录IP")
    private String loginIp;

    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @JsonFormat(
            timezone = "GMT+8",
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @ApiModelProperty(value = "登录时间")
    private Date loginTime;

    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @JsonFormat(
            timezone = "GMT+8",
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @ApiModelProperty(value = "推出时间")
    private Date logoutTime;

    @ApiModelProperty(value = "门类id")
    private Long categoryId;

    @ApiModelProperty(value = "门类名称")
    private String categoryName;

    @ApiModelProperty(value = "档案id")
    private Long arcInfoId;

    @ApiModelProperty(value = "档号")
    private String arcNo;

    @ApiModelProperty(value = "日志类型0登录日志1操作日志")
    private Integer logType;

    @ApiModelProperty(value = "操作模块")
    private String optType;

    @ApiModelProperty(value = "操作类型")
    private String optObj;

    @ApiModelProperty(value = "操作内容")
    private String optContent;

    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @JsonFormat(
            timezone = "GMT+8",
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @ApiModelProperty(value = "操作时间")
    private Date optTime;

    @ApiModelProperty(value = "操作参数")
    private String optParam;

}
