package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 备份记录
 *
 * @author flyme
 * @date 2021-12-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_db_record")
@TableAlias("adr")
@ApiModel(value="ArcDbRecord对象", description="备份记录")
public class ArcDbRecord extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "dbRecordId", type = IdType.ASSIGN_ID)
    private Long dbRecordId;

    @ApiModelProperty(value = "文件名称")
    private String fileName;

    @ApiModelProperty(value = "文件路径")
    private String filePath;

    @ApiModelProperty(value = "1自动2手动")
    private Integer type;

}
