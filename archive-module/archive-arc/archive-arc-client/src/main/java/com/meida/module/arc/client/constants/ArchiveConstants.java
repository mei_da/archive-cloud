package com.meida.module.arc.client.constants;

/**
 * <b>功能名：ArchiveConstants</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2021-11-19 jiabing
 */
public class ArchiveConstants {
    //首页及系统配置信息缓存key
    public static final String REDIS_SETTING_KEY_PREFIX = "settingkey_";

    public static final String REDIS_ID_CODE_KEY_PREFIX = "os_id_code_";
    //动态表后缀防止并发表名重复缓存key
    public static final String REDIS_TABLE_SUFFIX_KEY = "lastGenTableName";
    //跟节点id
    public static final Long ROOT_PARENT_ID = 0L;
    //档案信息表名前缀
    public static final String ARC_INFO_TABLE_PREFIX = "arc_info";
    //档案原文表名前缀
    public static final String ARC_ORIGINAL_TABLE_PREFIX = "arc_original";
    //档案动态表后缀长度
    public static final Integer ARC_TABLE_PREFIX_LENGTH = 5;
    //档案表分片数量，修改的话必须保证没有门类
    //0代表不分片
    // 1  分片 _1
    // 2  分片 -1 _2
    public static final Integer ARC_TABLE_DEFAULT_SIDECAR = 0;
    //原文默认排序
    public static final Integer ARC_ARCORIGINAL_SEQ = 999999;

    //默认数据库备份路径
    public static final String DEFAULT_DB_RECORD_PATH = "db";


    //档案报表缓存前缀
    public static final String ARC_REPORT_PREFIX = "arc_report";
    //档案字典缓存前缀
    public static final String ARC_DICT_PREFIX = "arc_dict";
    //档案二级门类缓存前缀
    public static final String ARC_CATEGORY_SECOND_PREFIX = "arc_category_second";
}
