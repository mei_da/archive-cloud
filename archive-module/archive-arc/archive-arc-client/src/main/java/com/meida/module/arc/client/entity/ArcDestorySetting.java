package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 销毁配置
 *
 * @author flyme
 * @date 2022-01-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_destory_setting")
@TableAlias("ads")
@ApiModel(value="ArcDestorySetting对象", description="销毁配置")
public class ArcDestorySetting extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "destorySettingId", type = IdType.ASSIGN_ID)
    private Long destorySettingId;

    @ApiModelProperty(value = "保留天数")
    private Integer holdBackDay;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "是否彻底清除")
    private Integer isThorough;

    @ApiModelProperty(value = "自动清除类型1按月2按天")
    private Integer autoDestoryType;

    @ApiModelProperty(value = "自动清除时间（月份或时辰）")
    private Integer autoDestoryTime;

}
