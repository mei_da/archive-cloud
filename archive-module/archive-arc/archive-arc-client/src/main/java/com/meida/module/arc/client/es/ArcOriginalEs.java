package com.meida.module.arc.client.es;

import lombok.Data;
import lombok.experimental.Accessors;
import org.dromara.easyes.annotation.*;
import org.dromara.easyes.annotation.rely.Analyzer;
import org.dromara.easyes.annotation.rely.FieldType;
import org.dromara.easyes.annotation.rely.IdType;

/**
 * @Description:
 * @ClassName ArcOriginalEs
 * @date: 2023.03.27 09:58
 * @Author: ldd
 */
@Data
@Accessors(chain = true)
@IndexName(value = "original_document", shardsNum = 3, replicasNum = 2, keepGlobalPrefix = true, routing = "originalRouting")
public class ArcOriginalEs {


    /**
     * es中的唯一id,字段名随便起,我这里演示用esId,你也可以用id(推荐),bizId等.
     * 如果你想自定义es中的id为你提供的id,比如MySQL中的id,请将注解中的type指定为customize或直接在全局配置文件中指定,如此id便支持任意数据类型)
     */
    @IndexId(type = IdType.CUSTOMIZE)
    private String arcOriginalId;

    @IndexField(fieldType = FieldType.TEXT, fieldData = true)
    private String fileName;


    private String arcInfoId;
    private String categoryId;
    private String qzId;
    /**
     * es返回的得分字段,字段名字随便取,只要加了@Score注解即可
     */
    @Score(decimalPlaces = 2)
    private Float score;

    /**
     * 文档内容,指定了类型及存储/查询分词器
     */
    @HighLight(mappingField = "highlightContent", fragmentSize = 200)
    @IndexField(fieldType = FieldType.TEXT, analyzer = Analyzer.IK_SMART, searchAnalyzer = Analyzer.IK_SMART)
    private String fileContent;


    /**
     * 高亮返回值被映射的字段
     */
    private String highlightContent;
}
