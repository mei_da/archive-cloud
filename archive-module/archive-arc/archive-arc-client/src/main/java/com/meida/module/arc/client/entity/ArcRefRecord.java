package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 关联记录
 *
 * @author flyme
 * @date 2022-09-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_ref_record")
@TableAlias("arr")
@ApiModel(value="ArcRefRecord对象", description="关联记录")
public class ArcRefRecord extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "refRecordId", type = IdType.ASSIGN_ID)
    private Long refRecordId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "源门类id")
    private Long srcCategoryId;

    @ApiModelProperty(value = "源门类类型")
    private Integer srcCategoryType;

    @ApiModelProperty(value = "目标门类id")
    private Long targetCategoryId;

    @ApiModelProperty(value = "源门类名称")
    private String srcCategoryName;

    @ApiModelProperty(value = "目标门类名称")
    private String targetCategoryName;

    @ApiModelProperty(value = "目标门类类型")
    private Integer targetCategoryType;

    @ApiModelProperty(value = "源门类字段")
    private Long srcArcInfoId;

    @ApiModelProperty(value = "目标门类字段")
    private Long targetArcInfoId;

    @ApiModelProperty(value = "关联类型1主 2被")
    private Integer refType;

    @ApiModelProperty(value = "关联编号")
    private Long refNo;

}
