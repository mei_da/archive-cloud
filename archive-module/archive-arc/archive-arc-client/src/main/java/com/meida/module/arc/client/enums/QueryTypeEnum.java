package com.meida.module.arc.client.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 二级门类枚举
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum QueryTypeEnum {
    /**
     * 1等于 2包含 3大于 4小于 5介于 6大于等于 7小于等于
     */
    QUERY_TYPE_1(1,"1等于"),
    QUERY_TYPE_2(2,"2包含"),
    QUERY_TYPE_3(3,"3大于"),
    QUERY_TYPE_4(4,"4小于"),
    QUERY_TYPE_5(5,"5介于");
//    QUERY_TYPE_6(6,"6大于等于"),
//    QUERY_TYPE_7(7,"7小于等于"),;

    QueryTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static QueryTypeEnum matchCode(Integer code){
        for(QueryTypeEnum obj: QueryTypeEnum.values()){
            if(obj.getCode().equals(code)){
                return obj;
            }
        }
        return null;
    }

    public static QueryTypeEnum getValue(int code){
        for (QueryTypeEnum color: values()) {
            if(color.getCode() == code){
                return  color;
            }
        }
        return null;
    }
}
