package com.meida.module.arc.client.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 温湿度记录
 *
 * @author flyme
 * @date 2022-01-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_store_temperature")
@TableAlias("ast")
@ApiModel(value="ArcStoreTemperature对象", description="温湿度记录")
public class ArcStoreTemperature extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "temperatureId", type = IdType.ASSIGN_ID)
    private Long temperatureId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "机构id")
    private Long unitId;

    @ApiModelProperty(value = "库房id")
    private Long storeId;

    @ApiModelProperty(value = "库房名称")
    private String storeName;

    @ApiModelProperty(value = "检查时间")
    private String checkTime;

    @ApiModelProperty(value = "温度")
    private BigDecimal temperature;

    @ApiModelProperty(value = "湿度")
    private BigDecimal humidity;

    @ApiModelProperty(value = "备注")
    private String remark;

}
