package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 档案到期提醒
 *
 * @author flyme
 * @date 2023-07-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_info_remind")
@TableAlias("air")
@ApiModel(value = "ArcInfoRemind对象", description = "档案到期提醒")
public class ArcInfoRemind extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "remindId", type = IdType.ASSIGN_ID)
    private Long remindId;

    @ApiModelProperty(value = "档案Id")
    private Long arcInfoId;

    @ApiModelProperty(value = "门类id")
    private Long categoryId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "被提醒人")
    private String remindUserIds;


    @ApiModelProperty(value = "提醒日期")
    private String remindDay;

    @ApiModelProperty(value = "提醒周期")
    private Integer remindType;

    @ApiModelProperty(value = "提醒时间")
    private String remindTime;

    @ApiModelProperty(value = "提醒时间")
    private Date remindDate;

    @ApiModelProperty(value = "已读状态")
    private Integer readState;

}
