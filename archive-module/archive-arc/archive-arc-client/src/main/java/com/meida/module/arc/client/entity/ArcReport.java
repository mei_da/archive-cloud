package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 报表模板
 *
 * @author flyme
 * @date 2022-01-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_report")
@TableAlias("report")
@ApiModel(value="ArcReport对象", description="报表模板")
public class ArcReport extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "reportId", type = IdType.ASSIGN_ID)
    private Long reportId;

    @ApiModelProperty(value = "1，档案 2，档案利用-销毁清册 3，档案利用-出库单，4档案销毁-销毁清册")
    private Integer reportType;

    @ApiModelProperty(value = "序号")
    private Integer seq;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "本地路径")
    private String localPath;

    @ApiModelProperty(value = "模板路径")
    private String tempPath;

    @ApiModelProperty(value = "门类id")
    private String categoryId;

    @ApiModelProperty(value = "门类名称")
    private String categoryName;

    @ApiModelProperty(value = "是否单条")
    private Integer isOne;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "是否启用")
    private Integer status;

    @ApiModelProperty(value = "bean名称")
    private String beanName;

}
