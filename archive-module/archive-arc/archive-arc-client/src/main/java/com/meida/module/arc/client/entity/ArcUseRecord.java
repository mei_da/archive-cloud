package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 档案借阅记录
 *
 * @author flyme
 * @date 2022-01-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_use_record")
@TableAlias("aur")
@ApiModel(value="ArcUseRecord对象", description="档案借阅记录")
public class ArcUseRecord extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "useRecordId", type = IdType.ASSIGN_ID)
    private Long useRecordId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "档案利用库id")
    private Long useId;

    @ApiModelProperty(value = "借阅人名称")
    private String useUserName;

    @ApiModelProperty(value = "借阅人id")
    private Long useUserId;

    //修改为输入的部门名称
    @ApiModelProperty(value = "借阅人机构id")
    private String useUnitId;

    //修改为输入的证件类型
    @ApiModelProperty(value = "借阅人证件类型1身份证2护照3工作证4其他")
    private String useCardType;

    @ApiModelProperty(value = "借阅人证件号码")
    private String useCardNum;

    @ApiModelProperty(value = "借阅人电话号码")
    private String usePhone;

    @ApiModelProperty(value = "借阅目的1工作查考2学术研究3经济建设4落实政策5个人取证6其他")
    private Integer usePurpose;

    @ApiModelProperty(value = "1电子借阅2实体借阅")
    private Integer useType;

    @ApiModelProperty(value = "出借日期")
    private String useTime;

    @ApiModelProperty(value = "归还日期")
    private String returnTime;

    @ApiModelProperty(value = "出借天数")
    private Integer borrowDay;

    @ApiModelProperty(value = "是否显示原文")
    private Integer isOriginal;

    @ApiModelProperty(value = "是否允许打印")
    private Integer isPrint;

    @ApiModelProperty(value = "是否允许下载原文")
    private Integer isDownload;

    @ApiModelProperty(value = "已下载次数")
    private Integer downloadCount;

    @ApiModelProperty(value = "允许下载次数")
    private Integer canDownloadCount;


    @ApiModelProperty(value = "已打印次数")
    private Integer printCount;

    @ApiModelProperty(value = "允许打印次数")
    private Integer canPrintCount;

    @ApiModelProperty(value = "审核人id")
    private Long verifyUserId;

    @ApiModelProperty(value = "审核状态 1待审核2驳回3待归还4已归还5逾期归还")
    private Integer verifyType;

    @ApiModelProperty(value = "驳回原因")
    private String rejectRemark;

    @ApiModelProperty(value = "审核人名称")
    private String verifyUserName;

}
