package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 档案利用库
 *
 * @author flyme
 * @date 2022-01-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_use")
@TableAlias("arcuse")
@ApiModel(value="ArcUse对象", description="档案利用库")
public class ArcUse extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "useId", type = IdType.ASSIGN_ID)
    private Long useId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "机构id")
    private Long unitId;

    @ApiModelProperty(value = "门类id")
    private Long categoryId;

    @ApiModelProperty(value = "门类名称")
    private String categoryName;

    @ApiModelProperty(value = "档案id")
    private Long arcInfoId;

    @ApiModelProperty(value = "档案号")
    private String arcNo;

    @ApiModelProperty(value = "是否移除")
    private Integer isRemove;

    @ApiModelProperty(value = "档案名称")
    private String archiveName;

    @ApiModelProperty(value = "原文数量")
    private Integer originalCount;

    @ApiModelProperty(value = "是否已经实体借阅")
    private Integer isUseSubstance;


}
