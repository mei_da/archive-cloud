package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.annotation.TableAlias;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 个人档案查询
 *
 * @author flyme
 * @date 2023-07-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_user_category")
@TableAlias("auc")
@ApiModel(value = "ArcUserCategory对象", description = "个人档案查询")
public class ArcUserCategory extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "创建人")
    private Long createBy;

    @ApiModelProperty(value = "更新人")
    private Long updateBy;

    @ApiModelProperty(value = "主键")
    @TableId(value = "aucId", type = IdType.ASSIGN_ID)
    private Long aucId;

    @ApiModelProperty(value = "用户ID ")
    private Long userId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "门类id")
    private Long categoryId;

    @ApiModelProperty(value = "自定义JSON")
    private String customData;

    @ApiModelProperty(value = "排序")
    private Integer seq;

}
