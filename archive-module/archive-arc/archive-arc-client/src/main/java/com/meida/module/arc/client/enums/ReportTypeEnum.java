package com.meida.module.arc.client.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 档案相关枚举
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ReportTypeEnum {
    //1，档案 2，档案利用-催还单 3，档案利用-出库单，4档案销毁-销毁清册
    REPORT_TYPE_1(1,"档案"),
    REPORT_TYPE_2(2,"档案利用-催还单"),
    REPORT_TYPE_3(3,"档案利用-出库单"),
    REPORT_TYPE_4(4,"档案销毁-销毁清册");

    ReportTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static ReportTypeEnum matchCode(Integer code){
        for(ReportTypeEnum obj: ReportTypeEnum.values()){
            if(obj.getCode().equals(code)){
                return obj;
            }
        }
        return null;
    }


     public static ReportTypeEnum getValue(int code){
        for (ReportTypeEnum  color: values()) {
             if(color.getCode() == code){
                     return  color;
             }
         }
         return null;
    }
}
