package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 系统字段表
 *
 * @author flyme
 * @date 2021-11-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_field_sys")
@TableAlias("afs")
@ApiModel(value="ArcFieldSys对象", description="系统字段表")
public class ArcFieldSys extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "fieldSysId", type = IdType.ASSIGN_ID)
    private Long fieldSysId;

    @ApiModelProperty(value = "类型1系统字段2业务字段")
    private Integer type;

    @ApiModelProperty(value = "字段中文名称")
    private String cnName;

    @ApiModelProperty(value = "字段英文")
    private String enName;

    @ApiModelProperty(value = "值类型1字符串2数字")
    private Integer dataType;

    @ApiModelProperty(value = "字段长度")
    private Integer length;

    @ApiModelProperty(value = "序号")
    private Integer seq;

    @ApiModelProperty(value = "默认值")
    private String defaultValue;

}
