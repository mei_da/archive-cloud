package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * excel导入记录
 *
 * @author flyme
 * @date 2021-12-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_import")
@TableAlias("import")
@ApiModel(value="ArcImport对象", description="excel导入记录")
public class ArcImport extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "importId", type = IdType.ASSIGN_ID)
    private Long importId;

    @ApiModelProperty(value = "全宗主键")
    private Long qzId;

    @ApiModelProperty(value = "门类id")
    private Long categoryId;

    @ApiModelProperty(value = "门类名称")
    private String categoryName;

    @ApiModelProperty(value = "导入文件名称")
    private String fileName;

    @ApiModelProperty(value = "部门id")
    private Long unitId;

    @ApiModelProperty(value = "部门名称")
    private String unitName;

    @ApiModelProperty(value = "导入数量")
    private Integer count;

    @ApiModelProperty(value = "导入文件路径")
    private String filePath;

}
