package com.meida.module.arc.client.handler;

import com.meida.common.mybatis.model.ResultBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * <b>功能名：ReportExportHandler</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-01-07 jiabing
 */
public interface ReportExportHandler {
    public void export(Map params, HttpServletRequest request, HttpServletResponse response,String localTempPath,String exportName);

}   