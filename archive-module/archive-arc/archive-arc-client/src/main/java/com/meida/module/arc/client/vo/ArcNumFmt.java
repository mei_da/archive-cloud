package com.meida.module.arc.client.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <b>功能名：ArcNumFmt</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-01-02 jiabing
 */
@Data
@Accessors(chain = true)
public class ArcNumFmt {
    private String fieldName;
    private String fieldId;
    private String arcConnect;
}   