package com.meida.module.arc.client.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 档案相关枚举
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ArchiveEnumInteger {
    IS_SYSTEM(1, "系统默认"),
    IS_NOT_SYSTEM(0, "非系统默认"),
    IS_ADMIN(1,"系统用户"),
    IS_DEFAULT(1,"系统默认"),
    IS_TRUE(1,"是"),
    IS_FALSE(0,"否"),
    IS_SYS_FIELD(1,"系统字段"),
    IS_BIZ_FIELD(2,"业务字段");


    ArchiveEnumInteger(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    @EnumValue
    private final Integer code;
    private final String name;

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }


}
