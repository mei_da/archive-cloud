package com.meida.module.arc.client.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.meida.common.mybatis.base.entity.AbstractEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.meida.common.mybatis.annotation.TableAlias;

/**
 * 批量关联
 *
 * @author flyme
 * @date 2021-11-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("arc_batch_ref")
@TableAlias("abr")
@ApiModel(value="ArcBatchRef对象", description="批量关联")
public class ArcBatchRef extends AbstractEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "租户号")
    private Long tenantId;

    @ApiModelProperty(value = "乐观锁")
    private Integer revision;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新人")
    private String updateBy;

    @ApiModelProperty(value = "删除标识")
    private Integer deleted;

    @ApiModelProperty(value = "业务主键")
    @TableId(value = "batchRefId", type = IdType.ASSIGN_ID)
    private Long batchRefId;

    @ApiModelProperty(value = "批量关联接口id")
    private Long arcBatchId;

    @ApiModelProperty(value = "全宗id")
    private Long qzId;

    @ApiModelProperty(value = "源门类类型")
    private Integer srcType;

    @ApiModelProperty(value = "目标门类类型")
    private Integer targetType;

    @ApiModelProperty(value = "源门类id")
    private Long srcCategoryId;

    @ApiModelProperty(value = "目标门类id")
    private Long targetCategoryId;

    @ApiModelProperty(value = "源门类名称")
    private String srcCategoryName;

    @ApiModelProperty(value = "目标门类名称")
    private String targetCategoryName;

    @ApiModelProperty(value = "源门类字段")
    private Long srcFieldId;

    @ApiModelProperty(value = "目标门类字段")
    private Long targetFieldId;

    @ApiModelProperty(value = "源门类序号")
    private Integer srcSeq;

    @ApiModelProperty(value = "目标门类序号")
    private Integer targetSeq;

}
