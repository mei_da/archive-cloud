package com.meida.module.virbox.provider.entity;

import lombok.Data;

/**
 * <b>功能名：AuthToken</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-02-14 jiabing
 */
@Data
public class AuthToken {
    //产品编码
    private String code;
    //授权机构
    private String org;
    //授权全宗数量
    private Integer qzCount;
    //授权用户数量
    private Integer userCount;
    //授权门类数量
    private Integer categoryCount;
    //授权档案数量
    private Integer categoryActCount;
    //授权机构数量
    private Integer companyCount;
    //授权时间
    private Long authTime;
    //并发用户树
    private Integer onlineCount;
    //文件转换
    private int fileConver;
    //文件合成
    private int fileConflate;
    //审批流授权
    private int approve;
    //0 jwt  1加密狗
    private Integer tokenType;


}
