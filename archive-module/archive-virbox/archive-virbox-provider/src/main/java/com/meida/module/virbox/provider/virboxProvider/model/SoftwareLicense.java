package com.meida.module.virbox.provider.virboxProvider.model;

import java.io.Serializable;
import java.util.Date;

public class SoftwareLicense implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 授权使用人
	 */
	private String enduser;
	/**
	 * 授权开始时间
	 */
	private Date startTime;
	/**
	 * 授权结束时间
	 */
	private Date endTime;
	/**
	 * 授权ID
	 */
	private String licenseId;
	private String licenseType;
	private Date firstUseTime;
	private boolean enable;
	
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	public String getLicenseType() {
		return licenseType;
	}
	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}
	public Date getFirstUseTime() {
		return firstUseTime;
	}
	public void setFirstUseTime(Date firstUseTime) {
		this.firstUseTime = firstUseTime;
	}
	/**
	 * 是否限制到期时间  默认为限制到期时间
	 */
	private boolean limited=true;
	public String getEnduser() {
		return enduser;
	}
	public void setEnduser(String enduser) {
		this.enduser = enduser;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getLicenseId() {
		return licenseId;
	}
	public void setLicenseId(String licenseId) {
		this.licenseId = licenseId;
	}
	public boolean isLimited() {
		return limited;
	}
	public void setLimited(boolean limited) {
		this.limited = limited;
	}
	
}
