package com.meida.module.virbox.provider.tokenProvider.jwt;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <b>功能名：JwtPaswer</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-02-14 jiabing
 */
public class JwtPaswer {
    private static String jwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJxekNvdW50IjoxLCJ1c2VyQ291bnQiOjAsImNhdGVnb3J5Q291bnQiOjAsImNhdGVnb3J5QWN0Q291bnQiOjAsImNvbXBhbnlDb3VudCI6MCwiYXV0aFRpbWUiOiIwIiwib25saW5lQ291bnQiOjAsImV4cGlyZWRUaW1lIjoiMTY0NDgzMTgyMyJ9.Ta4QckQzUQxj0DPMv5TzSxgbWGuyIrHSQvLUJuBYAFo";
    private static String sign = "36XW7SS55GD4D3NDG1HDFGS4887SVTJGc2RHVmtYMS9YV1JnRnZ2QUdDTWdNekZNZFdVNWg0OTIrZjVYanpqTT0";
    public static void main(String[] args) {
       System.out.println(createJWT());
        //System.out.println(DatatypeConverter.parseBase64Binary(sign));
        Claims c = parseJWT(jwt,sign);
        System.out.println(c.get("expiredTime"));
    }
    public static Claims parseJWT(String jwt,String sign){
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(sign))
                .parseClaimsJws(jwt).getBody();
        return claims;
    }

    public static String createJWT() {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary( sign);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
       String payload = "{\n" +
               "    \"qzCount\": \"1\",\n" +
               "    \"userCount\": \"0\",\n" +
               "    \"categoryCount\": \"0\",\n" +
               "    \"categoryActCount\": \"2\",\n" +
               "    \"companyCount\": \"0\",\n" +
               "    \"authTime\": \"0\",\n" +
               "    \"onlineCount\": \"3\",\n" +
               "    \"expiredTime\": \"1644769064\"\n" +
               "}";

        JwtBuilder builder = Jwts.builder().setPayload(payload)
                .signWith(signatureAlgorithm, signingKey);
        return builder.compact();

    }
}   