package com.meida.module.virbox.provider.virboxProvider.model;

public class SLMLicense {
	public String license_id;
	public boolean enable;
	public String guid;
	public Long first_use_time;
	public Long last_update_timestamp;
	public String developer_id;
	public Long start_time;
	public Long end_time;
	public String sn;
	public String type;
	public Long span_time;
}
