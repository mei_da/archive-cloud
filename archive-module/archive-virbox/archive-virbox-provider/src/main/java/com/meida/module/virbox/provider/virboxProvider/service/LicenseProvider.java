package com.meida.module.virbox.provider.virboxProvider.service;


import com.meida.module.virbox.provider.entity.AuthToken;
import com.meida.module.virbox.provider.virboxProvider.model.SoftwareLicense;

public interface LicenseProvider {
	/**
	 * 获取当前授权
	 * @return
	 */
	public SoftwareLicense getLicense();
	/**
	 * 实时读取授权信息
	 * @return
	 */
	public SoftwareLicense readLicense();
	/**
	 * 刷新授权信息(重新获取，并替换老的授权)
	 * @return
	 */
	public SoftwareLicense refeash();

	public AuthToken getAuthToken();
//	/**
//	 * 获取授权使用单位
//	 * @return
//	 */
//	public String getHolder();
}
