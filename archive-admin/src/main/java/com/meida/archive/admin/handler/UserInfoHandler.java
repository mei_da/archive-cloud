package com.meida.archive.admin.handler;

import cn.hutool.core.collection.CollUtil;
import com.meida.common.base.handler.AdminUserInfoHandler;
import com.meida.common.security.OpenUser;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.admin.provider.service.BaseRoleService;
import com.meida.module.admin.provider.service.BaseUserService;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysCompanyService;
import com.meida.module.system.provider.service.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description:
 * @ClassName AdminUserInfoHandler
 * @date: 2023.05.28 09:14
 * @Author: ldd
 */
@Component("adminUserInfoHandler")
public class UserInfoHandler implements AdminUserInfoHandler {

    @Autowired
    SysCompanyService sysCompanyService;

    @Autowired
    SysDeptService sysDeptService;

    @Autowired
    BaseUserService baseUserService;

    @Autowired
    BaseRoleService baseRoleService;


    @Override
    public void initOpenUser(OpenUser openUser) {
        Map<String, Object> attrs = openUser.getAttrs();

        BaseUser user = baseUserService.getUserById(openUser.getUserId());
        SysCompany company = sysCompanyService.getById(user.getCompanyId());
        if (null != company) {
            attrs.put("companyName", company.getCompanyName());
        } else {
            attrs.put("companyName", "");
        }
        SysDept dept = sysDeptService.getById(user.getDeptId());
        if (null != dept) {
            attrs.put("deptId", user.getDeptId());
            attrs.put("deptName", dept.getDeptName());
        } else {
            attrs.put("deptId", "");
            attrs.put("deptName", "");
        }
        List<String> list = baseRoleService.selectRoleNamesByUserId(user.getUserId());
        if (CollUtil.isNotEmpty(list)) {
            attrs.put("roleName", list.stream().collect(Collectors.joining(",")));
        } else {
            attrs.put("roleName", "");
        }
        openUser.setAttrs(attrs);

    }
}
