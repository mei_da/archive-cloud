package com.meida.archive.admin;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.dromara.easyes.starter.register.EsMapperScan;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;


/**
 * 平台基础服务
 * 提供系统用户、权限分配、资源、客户端管理
 *
 * @author zyf
 */
@EnableCaching
@EnableFeignClients(basePackages = {"com.meida"})
//@EnableDiscoveryClient
@SpringBootApplication(exclude = {FlywayAutoConfiguration.class}, scanBasePackages = "com.meida")
@MapperScan(basePackages = "com.meida.**.mapper")
@ComponentScan(basePackages = {"com.meida"})
@EnableKnife4j
//@EnableESTools(basePackages={"com.meida.module.arc.provider.repository"},entityPath = {"com.meida.module.arc.client.entity"})
@EnableAsync
@EsMapperScan("com.meida.module.arc.provider.repository")

public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }
}
