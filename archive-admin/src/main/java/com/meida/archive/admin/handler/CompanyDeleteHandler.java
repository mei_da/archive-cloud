package com.meida.archive.admin.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.DeleteInterceptor;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.provider.service.SysCompanyService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 机构删除扩展
 *
 * @author zyf
 */
@Component
@AllArgsConstructor
public class CompanyDeleteHandler implements DeleteInterceptor {

    @Autowired
    private SysCompanyService sysCompanyService;

    @Override
    public ResultBody validate(CriteriaDelete cd, EntityMap params) {

        SysCompany sysCompany = sysCompanyService.getById(params.get("ids").toString());

        return sysCompany.getArcInfoCount() > 0 ? ResultBody.failed("该全宗下有档案数据，请先删除档案数据") : ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaDelete criteriaDelete, EntityMap entityMap) {

    }
}
