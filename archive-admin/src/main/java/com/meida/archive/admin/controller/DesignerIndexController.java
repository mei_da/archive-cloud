package com.meida.archive.admin.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description: 流程设计器Controller
 * @Author: zyf
 * @Date: 2022-05-13
 */
@Controller
@RequestMapping("/act/common/")
public class DesignerIndexController {
    private static final Log logger = LogFactory.getLog(DesignerIndexController.class);


    /**
     * 设计器界面地址
     */
    private static final String DESIGN_INDEX = "designer";

    /**
     * 自定义流程id
     */
    private static final String PROCESS_ID = "id";
    /**
     * 流程定义ID
     */
    private static final String REAL_PROC_DEF_ID = "realProcDefId";

    /**
     * 流程分类ID
     */
    private static final String TYPE_ID = "typeid";


    /**
     * 设计器首页跳转
     *
     * @param request
     * @param id            流程id
     * @param realProcDefId 真实流程定义id
     * @param token         用户token
     * @param map
     * @return
     */
    @RequestMapping("/designer")
    //public String index(HttpServletRequest request, @RequestParam(name = PROCESS_ID, required = false, defaultValue = "") String id,@RequestParam(name = REAL_PROC_DEF_ID, required = false, defaultValue = "") String realProcDefId, @RequestParam(name = FlowableConstant.TOKEN_KEY, required = false, defaultValue = "") String token, Model map) {
    public String index(HttpServletRequest request, @RequestParam(name = PROCESS_ID, required = false, defaultValue = "") String id, @RequestParam(name = REAL_PROC_DEF_ID, required = false, defaultValue = "") String realProcDefId, @RequestParam(name = "token", required = false, defaultValue = "") String token, Model map) {
        logger.info(" index 登录令牌token： " + token);
        //校验token有效性
        //TokenUtils.verifyToken(request, sysBaseApi, redisUtil);
        //获取请求父路径
        //String domainUrl = this.getBaseUrl(request);
        map.addAttribute("token", token);
        //map.addAttribute(FlowableConstant.DOMIAN_URL, domainUrl);
        map.addAttribute(TYPE_ID, "oa");
        map.addAttribute(PROCESS_ID, id);
        map.addAttribute(REAL_PROC_DEF_ID, realProcDefId);
        return DESIGN_INDEX;
    }

    /**
     * 获取服务器地址 TODO [可以放到底层]
     *
     * @param request
     * @return
     */
   /* private String getBaseUrl(HttpServletRequest request) {
        //1.【微服务环境下】用户、部门等接口无法获取数据问题--------
        String xGatewayBasePath = request.getHeader("X_GATEWAY_BASE_PATH");
        if (oConvertUtils.isNotEmpty(xGatewayBasePath)) {
            log.info("x_gateway_base_path = " + xGatewayBasePath);
            return xGatewayBasePath;
        }

        //2. SSL认证之后，request.getScheme()获取不到https的问题记录
        // https://blog.csdn.net/weixin_34376986/article/details/89767950
        String scheme = request.getHeader("X-Forwarded-Scheme");
        if (oConvertUtils.isEmpty(scheme)) {
            scheme = request.getScheme();
        }

        //3.常规操作
        String serverName = request.getServerName();
        int serverPort = request.getServerPort();
        String contextPath = request.getContextPath();


        //返回 host domain
        String baseDomainPath = null;
        if (80 == serverPort) {
            baseDomainPath = scheme + "://" + serverName + contextPath;
        } else {
            baseDomainPath = scheme + "://" + serverName + ":" + serverPort + contextPath;
        }
        log.info("================getBaseUrl==============: " + baseDomainPath);
        return baseDomainPath;
    }*/


}
