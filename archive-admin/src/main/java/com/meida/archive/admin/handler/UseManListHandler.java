package com.meida.archive.admin.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.utils.ConvertUtils;
import com.meida.module.admin.client.entity.BaseAccount;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.admin.provider.service.BaseAccountService;
import com.meida.module.admin.provider.service.BaseRoleService;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.client.entity.SysDept;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 利用人列表扩展
 *
 * @author zyf
 */
@Component
@AllArgsConstructor
public class UseManListHandler implements PageInterceptor<BaseUser> {

    private final BaseAccountService accountService;

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        String certType=params.get("certType");
        cq.select(BaseUser.class, "*");
        cq.eq(BaseUser.class, "companyId");
        //利用人类型
        cq.eq("userType", 2);
        cq.like("certType", certType);
    }

    @Override
    public void complete(CriteriaQuery<BaseUser> cq, List<EntityMap> result, EntityMap extra) {
        for (EntityMap entityMap : result) {
            Long userId = entityMap.getLong("userId");
            List<BaseAccount> accountList=accountService.selectByUserId(userId);
            if(FlymeUtils.isNotEmpty(accountList)){
                entityMap.put("account",accountList.get(0).getAccount());
            }
        }
    }
}
