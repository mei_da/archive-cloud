package com.meida.archive.admin.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.security.OpenHelper;
import com.meida.module.file.client.entity.SysFile;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * 下载中心文件列表
 *
 * @author zyf
 */
@Component
@AllArgsConstructor
public class SysFileListHandler implements PageInterceptor<SysFile> {

    @Override
    public void prepare(CriteriaQuery cq, PageParams pageParams, EntityMap params) {
        Long userId = OpenHelper.getUserId();
        cq.eq(SysFile.class, "createUser", userId);
        cq.orderByAsc("downloadCount");
        cq.orderByDesc("createTime");
    }

    @Override
    public void complete(CriteriaQuery<SysFile> cq, List<EntityMap> result, EntityMap extra) {
        for (EntityMap entityMap : result) {
            Long fileSize = entityMap.getLong("fileSize");
            if(FlymeUtils.isNotEmpty(fileSize)) {
                if (fileSize < 1000000000) {
                    BigDecimal size=new BigDecimal(fileSize).divide(new BigDecimal("1000000"),2, RoundingMode.HALF_DOWN);
                    entityMap.put("fileSizeStr", size+ "MB");
                }
                if (fileSize > 1000000000) {
                    BigDecimal size=new BigDecimal(fileSize).divide(new BigDecimal("1000000000"),2, RoundingMode.HALF_DOWN);
                    entityMap.put("fileSizeStr", size + "G");
                }
            }
        }
    }
}
