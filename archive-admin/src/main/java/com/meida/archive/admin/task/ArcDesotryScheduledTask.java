package com.meida.archive.admin.task;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.utils.DateUtil;
import com.meida.module.arc.client.entity.ArcDestory;
import com.meida.module.arc.client.entity.ArcDestorySetting;
import com.meida.module.arc.client.enums.ArcDestoryStatusEnum;
import com.meida.module.arc.client.enums.ArchiveEnumInteger;
import com.meida.module.arc.provider.service.ArcDestorySettingService;
import com.meida.module.arc.provider.service.SyncService;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.provider.service.SysCompanyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <b>功能名：ArcDesotryScheduledTask</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-02-25 jiabing
 */

@Component
// 启用定时任务
@EnableScheduling
@Slf4j
public class ArcDesotryScheduledTask {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private ArcDestorySettingService arcDestorySettingService;

    @Autowired
    private SyncService syncService;
    
    @Autowired
    @Lazy
    private SysCompanyService companyService;

    // 每 小时执行一次执行一次任务。
    @Scheduled(cron = "0 0 * * * ?")
    public void performingTasks() {
        log.info("执行档案销毁定时任务时间：{}", DateUtil.date2Str(new Date(),"yyyy-MM-dd HH:mm:ss"));
        //查询所有全宗
        List<SysCompany> list = companyService.list();
        list.forEach(item->{
            Map param = new HashMap();
            param.put("qzId",item.getCompanyId());
            ResultBody<ArcDestorySetting> destorySettingResultBody = arcDestorySettingService.defaultSetting(param);
            ArcDestorySetting setting = destorySettingResultBody.getData();
            syncService.destoryArcTask(setting);
        });

    }
}   