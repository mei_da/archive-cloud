package com.meida.archive.admin.task;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.utils.DateUtils;
import com.meida.common.utils.RedisUtils;
import com.meida.module.arc.client.entity.ArcCategory;
import com.meida.module.arc.client.entity.ArcInfo;
import com.meida.module.arc.client.entity.ArcInfoRemind;
import com.meida.module.arc.provider.service.ArcCategoryService;
import com.meida.module.arc.provider.service.ArcInfoRemindService;
import com.meida.module.arc.provider.service.ArcInfoService;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysDeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <b>功能名：ArcDesotryScheduledTask</b><br>
 * <b>说明：</b><br>
 * <b>著作权：</b> Copyright (C) 2021 HUIFANEDU  CORPORATION<br>
 * <b>修改履历：
 *
 * @author 2022-02-25 jiabing
 */

@Component
// 启用定时任务
@EnableScheduling
@Slf4j
@DS("sharding")

public class ArcInfoRemindScheduledTask {


    @Autowired
    private ArcInfoRemindService arcInfoRemindService;

    @Autowired
    private ArcInfoService arcInfoService;

    @Autowired
    private SysDeptService sysDeptService;
    @Autowired
    private ArcCategoryService categoryService;

    @Autowired
    RedisUtils redisUtils;

    // 凌晨查询
    @Scheduled(cron = "0 0 1 * * ?")
    public void performingTasks() {
        //查询当天需要提醒的档案
        List<ArcInfoRemind> list = arcInfoRemindService.list(new QueryWrapper<ArcInfoRemind>().eq("to_days(remindDate)", DateUtils.toDays()));
        list.forEach(item -> {
            long l = item.getRemindDate().getTime() - System.currentTimeMillis();
            //mqTemplate.sendMessage(QueueConstants.QUEUE_REMIND, arcInfoRemind.getRemindId(), Integer.valueOf(l + ""));
            redisUtils.set("infoRemind:" + item.getRemindId(), item.getRemindId(), l / 1000);

        });

    }

    //@PostConstruct
    void change() {

        List<ArcCategory> list1 = categoryService.list();

        for (ArcCategory arcCategory : list1) {
            List<ArcInfo> list = arcInfoService.list(new QueryWrapper<ArcInfo>().lambda().eq(ArcInfo::getCategoryId, arcCategory.getCategoryId()));
            Map<Long, List<ArcInfo>> deptCollect6 = list.stream().collect(Collectors.groupingBy(ArcInfo::getUnitId));
            System.out.println(deptCollect6);

        }
        List<SysDept> list = sysDeptService.list();
        for (SysDept sysDept : list) {
            sysDept.getDeptId();
            sysDept.getCompanyId();

            long count = arcInfoService.count(new QueryWrapper<ArcInfo>().lambda().eq(ArcInfo::getUnitId, sysDept.getDeptId()));
            if (count > 0) {
                sysDept.setArcInfoCount(sysDept.getArcInfoCount() + (int) count);
                sysDept.updateById();
            }


        }

    }
}
