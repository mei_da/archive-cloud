package com.meida.archive.admin.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description: 前端页面
 * @Author: zyf
 * @Date: 2022-08-01
 */
@Controller("webIndexController")
@Slf4j
public class WebIndexController {

    /**
     * @param request
     * @param map
     * @return
     */
    @RequestMapping("/login")
    public String preview(HttpServletRequest request, Model map) {
        return "login";
    }

}