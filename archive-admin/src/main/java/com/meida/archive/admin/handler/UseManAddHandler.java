package com.meida.archive.admin.handler;

import com.meida.base.provider.handler.BaseAdminUserAddHandler;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.handler.BaseCheckAuthHandler;
import com.meida.common.base.service.BaseAdminUserService;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.SaveInterceptor;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.RedisUtils;
import com.meida.common.utils.StringUtils;
import com.meida.common.utils.event.FlymeEventClient;
import com.meida.module.admin.client.constants.BaseConstants;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.admin.provider.service.BaseAccountService;
import com.meida.module.admin.provider.service.BaseUserService;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysCompanyService;
import com.meida.module.system.provider.service.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 后台用户添加扩展
 *
 * @author zyf
 */
@Component
public class UseManAddHandler extends BaseAdminUserAddHandler {


}
