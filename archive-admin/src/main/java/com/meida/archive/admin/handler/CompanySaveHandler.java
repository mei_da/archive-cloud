package com.meida.archive.admin.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.PageInterceptor;
import com.meida.common.mybatis.interceptor.SaveInterceptor;
import com.meida.common.mybatis.model.PageParams;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaQuery;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.utils.ApiAssert;
import com.meida.common.utils.ConvertUtils;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.admin.provider.service.BaseRoleService;
import com.meida.module.arc.provider.service.ArcAuthTokenService;
import com.meida.module.arc.provider.service.ArcSettingService;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysCompanyService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 全宗添加扩展
 *
 * @author zyf
 */
@Component
@AllArgsConstructor
public class CompanySaveHandler implements SaveInterceptor<SysCompany> {

    private final SysCompanyService companyService;

    private final ArcSettingService settingService;

    private final ArcAuthTokenService arcAuthTokenService;

    /**
     * 添加前校验
     *
     * @param cs
     * @param params
     * @return
     */
    @Override
    public ResultBody validate(CriteriaSave cs, EntityMap params) {
        Long companyCount = FlymeUtils.getLong(companyService.count());
        //检测全宗数量
        arcAuthTokenService.checkCompanyCount(companyCount.intValue());
        return ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaSave criteriaSave, EntityMap entityMap, SysCompany company) {

    }
}
