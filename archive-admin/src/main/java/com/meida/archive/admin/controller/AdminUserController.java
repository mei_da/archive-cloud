package com.meida.archive.admin.controller;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.security.OpenHelper;
import com.meida.common.utils.RedisUtils;
import com.meida.module.admin.provider.mapper.BaseUserDeptMapper;
import com.meida.module.admin.provider.service.BaseUserService;
import com.meida.module.file.provider.service.SysFileService;
import com.meida.module.system.provider.service.SysDeptService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @Description:
 * @ClassName AdminUserController
 * @date: 2023.05.29 13:48
 * @Author: ldd
 */
@RestController
public class AdminUserController {

    @Autowired
    private BaseUserService baseUserService;

    @Autowired
    private SysFileService sysFileService;

    @Autowired
    public RedisUtils redisUtils;
    @Autowired
    private SysDeptService sysDeptService;

    @Autowired
    private BaseUserDeptMapper baseUserDeptMapper;
    /**
     * 更改头像
     *
     * @param file
     * @return
     */
    @ApiOperation(value = "用户-更改头像", notes = "更改头像")
    @PostMapping(value = "/current/user/updateAvatar")
    public ResultBody updateAvatar(HttpServletRequest request, @RequestParam(value = "file") MultipartFile file) {
        EntityMap upload = sysFileService.upload(request, file, OpenHelper.getUserId(), "", "");
        return baseUserService.edit(new HashMap<String, Object>() {{
            put("userId", OpenHelper.getUserId());
            put("avatar", upload.get("ossPath"));
        }});
    }

   /* @ApiOperation(value = "查询用户已分配的部门")
    @GetMapping(value = "/current/treeByUserId")
    public ResultBody listByUserId(@RequestParam Long userId,@RequestParam(required = false) Long parentId, @RequestParam(required = false) Long companyId) {

        List<ArcReport> list = redisUtils.getList(ArchiveConstants.ARC_REPORT_PREFIX);
        if(CollUtil.isEmpty(list)){
            sysDeptService.loadData2Redis();
            list = redisUtils.getList(ArchiveConstants.ARC_REPORT_PREFIX);
        }

        List<Long> longs = baseUserDeptMapper.selectUserDeptIds(userId);

        //List<SysDept> list = bizService.treeByUserId(userId,parentId,companyId);
        return ResultBody.ok(list);
    }*/

}
