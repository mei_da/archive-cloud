package com.meida.archive.admin.handler;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.meida.common.base.entity.EntityMap;
import com.meida.common.mybatis.interceptor.DeleteInterceptor;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaDelete;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.admin.provider.service.BaseUserService;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysDeptService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 机构删除扩展
 *
 * @author zyf
 */
@Component
@AllArgsConstructor
public class DeptDeleteHandler implements DeleteInterceptor {

    @Autowired
    private SysDeptService sysDeptService;

    @Autowired
    private BaseUserService userService;

    @Override
    public ResultBody validate(CriteriaDelete cd, EntityMap params) {

        SysDept sysDept = sysDeptService.getById(params.get("ids").toString());
        long count = userService.count(new QueryWrapper<BaseUser>().lambda().eq(BaseUser::getDeptId, sysDept.getDeptId()));

        String msg = sysDept.getArcInfoCount() > 0 ? "该部门下有档案数据，请先删除档案数据" : count > 0 ? "该部门下有系统用户，请先删除系统用户" : "";
        return StrUtil.isNotEmpty(msg) ? ResultBody.failed(msg) : ResultBody.ok();
    }

    @Override
    public void prepare(CriteriaDelete criteriaDelete, EntityMap entityMap) {

    }
}
