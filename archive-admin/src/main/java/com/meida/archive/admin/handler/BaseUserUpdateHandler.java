package com.meida.archive.admin.handler;

import com.meida.common.base.entity.EntityMap;
import com.meida.common.base.utils.FlymeUtils;
import com.meida.common.mybatis.interceptor.SaveInterceptor;
import com.meida.common.mybatis.interceptor.UpdateInterceptor;
import com.meida.common.mybatis.model.ResultBody;
import com.meida.common.mybatis.query.CriteriaSave;
import com.meida.common.mybatis.query.CriteriaUpdate;
import com.meida.module.admin.client.entity.BaseUser;
import com.meida.module.arc.provider.service.ArcAuthTokenService;
import com.meida.module.arc.provider.service.ArcSettingService;
import com.meida.module.system.client.entity.SysCompany;
import com.meida.module.system.client.entity.SysDept;
import com.meida.module.system.provider.service.SysCompanyService;
import com.meida.module.system.provider.service.SysDeptService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * 用户更新扩展
 *
 * @author zyf
 */
@Component
@AllArgsConstructor
public class BaseUserUpdateHandler implements UpdateInterceptor<BaseUser> {

    private final SysDeptService sysDeptService;

    @Override
    public void prepare(CriteriaUpdate cu, EntityMap params, BaseUser baseUser) {
        Long deptId = baseUser.getDeptId();
        if (FlymeUtils.isNotEmpty(deptId)) {
            SysDept sysDept = sysDeptService.getById(deptId);
            baseUser.setCompanyId(sysDept.getCompanyId());
            params.put("companyId", sysDept.getCompanyId());
        }
    }
}
