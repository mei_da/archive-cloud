-- ----------------------------
--  档案评论
-- ----------------------------

create table if not exists arc_info_comment
(
    commentId      bigint        not null comment '主键'
        primary key,
    arcInfoId          bigint        null comment '档案Id',
    categoryId                bigint        null comment '门类id',
    qzId                bigint        null comment '全宗id',
    replyUserId    bigint        null comment '评论人',
    replyCommentId bigint        null comment '评论ID',
    replyToUserId  bigint        null comment '被评论人',
    replyImages    varchar(1000) null comment '评论图片',
    replyContent   varchar(1024) null comment '评论内容',
    replyState     int           null comment '评论状态',
    dzNum          int           null comment '点赞数量',
    createTime     datetime      null comment '创建时间',
    updateTime     datetime      null comment '更新时间'
)
comment '档案评论' charset = utf8mb4;

-- ----------------------------
--  档案到期提醒
-- ----------------------------

create table if not exists arc_info_remind
(
    remindId      bigint        not null comment '主键'
        primary key,
    arcInfoId          bigint        null comment '档案Id',
    categoryId                bigint        null comment '门类id',
    qzId                bigint        null comment '全宗id',
    remindUserIds  text       null comment '被提醒人',
    remindDay      varchar(32)   null comment '提醒日期',
    remindType      varchar(32)   null comment '提醒周期',
    remindTime      varchar(32)   null comment '提醒时间',
    remindDate      datetime   null comment '提醒时间',
    readState     int           null comment '已读状态',
    createTime     datetime      null comment '创建时间',
    updateTime     datetime      null comment '更新时间'
)
comment '档案到期提醒' charset = utf8mb4;
