-- ----------------------------
--  档案保管菜单
-- ----------------------------

INSERT INTO base_menu (menuId, parentId, menuCode, menuName, menuDesc, scheme, path, icon, target, menuGroup, menuLayout, hiddenHeaderContent, priority, status, isPersist, serviceId, createTime, updateTime) VALUES (3004, 3000, 'recyclearchiveKeeping', '档案保管', '档案保管', '/', '/recycle/archiveKeeping/index', '', '_self', 2, 'PageView', 1, 31, 1, 0, 'meida-base-provider', null, null);

INSERT INTO base_authority (authorityId, authority, menuId, apiId, actionId, status, createTime, updateTime) VALUES (3004, 'MENU_recyclearchiveKeeping', 3004, null, null, 1, '2023-11-13 17:33:34', '2023-11-13 17:33:38');


INSERT INTO base_action (actionId, actionCode, actionName, actionDesc, menuId, priority, status, createTime, updateTime, isPersist, serviceId) VALUES (1723998792932802562, 'recycle_keeping_remove', '删除', '', 3004, 0, 1, '2023-11-13 17:38:37', '2023-11-13 17:38:37', 0, 'meida-base-provider');
INSERT INTO base_action (actionId, actionCode, actionName, actionDesc, menuId, priority, status, createTime, updateTime, isPersist, serviceId) VALUES (1723998625504575490, 'recycle_keeping_recovery', '还原', '', 3004, 0, 1, '2023-11-13 17:37:57', '2023-11-13 17:37:57', 0, 'meida-base-provider');
INSERT INTO base_action (actionId, actionCode, actionName, actionDesc, menuId, priority, status, createTime, updateTime, isPersist, serviceId) VALUES (1723998483334447106, 'recycle_keeping_clear', '清空', '', 3004, 0, 1, '2023-11-13 17:37:23', '2023-11-13 17:37:23', 0, 'meida-base-provider');

INSERT INTO base_authority (authorityId, authority, menuId, apiId, actionId, status, createTime, updateTime) VALUES (1723998483368001537, 'ACTION_recycle_keeping_clear', null, null, 1723998483334447106, 1, '2023-11-13 17:37:23', null);
INSERT INTO base_authority (authorityId, authority, menuId, apiId, actionId, status, createTime, updateTime) VALUES (1723998625529741314, 'ACTION_recycle_keeping_recovery', null, null, 1723998625504575490, 1, '2023-11-13 17:37:57', null);
INSERT INTO base_authority (authorityId, authority, menuId, apiId, actionId, status, createTime, updateTime) VALUES (1723998792949579777, 'ACTION_recycle_keeping_remove', null, null, 1723998792932802562, 1, '2023-11-13 17:38:37', null);



