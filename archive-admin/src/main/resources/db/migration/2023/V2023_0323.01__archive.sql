-- ----------------------------
-- Table structure for flywa_demo
-- ----------------------------
CREATE TABLE flywa_demo (
 id varchar(32) NOT NULL COMMENT '主键',
 name varchar(32) NOT NULL COMMENT 'name',
  CONSTRAINT flywa_demo_PK PRIMARY KEY (id)
)
ENGINE=InnoDB COMMENT = 'demo';

drop table flywa_demo;


