-- ----------------------------
--  组合查询
-- ----------------------------

create table if not exists arc_user_category
(
    createBy   bigint   null comment '创建人',
    createTime datetime null comment '创建时间',
    updateBy   bigint   null comment '更新人',
    updateTime datetime null comment '更新时间',
    aucId  bigint   not null comment '主键'
        primary key,
    userId     bigint   null comment '用户ID ',
    qzId       bigint   null comment '全宗id',
    categoryId bigint   null comment '门类id',
    customData text   null comment '自定义JSON',
    seq int   null comment '排序'
)
 comment '个人档案查询' charset = utf8mb4;

