-- ----------------------------
--  档案收藏
-- ----------------------------
create table if not exists arc_info_collect
(
    createBy   bigint   null comment '创建人',
    createTime datetime null comment '创建时间',
    updateBy   bigint   null comment '更新人',
    updateTime datetime null comment '更新时间',
    collectId  bigint   not null comment '主键'
        primary key,
    userId     bigint   null comment '用户ID ',
    arcInfoId  bigint   null comment '档案ID',
    qzId       bigint   null comment '全宗id',
    categoryId bigint   null comment '门类id'
)
    comment '档案收藏' charset = utf8mb4;



