-- ----------------------------
--  导入账号
-- ----------------------------

create table if not exists base_user_import_record
(
    tenantId   bigint        null comment '租户号',
    revision   int           null comment '乐观锁',
    createBy   varchar(32)   null comment '创建人',
    createTime datetime      null comment '创建时间',
    updateBy   varchar(32)   null comment '更新人',
    updateTime datetime      null comment '更新时间',
    deleted    int default 0 null comment '删除标识',
    importId   bigint        not null comment '业务id'        primary key,
    fileName   varchar(255)  null comment '文件名',
    companyId  bigint        null,
    deptId     varchar(255)  null comment '所属机构',
    importNum  int default 0 null comment '导入数量',
    successNum int default 0 null comment '成功数量',
    failNum    int default 0 null comment '失败数量'
)
    comment '用户导入' charset = utf8mb4;
 create table if not exists base_user_import_record_logs
(
    tenantId   bigint        null comment '租户号',
    revision   int           null comment '乐观锁',
    createBy   varchar(32)   null comment '创建人',
    createTime datetime      null comment '创建时间',
    updateBy   varchar(32)   null comment '更新人',
    updateTime datetime      null comment '更新时间',
    deleted    int default 0 null comment '删除标识',
    importId   bigint        not null comment '业务id',
    logsId     bigint        not null comment '业务id' primary key,
    fileName   varchar(255)  null comment '文件名',
    userId     bigint        null comment '用户ID',
    sourceData varchar(255)  null comment '用户ID',
    errMsg     varchar(255)  null comment '错误描述',
    nickName   varchar(64)   null comment '姓名',
    userName   varchar(64)   null comment '账号',
    userDesc   varchar(126)  null comment '备注',
    password   varchar(64)   null comment '密码',
    companyId  bigint        null comment '机构全宗号',
    deptId     bigint        null comment '部门机构代码'
)
    comment '用户导入日志记录'  charset = utf8mb4;



