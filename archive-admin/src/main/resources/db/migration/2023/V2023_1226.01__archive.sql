-- ----------------------------
--  水印设置
-- ----------------------------

update arc_setting
set customExpand='{"original":["preview","print","export"],"screen":0,"transparent":20,"img":0,"imgContent":{"imgUrl":"","position":"leftTop"},"text":1,"textContent":{"value":"涉密文件","family":"Vedana","font":14,"rotate":20,"lineHeight":20,"position":"tile","uuid":1,"date":1},"footer":"1","footerContent":{"family":"微软雅黑","font":14}}'
where settingId = 1
