-- ----------------------------
--  文件类型
-- ----------------------------
INSERT INTO `sys_file_type` (`fileTypeId`, `typeName`, `contentType`, `allowConvert`, `allowOcr`, `allowMaxSize`, `allowFbl`, `typeGroup`, `state`, `useState`, `fileIcon`, `remark`, `createTime`, `updateTime`) VALUES ('26', 'wav', 'audio/wav', '0', '0', '100', NULL, '3', '1', '1', 'http://127.0.0.1:85/archive/default/file/wav.png', 'wav格式音频', NULL, NULL);
INSERT INTO `sys_file_type` (`fileTypeId`, `typeName`, `contentType`, `allowConvert`, `allowOcr`, `allowMaxSize`, `allowFbl`, `typeGroup`, `state`, `useState`, `fileIcon`, `remark`, `createTime`, `updateTime`) VALUES ('27', 'mpg', 'video/mpg', '2', '0', '100', NULL, '4', '1', '1', 'http://127.0.0.1:85/archive/default/file/mpg.png', 'mpg格式视频', NULL, NULL);
update sys_file_type set allowConvert=0 ,allowOcr=0 where 1=1;
update sys_file_type set allowConvert=1 where fileTypeId in(7,8,12,13,18,19,20,21,22,24);
update sys_file_type set allowConvert=2 where fileTypeId in(16,27);
update sys_file_type set allowOcr=1 where fileTypeId in(6,7);
