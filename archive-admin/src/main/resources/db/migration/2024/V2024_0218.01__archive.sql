-- ----------------------------
--  同步IAM机构人员
-- ----------------------------
delete
from sys_config2
where configType = 'iam';
INSERT INTO sys_config2 (configId, configKey, configVal, configType, updateTime, createTime, remark)
VALUES (1441234756950319116, 'PULL_ORG', '0', 'iam', null, null, '最近一次拉取机构时间戳');
INSERT INTO sys_config2 (configId, configKey, configVal, configType, updateTime, createTime, remark)
VALUES (1441234756950319117, 'PULL_USER', '0', 'iam', null, null, '最近一次拉取人员时间戳');
