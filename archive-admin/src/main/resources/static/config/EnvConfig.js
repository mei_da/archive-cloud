const VUE_VERSION_TYPE = 2 // 版本类型 1=网络版 2=单机版
var EnvConfig = {
  BACK_API_PREFIX: "/base", // 后台接口地址代理前缀
  key: "local", // localStorage和sessionStorage存储前缀
  VUE_VERSION_TYPE: VUE_VERSION_TYPE,
  reportType: 'kkfile', // 报表类型 doc=Word文档直接下载 kkfile=word在kk预览 pdf=pdf文件流预览
  donwloadType: 'download', // 原文下载方式 download=直接输出下载
  env: {
    // 单机版数据请求参数配置
    VUE_APP_API_BASE_URL: "http://127.0.0.1:8211/base/", // API接口域名
    VUE_APP_API_DOMAIN: "http://127.0.0.1:8211/base/", // socket接口域名
    VUE_APP_API_BACKDATABASE_URL: "http://127.0.0.1:7080/", // 数据库备份地址
    VUE_APP_API_HOOKUP_URL: "http://127.0.0.1:9000/login/", // 原文挂接系统登录地址
    VUE_APP_API_ORGINALPREVIEW_URL: "http://127.0.0.1:8012/", // 原文和报表预览地址
    VUE_APP_API_DEFAULTIMAGE_URL: "http://127.0.0.1:85/" // 系统设置默认图片地址
  },
  env2: {
    // 网络版数据请求参数配置
    VUE_APP_API_BASE_URL: "http://127.0.0.1/base/", // API接口域名
    VUE_APP_API_DOMAIN: "http://127.0.0.1/", // socket接口域名
    VUE_APP_API_BACKDATABASE_URL: "http://127.0.0.1:7080/", // 数据库备份地址
    VUE_APP_API_HOOKUP_URL: "http://127.0.0.1:9000/login/", // 原文挂接系统登录地址
    VUE_APP_API_ORGINALPREVIEW_URL: "http://127.0.0.1:8012/", // 原文和报表预览地址
    VUE_APP_API_DEFAULTIMAGE_URL: "http://127.0.0.1:85/" // 系统设置默认图片地址
  },
  setupConfig: {
    colorName: "#DE3221" // 主题颜色
  },
  router: {
    mode: VUE_VERSION_TYPE == 2 ? "hash" : "history" // hash | history
  },
  // 菜单的属性映射
  menu: {
    iconDefault: "el-icon-menu", // 默认icon
    props: {
      menuId: "menuId", // id
      label: "menuName", // 页面名称
      path: "path", // 页面路径
      icon: "icon", // 图标
      parentId: "parentId", // 父级id
      children: "children", // 子节点
      query: "query", // 页面参数，显示在路径上，router的query属性
      meta: "meta" // 其他参数，router的meta属性
    }
  }
};
