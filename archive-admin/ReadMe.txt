DROP PROCEDURE IF EXISTS temp_columnWork;
DELIMITER$$
-- 1表示新增列,2表示修改列类型,3表示删除列
CREATE PROCEDURE temp_columnWork(TableName VARCHAR(50),ColumnName VARCHAR(50),SqlStr VARCHAR(4000),CType INT)
BEGIN
DECLARE Rows1 INT;
SET Rows1=0;
SELECT COUNT(*) INTO Rows1  FROM INFORMATION_SCHEMA.Columns
WHERE table_schema= DATABASE() AND table_name=TableName AND column_name=ColumnName;
-- 新增列
IF (CType=1 AND Rows1<=0) THEN
SET SqlStr := CONCAT( 'ALTER TABLE ',TableName,' ADD COLUMN ',ColumnName,' ',SqlStr);
-- 修改列类型
ELSEIF (CType=2 AND Rows1>0)  THEN
SET SqlStr := CONCAT('ALTER TABLE ',TableName,' MODIFY  ',ColumnName,' ',SqlStr);
-- 删除列
ELSEIF (CType=3 AND Rows1>0) THEN
SET SqlStr := CONCAT('ALTER TABLE  ',TableName,' DROP COLUMN  ',ColumnName);
ELSE  SET SqlStr :='';
END IF;
-- 执行命令
IF (SqlStr<>'') THEN
SET @SQL1 = SqlStr;
PREPARE stmt1 FROM @SQL1;
EXECUTE stmt1;
END IF;
END$$
DELIMITER ;


SELECT CONCAT('alter table  ', table_name, ' add isRemind int default 0 null comment \'设置提醒 1是 0否\' ;') FROM information_schema.TABLES WHERE (table_name like 'arc_info1%' ) AND table_schema = 'archive_cloud';
SELECT CONCAT('alter table  ', table_name, ' add commentCount int default 0 null comment \'评论数量\' ;') FROM information_schema.TABLES WHERE (table_name like 'arc_info1%' ) AND table_schema = 'archive_cloud';
SELECT CONCAT('update ', table_name, ' set  seq=999999 where seq is null ;' )  FROM information_schema.TABLES WHERE (table_name like 'arc_original1%' ) AND table_schema = 'archive_cloud';

